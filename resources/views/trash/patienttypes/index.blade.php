@extends('layouts.admin')
@section('content')
@php
    use App\PatientType;
    $patienttypes = PatientType::where('trashed','true')->get();
    $i=1;
@endphp
<div class="container">
    
    <div class="row">
        <div class="col-md-12">
            <!-- DATA TABLE-->
            <div class="table-responsive m-b-40">
                <table id="patient-que-table" class="table datatable table-borderless table-data3">
                    <thead>
                        <tr>
                            <th>Number</th>
                            <th>Name</th>
                            <th>Price</th>
                            {{-- <th></th>
                            
                            <th></th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($patienttypes as $patienttype)
                        <tr class="table-row">
                            <td>{{$i}}</td>
                            <td>{{$patienttype->name}}</td>
                            <td>{{$patienttype->price}}</td>
                            {{-- <td><a class="btn btn-primary" href="{{url('admin/reception/patienttypes/edit',$patienttype->id)}}">Edit</a></td>
                            <td>
                                <a href="{{url('admin/reception/patienttypes/delete',$patienttype->id)}}" class="btn btn-danger">Delete</a>
                            </td> --}}
                        </tr>
                        @php
                            $i++;
                        @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- END DATA TABLE                  -->
        </div>
    </div>
</div>
@endsection