@php
    use App\Medicine;
    use App\MedicineCategory;
    $categories = MedicineCategory::all();
    $medicines = Medicine::where('trashed','true')->get();
    $i=1;
@endphp
@extends('layouts.admin') @section('content')
<div class="container">
<div class="row">
    <div class="col-md-12">
         
        <!-- DATA TABLE-->
        <div class="table-responsive m-b-40">
            <table id="patient-que-table" class="datatable table table-borderless table-data3">
                <thead>
                    <tr>
                        <th>Number</th>
                        <th>Name</th> 
                        <th>Code</th>
                        <th>Description</th>
                        <th>Unit selling <br>Price</th>
                        {{-- <th></th>
                        <th></th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach($medicines as $medicine)
                    <tr class="table-row">
                    <td>{{$i}}</td>
                        <td>{{$medicine->name}}</td> 
                        <td>{{$medicine->code}}</td>
                        <td>{{$medicine->description}}</td>
                        <td>{{$medicine->selling_price}}</td>
                        {{-- <td><a href="" class="btn btn-primary">View</a></td>
                        <td>
                            <a href="{{url('admin/pharmacy/medicines/delete',$medicine->id)}}" class="btn btn-danger">Delete</a>
                        </td> --}}
                    </tr>
                    @php
                        $i++;
                    @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- END DATA TABLE                  -->
    </div>
</div>

</div>
@endsection