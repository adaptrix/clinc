@extends('layouts.admin')

@section('content')
    <div class="container">
        @include('components.content-header',[
            'header'=>"Schedules",
            'button'=>[
                "url"=>$new_schedule_url,
                "name"=>"Add new schedule",
                "color"=>"primary"
            ]
        ])
        @include('components.table',[
            "columns"=>["No","Date","Time","Brief","File","Action"],
            "collection"=>$schedules,
            "data_columns"=>[
                "iteration",
                "date",
                "time",
                "content",
                "file_name",
                "buttons"=>[
                    ["name"=>"Edit","url"=>$edit_schedule_url,"color"=>"primary","id"=>true],
                    ["name"=>"View","url"=>$view_schedule_url,"color"=>"success","id"=>true],
                    ["name"=>"Remove","url"=>$remove_schedule_url,"color"=>"danger","id"=>true,"should_confirm"=>true,"action"=>"remove"]
                ]
            ]
        ])
    </div>
@endsection