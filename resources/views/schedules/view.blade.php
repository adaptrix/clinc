@extends('layouts.admin')

@section('content')
    <div class="container">
        @include('components.content-header',[
            "header"=>"Schedule"
        ])
        <div class="row">
            <div class="col-md-12" style="padding-left:30px; padding-right:30px;">
                <table class="table borderless m-t-10">
                    <tr>
                        <td col="20%">Date:</td>
                        <td>{{$model->date}}</td>
                    </tr>
                    <tr>
                        <td col="20%">Time:</td>
                        <td>{{$model->time}}</td>
                    </tr>
                    <tr>
                        <td>Content:</td>
                        <td>{!!$model->content!!}</td>
                    </tr> 
                    <tr>
                        <td>File:</td>
                        <td><a href="{{asset('storage/'.$model->url)}}">{{$model->file_name}}</a></td>
                    </tr>
                </table>                
            </div>
        </div>

    </div>
@endsection