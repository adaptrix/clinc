@php
    $types = App\MedicineCategory::all(); 
@endphp
<div class="sufee-alert alert with-close alert-danger fade show" style="display:none;" id="warning_div">
    <span class="badge badge-pill badge-danger">Failed!</span>
    <p id="warning_text"></p> 
</div> 
<div class="row">
    <div class="col-lg-12" id="medicine_container">                
        <div class="row">
            <div class="col-md-2">      
                <label for="med">Medicine type:</label>                 
                <select class="select2 form-control" onchange="fetchMedicines(1);loadUnits(1)" id="medicinesType1">
                    <option value="" disabled selected> Please Select</option> 
                    @foreach($types as $type)
                    <option value="{{$type->id}}">{{$type->name}}</option>
                    @endforeach
                </select> 
            </div>
            <div class="col-md-2">      
                <label for="med">Medicine:</label>                 
                <select class="select2 form-control" name="medicine[]" id="medicines1">
                    <option value="" disabled selected> Please Select</option>  
                </select> 
            </div>
            <div class="col-md-2">    
                <div class="row">
                    <div class="col-md-5">
                        <label for="med">Dose:</label>
                        <input type="hidden" name="dose[]"  id="tabs1">
                        {{-- <input type="hidden" name="quantityy[]"  id="tabs1"> --}}
                        <input type="text" style="width:100%" name="quantity[]" onkeyup="calculateTablets(1)" id="quantity1" placeholder="" class="form-control form-control-sm">
                    </div> 
                    <div class="col-md-7"> 
                        <label for="unit">Unit:</label>
                        @php
                            $units = \App\MedicineUnit::all();
                        @endphp
                        <select class="select2-tagged" name="medicine_unit[]" id="medicineUnits">
                            @foreach($units as $unit)
                                <option value="{{$unit->id}}">{{$unit->name}}</option>
                            @endforeach
                        </select>
                        
                        {{-- <input type="text" style="width:100%" name="mgs_per_tablet[]" onkeyup="calculateTablets(1,2)" id="mgs_per_tablet_1" placeholder="Mgs" class="form-control form-control-sm" disabled> --}}
                    </div> 
                </div>                        
            </div>
            <div class="col-md-1">
                    <label for="med">Per Day:</label>    
                <input type="text" name="per_day[]" value="" onkeyup="calculateQuantity(1)" id="perday1" placeholder="" class="form-control form-control-sm ">
            </div>
                <div class="col-md-1">
                    <label for="med">Period:</label>    
                    <div class="row">
                        <input type="text" name="days[]" value="
                        " onkeyup="calculateQuantity(1)" id="days1" placeholder="" class="form-control form-control-sm col-md-4">
                        <select name="length[]" onchange="calculateQuantity(1)" id="length1" class="form-control form-control-sm  col-md-8">
                            <option value="/7">/7</option>
                            <option value="/12">/12</option>
                            <option value="/52">/52</option>
                        </select>
                    </div>                            
                </div>
            <div class="col-md-1">
                    <label for="med">Quantity:</label>    
                <input type="text" onkeyup="checkStock(1)" name="total[]" id="total1" placeholder="" class="form-control form-control-sm ">
            </div>
            <div class="col-md-1">
                <label for="med"></label>   
                <button style="margin-top:8px;" type="button" onclick="show_descripiton(1);" class="btn btn-sm btn-outline-success">Description</button>
            </div>
            <div class="col-md-1">
                <label for="med"></label>   
                <button style="margin-top:35px;" type="button" onclick="add_medicine_fields();" class="btn btn-outline-success btn-sm"><i class="fa fa-plus"></i></button>
            </div>
            <div class="col-md-1" id="checking_stock_gif" style="display:none;">
                <p style="font-size:10px;">checking stock..</p>
                <img src="{{asset('assets/images/icon/loading.gif')}}" style="width:50px;" alt="">
            </div>
        </div>
    </div>
</div> 