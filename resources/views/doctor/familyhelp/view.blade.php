@extends('home')
@section('section')
<div style="padding:10px;">
    @php
    $fh = App\FamilyHelp::find($fhid);
    $types = App\MedicineCategory::all();
    $labtests = App\LabTest::all();
    $symptoms = App\Symptom::all();
    $medrecords =
    App\MedicineRecord::where('visit_no',$pvn)->where('patient_registration_no',$prn)->where('family_help_id',$fhid)->get();
    $labtestrecs =
    App\LabTestRecord::where('visit_no',$pvn)->where('patient_registration_no',$prn)->where('family_help_id',$fhid)->get();
    $bl =
    App\BillingList::where('patient_registration_no','=',$prn)->where('visit_no','=',$pvn)->where('family_help_id',$fhid)->first();
    $no = 1; 
    // dd($bl);
    @endphp
    <div>
        <a href="{{$url2}}" style="z-index:10000000000000000;margin-left:115px; ">back</a>
        <div class="modal-dialog modal-lg" role="document" style="max-width:1100px !important;">
            <div class="modal-content">
                {{-- <form action="{{url('doctor/patient/familyhelp')}}" method="POST">
                    {{csrf_field()}} --}}
                    <div class="modal-header">
                        <h5 class="modal-title" id="mediumModalLabel">Family Help</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="name" class=" form-control-label">Name</label>
                                    <div class="input-group">
                                        <input type="text" name="name" id="name" value="{{$fh->name}}" class="form-control form-control-sm"
                                            disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">Relationship:</label>
                                    <select class="form-control form-control-sm" name="relationship" id="" disabled>
                                        <option {{$fh->relationship=="Mother"?'selected':''}} value="Mother">Mother</option>
                                        <option {{$fh->relationship=="Father"?'selected':''}} value="Father">Father</option>
                                        <option {{$fh->relationship=="Brother"?'selected':''}} value="Brother">Brother</option>
                                        <option {{$fh->relationship=="Sister"?'selected':''}} value="Sister">Sister</option>
                                        <option {{$fh->relationship=="Son"?'selected':''}} value="Son">Son</option>
                                        <option {{$fh->relationship=="Daughter"?'selected':''}} value="Daughter">Daughter</option>
                                        <option {{$fh->relationship=="Grandfather"?'selected':''}} value="Grandfather">Grandfather</option>
                                        <option {{$fh->relationship=="Grandson"?'selected':''}} value="Grandson">Grandson</option>
                                        <option {{$fh->relationship=="Cousin"?'selected':''}} value="Cousin">Cousin</option>
                                        <option {{$fh->relationship=="Niece"?'selected':''}} value="Niece">Niece</option>
                                        <option {{$fh->relationship=="Nephew"?'selected':''}} value="Nephew">Nephew</option>
                                        <option {{$fh->relationship=="Uncle"?'selected':''}} value="Uncle">Uncle</option>
                                        <option {{$fh->relationship=="Aunt"?'selected':''}} value="Aunt">Aunt</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="dob" class="form-control-label">Date of Birth</label>
                                    <div class="input-group">
                                        <input type="date" id="modal-dob" name="dob" value="{{$fh->dob}}" class="form-control form-control-sm"
                                            disabled>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="col-md-4">
                                <label for="age-year" class=" form-control-label">Age</label>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" id="modal-age-year" name="age-year" class="form-control form-control-sm"
                                                    disabled>
                                            </div>
                                            <small class="form-text text-muted">Year</small>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" id="modal-age-month" name="age-month" class="form-control form-control-sm"
                                                    disabled>
                                            </div>
                                            <small class="form-text text-muted">Month</small>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" id="modal-age-day" name="age-day" class="form-control form-control-sm"
                                                    disabled>
                                            </div>
                                            <small class="form-text text-muted">Day</small>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                        <hr>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <form action="{{url('doctor/giveLabTestFh')}}" method="post">
                                   {{csrf_field()}} 
                                <label for="symptoms">Lab tests:</label>
                                {{-- <input type="text" name="symptoms" id="symptoms" /> --}}

                                {{-- <select name="select" id="symptomsselect" class="form-control form-control-sm"
                                    multiple data-role="tagsinput">
                                </select> --}}
                                <select class="select2 form-control-sm" name="lab_tests[]" id="lab_tests" multiple="multiple">
                                    @foreach($labtests as $labtest)
                                    <option value="{{$labtest->id}}">{{$labtest->name}}</option>
                                    @endforeach
                                </select> 
                                <input type="hidden" name="url" value="{{url()->full()}}">
                                <input type="hidden" name="prn" value="{{$prn}}">
                                <input type="hidden" name="pvn" value="{{$pvn}}">
                                <input type="hidden" name="fhid" value="{{$fhid}}">
                                <button type="submit" class="btn btn-primary" style="margin-top:10px;">Give tests</button>
                               </form>     
                            </div>
                            {{-- <div class="form-group col-md-6">
                                <label for="symptoms">Symptoms/Cc:</label>
                                {{-- <input type="text" name="symptoms" id="symptoms" /> --}}

                                {{-- <select name="select" id="symptomsselect" class="form-control form-control-sm"
                                    multiple data-role="tagsinput">
                                </select>
                                <select class="select2tags form-control-sm" name="symptoms[]" id="symptoms" multiple="multiple">
                                    @foreach($symptoms as $symptom)
                                    <option value="{{$symptom->name}}">{{$symptom->name}}</option>
                                    @endforeach
                                </select>
                            </div> --}}
                        </div>
                        @if (!empty($labtestrecs))
                        <div class="row">
                            <div class="col-lg-12">
                                <p><strong>Previous Tests:</strong></p>
                                <p>
                                    @forelse ($labtestrecs as $labtestrec)
                                    {{$labtestrec->lab_test->name." test,"}}
                                    @empty
                                    @endforelse
                                    <a href="{{url('doctor/results',$bl->id)}}">View results</a></p>
                            </div>
                        </div>
                        <br>
                        @endif
                        <hr>

                        <form action="{{url('doctor/prescribeMedicineFh')}}" method="POST">
                            {{ csrf_field() }}
                            <div class="row" style="font-size:11px;padding-bottom:50px;">
                                <div class="col-lg-12" id="medicine_container_modal">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="med">Medicine type:</label>
                                            <select class="select2 form-control form-control-sm" onchange="fetchMedicinesModal(1);loadUnitsModal(1)"
                                                id="medicinesTypeModal1">
                                                <option value="" disabled selected> Please Select</option>
                                                @foreach($types as $type)
                                                <option value="{{$type->id}}">{{$type->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label for="med">Medicine:</label>
                                            <select class="select2 form-control form-control-sm" name="medicine[]" id="medicinesModal1">
                                                <option value="" disabled selected> Please Select</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <label for="med">Dose:</label>
                                                    <input type="hidden" name="quantity[]" id="tabsModal1">
                                                    <input type="text" style="width:100%" name="dose[]" onkeyup="calculateTabletsModal(1)"
                                                        id="quantityModal1" placeholder="" class="form-control form-control-sm">
                                                </div>
                                                <div class="col-md-7">
                                                    <label for="med">Unit:</label>
                                                    <select name="" id="medicineUnitModal1" name="unit[]" onchange="calculateTabletsModal(1)"
                                                        class="form-control form-control-sm">
                                                    </select>

                                                    {{-- <input type="text" style="width:100%" name="mgs_per_tablet[]"
                                                        onkeyup="calculateTablets(1,2)" id="mgs_per_tablet_1"
                                                        placeholder="Mgs" class="form-control form-control-sm" disabled>
                                                    --}}
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-1">
                                            <label for="med">Per Day:</label>
                                            <input type="text" name="per_day[]" value="0" onkeyup="calculateQuantityModal(1)"
                                                id="perdayModal1" placeholder="" class="form-control form-control-sm ">
                                        </div>
                                        <div class="col-md-1">
                                            <label for="med">Period:</label>
                                            <div class="row">
                                                <input type="text" name="days[]" value="0" onkeyup="calculateQuantityModal(1)"
                                                    id="daysModal1" placeholder="" class="form-control form-control-sm col-md-4">
                                                <select name="length[]" onchange="calculateQuantityModal(1)" id="lengthModal1"
                                                    class="form-control form-control-sm  col-md-8">
                                                    <option value="/7">/7</option>
                                                    <option value="/12">/12</option>
                                                    <option value="/52">/52</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <label for="med">Quantity:</label>
                                            <input type="text" onkeyup="checkStockModal(1)" name="total[]" id="totalModal1"
                                                placeholder="" class="form-control form-control-sm ">
                                        </div>
                                        <div class="col-md-1">
                                            <label for="med"></label>
                                            <button style="margin-top:8px;font-size:13px;" type="button" onclick="show_descripiton_modal(1);"
                                                class="btn btn-sm btn-outline-success">Description</button>
                                        </div>
                                        <div class="col-md-1">
                                            <label for="med"></label>
                                            <button style="margin-top:24px;" type="button" onclick="add_medicine_fields_modal();"
                                                class="btn btn-outline-success btn-sm"><i class="fa fa-plus"></i></button>
                                        </div>
                                        <div class="col-md-1" id="checking_stock_gif_modal" style="display:none;">
                                            <p style="font-size:10px;">checking stock..</p>
                                            <img src="{{asset('assets/images/icon/loading.gif')}}" style="width:23px;"
                                                alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="url" value="{{url()->full()}}">
                            <input type="hidden" name="prn" value="{{$prn}}">
                            <input type="hidden" name="pvn" value="{{$pvn}}">
                            <input type="hidden" name="fhid" value="{{$fhid}}">
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary">Prescribe medicines</button></div>
                            </div>
                        </form>
                        @if(!$medrecords->isEmpty())
                        <div class="row">
                            <div class="col-md-12">
                                <p><strong>Previously prescribed medicines</strong></p>
                            </div>
                            <div class="col-md-10">
                                <!-- DATA TABLE-->
                                <div class="table-responsive m-b-40">
                                    <table id="patient-que-table" class="mytable">
                                        <thead>
                                            <tr>
                                                {{-- <th>Number</th> --}}
                                                <th>No</th>
                                                <th>Medicine Name</th>
                                                <th>Quantity</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($medrecords as $medrecord)
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$medrecord->medicine->name}}</td>
                                                <td>{{' '. $medrecord->quantity.' X'.$medrecord->perday.' X
                                                    '.$medrecord->days}}</td>
                                                <td>{{$medrecord->status}}</td>
                                                @php $no++; @endphp
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE                  -->
                            </div>
                        </div>
                        @endif
                    </div>
                    <input type="hidden" name="prn" value="{{$prn}}">
                    <input type="hidden" name="pvn" value="{{$pvn}}">
                    <div class="modal-footer">
                        <div class="sufee-alert alert with-close alert-danger fade show" style="display:none;" id="warning_div_modal">
                            <span class="badge badge-pill badge-danger">Failed!</span>
                            <p id="warning_text_modal"></p>
                        </div>
                        {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> --}}
                        {{-- <button type="submit" id="submit_button_modal" class="btn btn-primary">Submit</button> --}}
                    </div>
            </div>
            {{-- </form> --}}
        </div>
    </div>
</div>
@endsection