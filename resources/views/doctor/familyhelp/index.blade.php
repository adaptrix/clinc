@php
    $types = App\MedicineCategory::all();    
@endphp
<div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width:1100px !important;">
        <div class="modal-content">
            <form action="{{url('doctor/patient/familyhelp')}}" method="POST">
            {{csrf_field()}}
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Family Help</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">                    
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="name" class=" form-control-label">Name</label>
                            <div class="input-group"> 
                                <input type="text" name="name" id="name" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Relationship:</label> 
                            <select class="form-control" name="relationship" id=""> 
                                <option value="Mother">Mother</option>
                                <option value="Father">Father</option>
                                <option value="Brother">Brother</option>
                                <option value="Sister">Sister</option>
                                <option value="Son">Son</option>
                                <option value="Daughter">Daughter</option>
                                <option value="Grandfather">Grandfather</option>
                                <option value="Grandson">Grandson</option>
                                <option value="Cousin">Cousin</option>
                                <option value="Niece">Niece</option>
                                <option value="Nephew">Nephew</option>
                                <option value="Uncle">Uncle</option>
                                <option value="Aunt">Aunt</option>
                            </select>
                        </div>                        
                    </div> 
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="dob" class="form-control-label">Date of Birth</label>
                            <div class="input-group"> 
                                <input type="date" id="modal-dob" name="dob" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="age-year" class=" form-control-label">Age</label>
                        <div class="row">
                            <div class="col-md-4">
                                    <div class="form-group">
                                    <div class="input-group"> 
                                        <input type="text" id="modal-age-year" name="age-year" class="form-control">
                                    </div>
                                    <small class="form-text text-muted">Year</small>
                                </div>
                            </div>                                   
                            <div class="col-md-4">
                                <div class="form-group"> 
                                    <div class="input-group"> 
                                        <input type="text" id="modal-age-month" name="age-month" class="form-control">
                                    </div>
                                    <small class="form-text text-muted">Month</small>
                                </div>
                            </div> 
                            <div class="col-md-4">
                                <div class="form-group"> 
                                    <div class="input-group"> 
                                        <input type="text" id="modal-age-day" name="age-day" class="form-control">
                                    </div>
                                    <small class="form-text text-muted">Day</small>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="symptoms">Lab tests:</label>
                        {{-- <input type="text"  name="symptoms" id="symptoms" /> --}}
        
                        {{-- <select name="select" id="symptomsselect" class="form-control"  multiple data-role="tagsinput">
                        </select> --}}
                        <select class="select2" name="lab_tests[]" id="lab_tests" multiple="multiple">
                            @foreach($labtests as $labtest)
                            <option value="{{$labtest->id}}">{{$labtest->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="symptoms">Symptoms/Cc:</label>
                        {{-- <input type="text"  name="symptoms" id="symptoms" /> --}}
        
                        {{-- <select name="select" id="symptomsselect" class="form-control"  multiple data-role="tagsinput">
                        </select> --}}
                        <select class="select2tags" name="symptoms[]" id="symptoms" multiple="multiple">
                            @foreach($symptoms as $symptom)
                            <option value="{{$symptom->name}}">{{$symptom->name}}</option>
                            @endforeach
                        </select>
                    </div> 
                </div>
                <div class="row" style="font-size:11px;">
                    <div class="col-lg-12" id="medicine_container_modal">                
                        <div class="row">
                            <div class="col-md-2">      
                                <label for="med">Medicine type:</label>                 
                                <select class="select2 form-control" onchange="fetchMedicinesModal(1);loadUnitsModal(1)" id="medicinesTypeModal1">
                                    <option value="" disabled selected> Please Select</option> 
                                    @foreach($types as $type)
                                    <option value="{{$type->id}}">{{$type->name}}</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="col-md-2">      
                                <label for="med">Medicine:</label>                 
                                <select class="select2 form-control" name="medicine[]" id="medicinesModal1">
                                    <option value="" disabled selected> Please Select</option>  
                                </select> 
                            </div>
                            <div class="col-md-2">    
                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="med">Dose:</label>
                                        <input type="hidden" name="quantity[]"  id="tabsModal1">
                                        <input type="text" style="width:100%" name="dose[]" onkeyup="calculateTabletsModal(1)" id="quantityModal1" placeholder="" class="form-control form-control-sm">
                                    </div> 
                                    <div class="col-md-7">
                                        <label for="med">Unit:</label>
                                        <select name="" id="medicineUnitModal1" name="unit[]" onchange="calculateTabletsModal(1)" class="form-control form-control-sm"> 
                                        </select>
                                        
                                        {{-- <input type="text" style="width:100%" name="mgs_per_tablet[]" onkeyup="calculateTablets(1,2)" id="mgs_per_tablet_1" placeholder="Mgs" class="form-control form-control-sm" disabled> --}}
                                    </div> 
                                </div>
                                
                            </div>
                            <div class="col-md-1">
                                    <label for="med">Per Day:</label>    
                                <input type="text" name="per_day[]" value="0" onkeyup="calculateQuantityModal(1)" id="perdayModal1" placeholder="" class="form-control form-control-sm ">
                            </div>
                                <div class="col-md-1">
                                    <label for="med">Period:</label>    
                                    <div class="row">
                                        <input type="text" name="days[]" value="0" onkeyup="calculateQuantityModal(1)" id="daysModal1" placeholder="" class="form-control form-control-sm col-md-4">
                                        <select name="length[]" onchange="calculateQuantityModal(1)" id="lengthModal1" class="form-control form-control-sm  col-md-8">
                                            <option value="/7">/7</option>
                                            <option value="/12">/12</option>
                                            <option value="/52">/52</option>
                                        </select>
                                    </div>                            
                                </div>
                            <div class="col-md-1">
                                    <label for="med">Quantity:</label>    
                                <input type="text" onkeyup="checkStockModal(1)" name="total[]" id="totalModal1" placeholder="" class="form-control form-control-sm ">
                            </div>
                            <div class="col-md-1">
                                <label for="med"></label>   
                                <button style="margin-top:8px;font-size:13px;" type="button" onclick="show_descripiton_modal(1);" class="btn btn-sm btn-outline-success">Description</button>
                            </div>
                            <div class="col-md-1">
                                <label for="med"></label>   
                                <button style="margin-top:24px;" type="button" onclick="add_medicine_fields_modal();" class="btn btn-outline-success btn-sm"><i class="fa fa-plus"></i></button>
                            </div>
                            <div class="col-md-1" id="checking_stock_gif_modal" style="display:none;">
                                <p style="font-size:10px;">checking stock..</p>
                                <img src="{{asset('assets/images/icon/loading.gif')}}" style="width:23px;" alt="">
                            </div>
                        </div>
                    </div>
                </div> 
                {{-- <div class="row">
                    <div class="col-lg-12" id="modal_medicine_container">                
                        <div class="row" style="font-size:12px;">
                            <div class="col-md-2">      
                                <label for="med">Medicine type:</label>                 
                                <select class="select2 form-control" onchange="fetchMedicinesModal(1)" id="medicinesTypeModal1">
                                    <option value="" disabled selected> Please Select</option> 
                                    @foreach($types as $type)
                                    <option value="{{$type->id}}">{{$type->name}}</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="col-md-2">      
                                <label for="med">Medicine:</label>                 
                                <select class="select2 form-control" name="medicine[]" id="medicinesModal1">
                                    <option value="" disabled selected> Please Select</option>  
                                </select> 
                            </div>
                            <div class="col-md-2">
                                <label for="med">Dose:</label>    
                                <div class="row">
                                    <input type="text" name="quantity[]" onkeyup="calculateTabletsModal(1,1)" id="quantityModal1" placeholder="" class="form-control col-md-5">
                                    <input type="text" name="mgs_per_tablet[]" onkeyup="calculateTabletsModal(1,2)" id="mgs_per_tablet_Modal1" placeholder="Mgs" class="form-control col-md-7" disabled> 
                                </div>
                                
                            </div>
                            <div class="col-md-1">
                                    <label for="med">Per Day:</label>    
                                <input type="text" name="per_day[]" value="0" onkeyup="calculateQuantityModal(1)" id="perdayModal1" placeholder="" class="form-control">
                            </div>
                                <div class="col-md-2">
                                    <label for="med">Period:</label>    
                                    <div class="row">
                                        <input type="text" name="days[]" value="0" onkeyup="calculateQuantityModal(1)" id="daysModal1" placeholder="" class="form-control col-md-4">
                                        <select name="length[]" onchange="calculateQuantityModal(1)" id="lengthModal1" class="form-control col-md-8">
                                            <option value="/7">/7</option>
                                            <option value="/12">/12</option>
                                            <option value="/52">/52</option>
                                        </select>
                                    </div>                            
                                </div>
                            <div class="col-md-1">
                                    <label for="med">Quantity:</label>    
                                <input type="text" onkeyup="checkStockModal(1)" name="total[]" id="totalModal1" placeholder="" class="form-control">
                            </div>
                            <div class="col-md-1">
                                <label for="med"></label>   
                                <button style="margin-top:35px;" type="button" onclick="add_modal_medicine_fields();" class="btn btn-outline-success"><i class="fa fa-plus"></i></button>
                            </div>
                            <div class="col-md-1" id="checking_stock_gif_modal" style="display:none;">
                                <p style="font-size:10px;">checking stock..</p>
                                <img src="{{asset('assets/images/icon/loading.gif')}}" style="width:50px;" alt="">
                            </div>
                        </div>
                    </div>
                </div>   --}}
                <input type="hidden" name="prn" value="{{$prn}}">
                <input type="hidden" name="pvn" value="{{$pvn}}">
                <div class="modal-footer">
                        <div class="sufee-alert alert with-close alert-danger fade show" style="display:none;" id="warning_div_modal">
                                <span class="badge badge-pill badge-danger">Failed!</span>
                                <p id="warning_text_modal"></p> 
                        </div> 
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" id="submit_button_modal" class="btn btn-primary">Confirm</button>
                </div>
            </div>
            </form>
        </div>
    </div>