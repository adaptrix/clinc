@extends('home')
@section('section')

    
    <div class="default-tab">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="queue-list-tab" data-toggle="tab" href="#queue-list" role="tab" aria-controls="queue-list"
            aria-selected="true" ><i class="fas fa-list"></i> Patient Queue</a>
            <a class="nav-item nav-link" id="patients-seen-tab" data-toggle="tab" href="#patients-seen" role="tab" aria-controls="diagnosis"
            aria-selected="false"><i class="fas fa-list"></i> Patients Seen</a> 
            <a class="nav-item nav-link" id="lab-results-tab" data-toggle="tab" href="#lab-results" role="tab" aria-controls="diagnosis"
            aria-selected="false"><i class="fas fa-list"></i> Lab Progress</a> 
            <a class="nav-item nav-link" id="appointments-tab" data-toggle="tab" href="#appointments" role="tab" aria-controls="diagnosis"
            aria-selected="false"><i class="fas fa-list"></i> Appointments</a>  
            <a class="nav-item nav-link" id="sop-tab" data-toggle="tab" href="#sop" role="tab" aria-controls="sop"
            aria-selected="false"><i class="fas fa-plus"></i> SOPs</a> 
            {{-- <a class="nav-item nav-link" id="diagnosis-tab" data-toggle="tab" href="#diagnosis" role="tab" aria-controls="diagnosis"
            aria-selected="false"><i class="fas fa-list"></i> Diagnosis</a> --}}
            </div>
        </nav>
        <div class="tab-content pl-3 pt-2" id="nav-tabContent">
        
            <div class="tab-pane fade show active" id="queue-list" role="tabpanel" aria-labelledby="queue-list-tab">
            @include('doctor.tabs.queue')
            </div>
            <div class="tab-pane fade" id="patients-seen" role="tabpanel" aria-labelledby="patients-seen-tab">
            @include('doctor.tabs.patients-seen')
            </div> 
            <div class="tab-pane fade" id="lab-results" role="tabpanel" aria-labelledby="lab-results-tab">
            @include('doctor.tabs.lab-results')
            </div> 
            <div class="tab-pane fade" id="appointments" role="tabpanel" aria-labelledby="appointments-tab">
            @include('doctor.tabs.appointments')
            </div>   
            <div class="tab-pane fade" id="sop" role="tabpanel" aria-labelledby="sop-tab">
              @include('components.standard_operating_procedure_tab') 
            </div>       
            {{-- <div class="tab-pane fade" id="diagnosis" role="tabpanel" aria-labelledby="diagnosis-tab">
            @include('doctor.diagnosis')
            </div> --}}
        
        </div>
    </div>

@endsection