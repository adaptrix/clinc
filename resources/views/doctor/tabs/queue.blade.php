@php
use App\PatientQueueTemporary;
use App\Patient;
$patients = PatientQueueTemporary::whereDate('created_at',Carbon\Carbon::today())
                                  ->whereIn('status',['waiting','out','entered'])
                                  ->whereDoctor(Auth::id())
                                  ->orderBy('service_mode','asc')
                                  ->orderBy('created_at','asc')
                                  ->get();
$results = App\BillingList::whereIn('status',['done','Left','Seen'])->orderBy('updated_at','asc')->get();
// dd($results);
@endphp
  <!-- DATA TABLE-->
  <section class="p-t-20">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="">Patients Queue</h3>
                <h5>{{Carbon\Carbon::now()->format('d/m/y')}}</h5><br>
                <div class="row">
                    <div class="col-md-12">
                        <!-- DATA TABLE-->
                        <div class="table-responsive m-b-40">
                            <table id="patient-doc-que" class="mytable datatable">
                                <thead>
                                    <tr>
                                        <th>Que. #</th>
                                        <th>Time/<br>Appointment</th> 
                                        <th>Status</th>
                                        <th>Reg #</th>
                                        <th>name</th>
                                        <th>Type</th>
                                        <th>Service Mode</th>
                                        {{-- <th>Waiting Time</th> --}}
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($patients as $pt)
                                        @php
                                            $patient = Patient::find($pt->patient_reg_no);
                                        @endphp
                                    <tr>
                                        <td>{{$pt->patient_que_no}}</td>
                                        <td>{{$pt->created_at->format('h:i a')}}</td> 
                                        <td>{{$pt->status}}</td>
                                        <td>{{$pt->patient_reg_no}}</td>
                                        <td>{{$patient->name}}</td>
                                        <td>{{$patient->patient_type}}</td>
                                        <td>{{$pt->serviceMode->name}}</td>
                                        <td><a href="{{route('patient-details',$pt->id)}}" class="btn btn-primary">Attend to</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE                  -->
                    </div>
                </div>
                <h3 class="">Lab Queue</h3>
                <h5>{{Carbon\Carbon::now()->format('d/m/y')}}</h5><br>
                  <div class="row">
                      <div class="col-md-12">
                          <!-- DATA TABLE-->
                          <div class="table-responsive m-b-40">
                              <table id="patient-que-table" class="mytable datatable">
                                  <thead>
                                      <tr>
                                            <th>Que #</th> 
                                            <th>Time</th>
                                            <th>reg. #</th>
                                            <th>Visit no</th>
                                            <th>name</th>
                                            <th>Patient<br>Type</th>
                                            <th>Service Mode</th>  
                                            <th>Unit Alloc.</th>
                                            <th></th> 
                                      </tr>
                                  </thead>
                                  <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach ($results as $result)
                                      <tr>
                                          <td>{{$i}}</td>
                                          @php 
                                              $i++;
                                              $patient = App\Patient::find($result->patient_registration_no);
                                              $patientQ = App\PatientQueueTemporary::where('patient_reg_no',$result->patient_registration_no)->where('visit_no',$result->visit_no)->first();
                                              $name = $patient->name;
                                              $url = url('doctor/history?id='.$patient->reg_no.'&url='.url()->full().'&status='.'close'.'&billinglist_id='.$result->id);
                                              if($result->family_help_id!=0){ 
                                                    $name = $name.'\'s '.$result->familyHelp->relationship;
                                                    $url = url('doctor/familyhelp/view?prn='.$result->patient_registration_no.'&pvn='.$result->visit_no.'&fhid='.$result->family_help_id.'&url2='.url()->full().'&id='.$result->id);
                                              } 
                                          @endphp
                                          <td>{{$result->updated_at->format('h:i a')}}</td>
                                          <td>{{$result->patient_registration_no}}</td>
                                          <td>{{$result->visit_no}}</td>
                                          <td>{{$name}}</td>
                                          <td>{{$patient->patient_type}}</td>
                                          <td>{{$patientQ->service_mode}}</td> 
                                          <td>{{$patientQ->unit_allocation}}</td>
                                          <td><a href="{{$url}}" class="btn btn-primary">View</a></td>                                      
                                      </tr>   
                                    @endforeach
                                  </tbody>
                              </table>
                          </div>
                          <!-- END DATA TABLE                  -->
                      </div>
                  </div>
            </div>
        </div>
    </div>
</section>
<!-- END DATA TABLE-->