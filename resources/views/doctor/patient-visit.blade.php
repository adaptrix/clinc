@php
    use App\PatientQueueTemporary;
    use App\Patient;
    use Carbon\Carbon;
    use App\Vital;
    use App\PatientVisit;
    use App\ChiefComplain;
    use App\History;
    use App\HomeMedicine;
    use App\Examination;
    use App\SymptomRecord;
    use App\MedicineRecord;
    use App\CommentFromDoctor;
    use App\LabTestRecord;
    use App\LabList;
    use App\KnownPatientOf;
    $pv = PatientVisit::findOrFail($id); 
    $pts = PatientQueueTemporary::where('patient_reg_no',$pv->patient_registration_no)->where('visit_no',$pv->visit_no)->where('status','attended')->first();
    $pvn = $pts->visit_no;
    if($pvn>1)
        $ppvn = $pvn-1;
    else
        $ppvn = $pvn;

    $prn = $pts->patient_reg_no;

    $patient = Patient::find($pts->patient_reg_no);
    
    $date = date_create_from_format('Y-m-d', $patient->DOB);   
    $reference = Carbon::now();
    $diff = $reference->diff($date);
    $years = $diff->y;

    $vitals = Vital::where('patient_reg_no','=',$prn)->where('visit_no','=',$pvn)->first();
    // $cc = ChiefComplain::where('patient_registration_no','=',$prn)->where('visit_no','=',$pvn)->first();
    $history = History::where('patient_registration_no','=',$prn)->where('visit_no','=',$pvn)->first();
    $homeMedicine = HomeMedicine::where('patient_registration_no','=',$prn)->where('visit_no','=',$pvn)->first();
    $examination = Examination::where('patient_registration_no','=',$prn)->where('visit_no','=',$pvn)->first();
    $cfd = CommentFromDoctor::where('patient_registration_no','=',$prn)->where('visit_no','=',$pvn)->first();
    $lab_results = LabList::where('patient_registration_no',$prn)->where('visit_no',$pvn)->first();

    $symptoms = SymptomRecord::where('patient_registration_no','=',$prn)->where('visit_no','=',$pvn)->get();
    $symptoms_count = $symptoms->count();
    $labtests = LabTestRecord::where('patient_registration_no','=',$prn)->where('visit_no','=',$pvn)->get();
    $labtests_count = $labtests->count(); 
    $medicines = MedicineRecord::where('patient_registration_no','=',$prn)->where('visit_no','=',$pvn)->get(); 
    $kpf = KnownPatientOf::where('patient_registration_no',$prn)->first(); 
    
    $a = 1;$b = 1;  
@endphp

@extends('home')
@section('section')
<div style="padding:40px;">
    <a href="{{ url()->previous()}}" class="btn btn-link">Back</a>
    <div class="card card-body">
        <h4 class="">Patient Details</h4>
        <hr>
        <div class="row">
            <div class="col-lg-1">
                Date: <br> <strong>{{$pts->created_at->format('d/m/Y')}}</strong> 
            </div>
            <div class="col-lg-1">
                PQ No:<br> <strong>{{$pts->patient_que_no}}</strong>
            </div>
            <div class="col-lg-1">
                C/I:<br> <strong>Cash</strong>
            </div>
            <div class="col-lg-2">
                Name:<br> <strong>{{$patient->name}}</strong>
            </div>
            <div class="col-lg-1">
                Age:<br> <strong>{{$years}}</strong>
            </div>
            <div class="col-lg-1">
                Gender:<br> <strong>{{$patient->gender}}</strong>
            </div>
            {{-- <div class="col-lg-2">
                Occupation:<br> <strong>{{$patient->Occupation}}</strong>
            </div> --}}
        </div>
        <br>
        <h4 class="">Vital Signs</h4>
        <hr>
        <div class="row">
            <div class="col-lg-1">
                Weight: <br> <strong>{{$vitals->weight}}</strong> 
            </div>
            <div class="col-lg-1">
                Height:<br> <strong>{{$vitals->height}}</strong>
            </div>
            <div class="col-lg-1">
                BP:<br> <strong>{{$vitals->blood_pressure}}</strong>
            </div>
            <div class="col-lg-1">
                Temp:<br> <strong>{{$vitals->temperature}}</strong>
            </div>
            <div class="col-lg-1">
                PR:<br> <strong>{{$vitals->pulse_rate}}</strong>
            </div>
        </div>
        <hr><hr>
        <div class="row">
            <div class="col-lg-2">Visit No: <strong>{{$pvn}}</strong></div>
            <div class="col-lg-3">Known Patient of: &nbsp;<strong>{{$kpf->content}}</strong></div>
            <div class="col-lg-2"><a href="{{route('patient-visit',$ppvn)}}">Previous Visit</a></div>
            <div class="col-lg-2"><a href="{{ url('/laboratory/results/view',$lab_results->id)}}">Lab Results</a></div>
        </div>    
        <hr><hr>
        <div class="row">
            {{-- <div class="col-lg-3">
                <p>C/c:</p>
                <p><strong>{{$cc->content}}</strong></p>
            </div> --}}
            <div class="col-lg-6">
                <p>History:</p>
                <p><strong>{{$history->content}}</strong></p>
            </div>    <br><br>
            <div class="col-lg-3">
                <p>Home Medicine:</p>
                <p><strong>{{$homeMedicine->content}}</strong></p>
            </div>
            <div class="col-lg-3">
                <p>Examinations:</p>
                <div class="row">
                    <div class="col-md-4">
                            <p>Ge:</p>
                            <p><strong>{{$examination->ge}}</strong></p>
                    </div>
                    <div class="col-md-4">
                            <p>RES:</p>
                            <p><strong>{{$examination->res}}</strong></p>
                    </div>
                    <div class="col-md-4">
                            <p>CVS:</p>
                            <p><strong>{{$examination->cvs}}</strong></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                            <p>PA:</p>
                            <p><strong>{{$examination->pa}}</strong></p>
                    </div>
                    <div class="col-md-4">
                            <p>CNS:</p>
                            <p><strong>{{$examination->cns}}</strong></p>
                    </div>
                    <div class="col-md-4">
                            <p>Other:</p>
                            <p><strong>{{$examination->other}}</strong></p>
                    </div>
                </div>
            </div>
        </div>    
        <hr>
        <div class="row">
            <div class="col-lg-3">
                <p>Symptoms/Cc:</p>
                <strong><p>
                @foreach($symptoms as $symptom)
                    @if($symptoms_count>$a)
                    {{$symptom->symptom->name}},
                    @else
                    {{$symptom->symptom->name}}
                    @endif
                    @php
                        $a++;
                    @endphp
                @endforeach
                </p></strong>
            </div>
            <div class="col-lg-9">
                    <p>Prescribed Medicines:</p>
                    <strong>
                    @if($medicines!=null)
                        @foreach($medicines as $medicine) 
                        <div class="row">
                            <div class="col-md-3">{{$medicine->medicine->name}}</div>
                            <div class="col-md-2">quantity {{$medicine->quantity}} </div>
                            <div class="col-md-2">{{$medicine->perday}} per day</div>
                            <div class="col-md-2">{{$medicine->days}} days</div>       
                            <div class="col-md-2">{{$medicine->status}}</div>                    
                        </div> 
                        @endforeach
                    @else
                        <span>no tests</span> 
                    @endif
                    </strong>
            </div>
            <div class="col-lg-3">
                <p>Lab tests:</p>
                <strong><p>
                @if($labtests!=null)
                    @foreach($labtests as $labtest)
                        @if($labtests_count>$b)
                        {{$labtest->lab_test->name}},
                        @else
                        {{$labtest->lab_test->name}}
                        @endif
                        @php
                            $b++;
                        @endphp
                    @endforeach
                @else
                    <span>no tests</span> 
                @endif
                </p></strong>
            </div>
        </div>
    </div>
</div>

@endsection