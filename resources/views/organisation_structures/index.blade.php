@extends('layouts.admin')
@section('content')
    <div class="container"> 
        @include('components.content-header',[
            "header"=>"Organisation Structure",
            "button"=>[
                "name"=>"Update organisation structure",
                "color"=>"primary",
                "url"=>$new_organisation_structure_url
            ]
        ])
        <div class="row">
            <div class="col-md-12 ml-3">
                @if ($organisation_structure)
                    <a href="{{$download_organisation_structure_url.$organisation_structure->   id}}">
                        {{$organisation_structure->name}}
                    </a>
                @else
                    <p>No uploaded organisation structure</p>                   
                @endif
            
            </div>
        </div>

        {{-- @include('components.table',[
            "columns"=> ["No","File","Description","Date","Action"],
            "collection"=>$organisation_structures,
            "data_columns"=>[
                "iteration",
                "name",
                "description",
                "created_at",
                "buttons"=>[
                    ["name"=>"Edit","url"=>$edit_organisation_structure_url,"id"=>true,"color"=>"primary"],
                    ["name"=>"Download","url"=>$download_organisation_structure_url,"id"=>true,"color"=>"success"],
                    ["name"=>"Delete","url"=>$remove_organisation_structure_url,"id"=>true,"color"=>"danger","should_confirm"=>true,"action"=>"delete"] 
                ]
            ]
        ]) --}}
    </div> 
@endsection