<div class="row"> 
        <div class="table-responsive">
            <table class="table col-lg-12 col-md-12 col-sm-12">
                <tbody>
                    @foreach($medicinerecords as $medicinerecord)
                    <tr>
                    <td>{{$medicinerecord->medicine_id}}</td>
                    <td>{{$medicinerecord->quantity}}</td>
                        <td>{{$medicinerecord->perday}} per day</td>
                        <td>{{$medicinerecord->days}} days</td>
                        <td>
                            <select name="medicine-status" id="">
                                <option value="Taken">Taken</option>
                                <option value="Refused">Refused</option>
                            </select>
                        </td> 
                    </tr>  
                    @php 
                        $servicecost = $servicecost + ($medicinerecord->medicine->selling_price * $medicinerecord->days);
                    @endphp
                    @endforeach
                </tbody>
            </table>
        </div> 
</div>