<!-- DATA TABLE-->
<section class="p-t-20">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="title-5 m-b-35">Laboratory Requests</h3>

                <div class="row">
                    <div class="col-md-12">
                        <!-- DATA TABLE-->
                        <div class="table-responsive m-b-40">
                            <table id="patient-que" class="table datatable-1 table-borderless table-data3">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Item Requested</th>
                                        <th>Quantity</th>
                                        <th>Status</th>
                                        <th>Usage</th>
                                        <th>Item Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>2018-09-29 05:57</td>
                                        <td>Lab reagent</td>
                                        <td>4</td>
                                        <td class="denied">Not Recieved</td>
                                        <td>Finished</td>
                                        <td>Finished</td>
                                        <td>
                                            <div class="table-data-feature">
                                                <!--<a href="{{-- - --}}url('/diagnosis')}}" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </a>-->
                                                <a href="#" data-target="#editRequest" data-toggle="modal">
                                                    <button class="btn btn-success" >
                                                        <i class="zmdi zmdi-edit"> Edit</i>
                                                    </button>
                                                </a>
                                                <button class="btn btn-danger">
                                                    <i class="zmdi zmdi-delete"> Delete</i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE                  -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END DATA TABLE-->