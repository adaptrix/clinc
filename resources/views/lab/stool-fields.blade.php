<div class="col-lg-4"> 
        <input type="hidden" name="stool_result_id" value="{{$sresult->id}}">
        <h3>Stool test:</h3> 
        <div class="row">
            <p>a) Macro:</p>
            <div class="col-md-12"> 
                <input type="text" value="{{$sresult->macro}}" name="macro_stool" class="form-control">
            </div>        
        </div>     <br>
        <div class="row">
            <p>b) Micro:</p>
            <div class="col-md-12 form-group">
                    <label for="">Blood</label>
                    <input type="text" value="{{$sresult->blood}}" name="blood_stool" class="form-control">
            </div>
            <div class="col-md-12 form-group">
                    <label for="">pus</label>
                    <input type="text" value="{{$sresult->pus}}" name="pus_stool" class="form-control">
            </div>
            <div class="col-md-12 form-group">
                <label for="">ova</label>
                <input type="text" value="{{$sresult->ova}}" name="ova_stool" class="form-control">
            </div>
            <div class="col-md-12 form-group">
                    <label for="">Other</label>
                    <input type="text" value="{{$sresult->other}}" name="other_stool" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 form-group">        
                    <label for="">From Machine</label>
                    <input type="text" value="{{$sresult->from_machine}}" name="machine_stool" class="form-control" readonly>
            </div> 
        </div>
        
    <hr>
    </div>