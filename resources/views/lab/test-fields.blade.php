@php
    use App\LabTestRecord;
    use App\BillingList;
    use App\FbpResult;
    use App\StoolResult;
    use App\UrinalResult; 
    use App\User;

    $tests = LabTestRecord::where('patient_registration_no',$pts->patient_registration_no)->where('visit_no',$pts->visit_no)->where('family_help_id',$pts->family_help_id)->get();
    
@endphp
<div class="card card-body">
    <h4 class="">Test Results</h4>
    <hr>

    <form action="{{url('/laboratory/testresults')}}" method="POST">
        {{csrf_field()}}
        <input type="hidden" name="lablist_id" value="{{$id}}">
        <div class="row">    
        @if(!empty($tests))     
            @foreach($tests as $test)  
                    @php 
                    @endphp
                    @if($test->lab_test->name=="Urinal") 
                        @php 
                            $uresult = UrinalResult::where('lab_test_record_id',$test->id)->first(); 
                        @endphp
                        @include('lab.urinal-fields')
                        <hr>
                    @elseif($test->lab_test->name=="Stool")
                        @php 
                            $sresult = StoolResult::where('lab_test_record_id',$test->id)->first();
                        @endphp
                        @include('lab.stool-fields')
                        <hr>
                    @elseif($test->lab_test->name=="FBP")
                        @php 
                            $fresult = FbpResult::where('lab_test_record_id',$test->id)->first();
                        @endphp
                        @include('lab.fbp-fields')
                        <hr>
                    @else 
                    <div class="form-group col-md-4">
                            <div class="row">
                                <div class="col-sm-12">
                                <h3>{{$test->lab_test->name}}:</h3>
                                <input type="hidden" name="test_id[]" value="{{$test->id}}">
                                <input class="form-control" type="text" name="result[]" id="ge" value="{{$test->result}}">
                                </div>
                            </div>
                            <hr>
                    </div>
                    @endif
            @endforeach
        @endif
        </div>
        <br>
        <label>Note from lab:</label>
        <input type="hidden" name="note_from_laboratory_id" value="{{!empty($nflab->id)?$nflab->id:''}}">
        <textarea name="note_from_laboratory" id="nfl" class="form-control" rows="3">{{!empty($nfl)?$nfl:''}}</textarea>
        <label>To doctor:</label>
        <select name="" id="" class="form-control">  
            <option value="">doctor 1</option>
        </select> <br><br>
        @if(Auth::user()->hasRole('laboratorist')||Auth::user()->hasRole('admin'))
        <div class="card-footer">
            @if((Auth::user()->hasRole('laboratorist')||Auth::user()->hasRole('admin'))&&Request::is('laboratory/*'))
            <input class="btn btn-primary" type="submit" value="Save" formaction="{{url('/laboratory/savetestresults')}}">
            <input class="btn btn-success" type="submit" value="Submit">
            @else
            <input class="btn btn-success" onclick="window.history.back()" type="button" value="OK">
            @endif
        </div>
        @endif
    </form>
</div>