@extends('home')
@section('section')
    @php
        $is_request_equipment = session('equipment')?true: false;
    @endphp
    {{-- <div class="table-data__tool-right pull-right">
        <a href="#" data-target="#createRequest" data-toggle="modal">
            <button class="au-btn au-btn-icon au-btn--blue au-btn--small"><i class="zmdi zmdi-plus"></i>Make Request</button>
        </a>
    </div> --}}
    
    <div class="default-tab">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
    
                <a class="nav-item nav-link {{$is_request_equipment?'':'active'}}" id="queue-list-tab" data-toggle="tab" href="#queue-list" role="tab" aria-controls="queue-list"
            aria-selected=" {{$is_request_equipment?'':'active'}}" ><i class="fas fa-list"></i> Lab Queue</a>
            
                <a class="nav-item nav-link" id="request-tab" data-toggle="tab" href="#request-list" role="tab" aria-controls="request-list"
                aria-selected="false"><i class="fas fa-list"></i> Patients Waiting</a>
    
                  <a class="nav-item nav-link  {{$is_request_equipment?'active':''}}" id="result-tab" data-toggle="tab" href="#result-list" role="tab" aria-controls="result"
                aria-selected=" {{$is_request_equipment?'true':''}}"><i class="fas fa-flask"></i> Request Equipment</a>
             
                <a class="nav-item nav-link" id="sop-tab" data-toggle="tab" href="#sop" role="tab" aria-controls="sop"
                aria-selected="false"><i class="fas fa-plus"></i> SOPs</a> 
     
            </div>
        </nav>
        <div class="tab-content pl-3 pt-2" id="nav-tabContent">
        
            <div class="tab-pane fade  {{$is_request_equipment?'':'show active'}}" id="queue-list" role="tabpanel" aria-labelledby="queue-list-tab">
                @include('lab.queue')
            </div>
            <div class="tab-pane fade" id="request-list" role="tabpanel" aria-labelledby="request-tab">               
                @include('lab.results')
            </div>
            
            <div class="tab-pane fade  {{$is_request_equipment?'show active':''}}" id="result-list" role="tabpanel" aria-labelledby="result-tab">
                @include('lab.request-equipment')
            </div>  
        
            <div class="tab-pane fade" id="sop" role="tabpanel" aria-labelledby="sop-tab">
                @include('components.standard_operating_procedure_tab') 
              </div>       
        </div>
    </div>

    @include('lab.request-create')
    @include('lab.request-edit')

@endsection