<!-- DATA TABLE-->
<section class="p-t-20">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="title-5 m-b-35">Patients Queue</h3>

                <div class="row">
                    <div class="col-md-12">
                        <!-- DATA TABLE-->
                        <div class="table-responsive m-b-40">
                            <table id="patient-que" class="table datatable-1 table-borderless table-data3">
                                <thead>
                                    <tr>
                                        <th>Patient Name</th>
                                        <th>Investigation Requested</th>
                                        <th>Status</th>
                                        <th>Total #</th>
                                        <th># Done</th>
                                        <th>Paid #</th>
                                        <th>Total Amount Paid</th>
                                        <th>Queue Time</th>
                                        <th>Waiting Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>001</td>
                                        <td>2018-09-29 05:57</td>
                                        <td>P0019</td>
                                        <td>John Doe</td>
                                        <td>Insurance</td>
                                        <td>Insurance</td>
                                        <td>Insurance</td>
                                        <td>Insurance</td>
                                        <td>Insurance</td>
                                        <td>Insurance</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE                  -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END DATA TABLE-->