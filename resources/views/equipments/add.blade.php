@extends('components.add-form')
@section('form') 
<div class="col-md-6 form-group">
    <label for="">Name</label>
    <input type="text" name="name" value="{{$model->name??''}}" id="" class="form-control">
</div>
<div class="col-md-6 form-group">
    <label for="">Code</label>
    <input type="text" name="code" value="{{$model->code??''}}" id="" class="form-control">
</div>
<div class="col-md-6 form-group">
    <label for="">Purchasing Unit Name</label>
    <input type="text" name="purchasing_unit_name" value="{{$model->purchasing_unit_name??''}}" id="" class="form-control">
</div> 
<div class="col-md-6 form-group">
    <label for="">Selling Unit Name</label>
    <input type="text" name="selling_unit_name" value="{{$model->selling_unit_name??''}}" id="" class="form-control">
</div> 
<div class="col-md-6 form-group">
    <label for="">Unit purchasing price</label>
    <input type="text" name="unit_purchasing_price" value="{{$model->unit_purchasing_price??''}}" id="" class="form-control">
</div> 
<div class="col-md-6 form-group">
    <label for="">Quantity per unit</label>
    <input type="number" name="quantity_per_unit" value="{{$model->quantity_per_unit??''}}" id="" class="form-control">
</div> 
@endsection