@extends('components.add-form')

@section('form')
    <div class="form-group col-md-6">
        <label for="">Quantity</label>
        <input type="number" name="quantity" class="form-control">
    </div>
    <div class="form-group col-md-6">
        <label for="">Code</label>
        <input type="text" name="code" class="form-control">
    </div>
    <div class="form-group col-md-6">
        <label for="">Date</label>
        <input type="date" name="date" id="" class="form-control">
    </div>
@endsection