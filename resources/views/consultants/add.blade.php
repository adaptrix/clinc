@extends('components.add-form')
@section('form')
<div class="col-md-6 form-group">
    <label for="">Name</label>
    <input type="text" name="name" value="{{$model->name??''}}" id="" class="form-control">
</div>
<div class="col-md-6 form-group">
    <label for="">Email</label>
    <input type="text" name="email" value="{{$model->email??''}}" id="" class="form-control">
</div>
<div class="col-md-6 form-group">
    <label for="">Phone Number</label>
    <input type="number" name="phone_no" value="{{$model->phone_no??''}}" id="" class="form-control">
</div>
<div class="col-md-6 form-group">
    <label for="">Password</label>
    <input type="number" name="password" value="{{$model->password??''}}" id="" class="form-control">
</div> 
<div class="col-md-6 form-group">
    <label for="">Consultation fee</label>
    <input type="number" name="fee" value="{{$model->fee??''}}" id="" class="form-control" required>
</div> 
<div class="col-md-6 form-group">
    <label for="">Commission <small>( In percentage )</small></label>
    <input type="number" name="commission" value="{{$model->commission??''}}" id="" class="form-control">
</div>
<div class="col-md-6 form-group">
    <label for="">Address</label>
    <textarea name="address" id="" cols="30" rows="3" class="form-control">{{$model->address??''}}</textarea> 
</div>
@endsection