@extends('components.add-form')
@section('form')
    <div class="col-md-6 form-group">
        <label for="">Description</label>
        <textarea name="description" id="" cols="30" rows="2" class="form-control" required>{{$model->description??null}}</textarea>
    </div>
    <div class="col-md-6 form-group">
        <label for="">Target Type</label>
        <select name="target_type" id="" class="form-control" onchange="toggleTargets(this)"> 
            @foreach ($target_types as $key=>$item)
                <option value="{{$item}}" {{$item==$model->target_type?'selected':''}}>{{$item}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-6 form-group {{$model->target_type==$target_types['select']?'':'hidden'}}" id="targets">
        <label for="">Select Target</label>
        <select name="targets[]" id="" class="form-control select2" multiple>
            @foreach ($staffs as $staff)
                <option value="{{$staff->id}}" {{in_array($staff->id,$selected_target_types)?'selected':''}}>{{$staff->name}}</option>
            @endforeach
        </select>
    </div> 
@endsection