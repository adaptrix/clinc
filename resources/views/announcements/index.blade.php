@extends('layouts.admin')
@section('content')
    <div class="container">
        @include('components.content-header',[
            "header"=>"Announcements",
            "button"=>[
                "name"=>"Create new announcemnt",
                "url"=>$new_announcement_url,
                "color"=>"primary"
            ]
        ])

        @include('components.table',[
            "columns"=>["No","Description","Target","Created On","Action"],
            "collection"=>$announcements,
            "data_columns"=>[
                "iteration",
                "description",
                "target",
                "created_at",
                "buttons"=>[
                    ["name"=>"Edit","id"=>true,"url"=>$edit_announcement_url,"color"=>"primary"],
                    ["name"=>"Dismiss","id"=>true,"url"=>$dismiss_announcement_url,"should_confirm"=>true,"action"=>"dismiss","color"=>"danger"]
                ]
            ]
        ])
    </div>
@endsection