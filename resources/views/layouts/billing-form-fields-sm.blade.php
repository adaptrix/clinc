<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="service_cost" class=" form-control-label">Service cost:</label>
            <div class="input-group"> 
                
                <input type="text" value="{{$servicecost}}" name="service_cost" id="service_cost" class="form-control service_cost" required readonly>
            </div>
        </div>
    </div>
    {{-- <div class="col-md-6">
        <div class="form-group">
            <label for="discount" class=" form-control-label">Discount:</label>
            <div class="input-group"> 
                <input type="text" value="0" name="discount" id="discount" class="form-control discount" required readonly>
            </div>
        </div>
    </div> --}}
    <div class="col-md-6">
        <div class="form-group">
            <label for="amount_paid" class=" form-control-label">Amount Paid:</label>
            <div class="input-group"> 
                <input type="number" onkeyup="calculateChange();" name="amount_paid" id="amount_paid" class="form-control amount_paid" required>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="amount_due" class=" form-control-label">Amount Due:</label>
            <div class="input-group"> 
                <input type="text" value="0" name="amount_due" id="amount_due" class="form-control amount_due" required readonly>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="change" class=" form-control-label">Change:</label>
            <div class="input-group"> 
                <input type="text" value="0" name="change" id="change" class="form-control change" required readonly>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="change_given" class=" form-control-label">Change given:</label>
            <div class="input-group"> 
                <input type="text" name="change_given" onkeyup="calculateRemainingChange()" id="change_given" class="form-control change_given" required>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="change_remaining" class="form-control-label">Change remaining:</label>
            <div class="input-group"> 
                <input type="text" value="0" name="change_remaining" id="change_remaining" class="form-control change_remaining" required readonly>
            </div>
        </div>
    </div>
</div>