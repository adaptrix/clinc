@extends('home')
@section('section')
<div class="default-tab">
    <nav>
      <div class="nav nav-tabs" id="nav-tab" role="tablist">
        {{-- <a class="nav-item nav-link" id="payment-list-tab" data-toggle="tab" href="#payment-list" role="tab" aria-controls="payment-list"
        aria-selected="true" ><i class="fas fa-list"></i> All Payments</a> --}}
  
        <a class="nav-item nav-link active" id="lab-bill-tab" data-toggle="tab" href="#lab-bill" role="tab" aria-controls="lab-bill"
        aria-selected="false"><i class="fas fa-list"></i> (B)illing</a>
  
        <a class="nav-item nav-link" id="pharmacy-bill-tab" data-toggle="tab" href="#pharmacy-bill" role="tab" aria-controls="pharmacy-bill"
        aria-selected="false"><i class="fas fa-list"></i> (N)ot Paid/Refused</a>

        <a class="nav-item nav-link" id="appointment-tab" data-toggle="tab" href="#appointment" role="tab" aria-controls="appointment"
        aria-selected="false"><i class="fas fa-list"></i> (P)Appointments</a>
        <a class="nav-item nav-link" id="sop-tab" data-toggle="tab" href="#sop" role="tab" aria-controls="sop"
        aria-selected="false"><i class="fas fa-plus"></i> SOPs</a> 
{{-- 
        <a class="nav-item nav-link" id="glove-tab" data-toggle="tab" href="#gloves" role="tab" aria-controls="gloves"
        aria-selected="false"><i class="fas fa-list"></i> (G)loves</a> --}}
      </div>
    </nav>
    <div class="tab-content pl-3 pt-2" id="nav-tabContent"> 
      <div class="tab-pane fade show active" id="lab-bill" role="tabpanel" aria-labelledby="lab-bill-tab">
          @include('billings.billinglist')
      </div>

     <div class="tab-pane fade" id="pharmacy-bill" role="tabpanel" aria-labelledby="pharmacy-bill-tab">
        @include('billings.refused')
      </div>
 
     <div class="tab-pane fade" id="appointment" role="tabpanel" aria-labelledby="appointment-tab">
        @include('billings.appointments')
      </div>
 
      <div class="tab-pane fade" id="sop" role="tabpanel" aria-labelledby="sop-tab">
        @include('components.standard_operating_procedure_tab') 
      </div>       
      {{-- <div class="tab-pane fade" id="gloves" role="tabpanel" aria-labelledby="glove-tab">
          @include('billings.gloves')
        </div> --}}
    </div>
  </div>


  @endsection
  @section('scripts')
  @if(!empty($pharmacy_tab))
  <script>
    activateTab("pharmacy-bill");
  </script>
  @endif
  @endsection