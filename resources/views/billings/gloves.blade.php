@php 
    $requests = \App\GloveRequest::where('created_at','>=',Carbon\Carbon::today())->orderBy('created_at','asc')->get();
    $sum = 0; 
    foreach($requests as $request){
        $sum += $request->no_of_gloves;
    }
@endphp
@if(!empty(session('success')))
<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
    <span class="badge badge-pill badge-success">Success</span>
    {{session('success')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
@endif
<section class="p-t-20">
    <div class="row"> 
        <div class="container">
          
            
                                     
            <div class="row">
                <div class="col-md-7">
                    <!-- DATA TABLE-->
                    <h5>Glove requests <br> {{\Carbon\Carbon::today()->format('d/m/Y')}}</h5>
                    <div class="table-responsive m-b-40">
                        <table id="users-table" class="mytable datatable">
                            <tr>
                                <th>Time</th>
                                <th>No of gloves</th>
                            </tr>
                            @foreach ($requests as $request)
                                <tr>
                                <td>{{$request->created_at->format('h:i a')}}</td>
                                <td>{{$request->no_of_gloves}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</section>
<style>
    .table-row:hover td {
        cursor: pointer;
        background-color: grey;
        color: white;
    }

    .active-row td {
        background-color: grey;
        color: white;
    }
</style>