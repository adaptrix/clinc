@php
    //use App\PharmacyList;
    use App\BillingList;
    use App\Patient;
    use App\MedicineRecord;
    use App\LabTestRecord; 
    $billinglist = BillingList::where('type','Pharmacy')
    ->where('created_at','>=',Carbon\Carbon::today())
    ->whereNotIn('billing_id',[0])
    ->where('status','paid')
    ->get();
    $no = 1;
    function calculate($serv)
    {
        $period = explode("/",$serv->days);
        switch ($period[1]) {
            case '7':
                $length = $serv->quantity*$serv->perday*1*$period[0];
                return $length;
                break;
            case '12':
                $length = $serv->quantity*$serv->perday*30*$period[0];
                return $length;
                break;
            case '52':
                $length = $serv->quantity*$serv->perday*7*$period[0];
                return $length;
            default: 
                break;
        }
        return ' '. $serv->quantity*$serv->perday;//*$serv->days;
    }
@endphp
@if(session('success'))
<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
        <span class="badge badge-pill badge-success">Success</span>
        {{session('success')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
@endif
  <section class="p-t-20"> 
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="">Patients List</h3>    
                    <h5 class="">Date: {{Carbon\Carbon::now()->format('d/m/y')}}</h5>    <br>               
                      <div class="row">
                          <div class="col-md-12">
                              <!-- DATA TABLE-->
                              <div class="table-responsive m-b-40">
                                  <table id="patient-que-table" class="datatable mytable">
                                      <thead>
                                          <tr>
                                              {{-- <th>Number</th> --}}
                                              <th>No</th>
                                              <th>Reg no</th>
                                              <th>Name</th>   
                                              <th>Medicine Name</th>  
                                              <th>Dose</th>  
                                              <th>Quantity</th>
                                              <th></th> 
                                          </tr>
                                      </thead>
                                      <tbody>
                                          @foreach ($billinglist as $billingitem)   
                                            @php
                                                $patient = Patient::find($billingitem->patient_registration_no);
                                                $prn = $billingitem->patient_registration_no;
                                                $pvn = $billingitem->visit_no;
                                                if($billingitem->type == "Pharmacy"){
                                                        $servs = MedicineRecord::where('patient_registration_no',$prn)
                                                        ->where('visit_no',$pvn)
                                                        ->where('family_help_id',$billingitem->family_help_id)
                                                        ->whereIn('billing_id',[0,$billingitem->id])
                                                        ->where('status','Paid')
                                                        ->get();
                                                }  
                                                $name = $patient->name; 
                                                if($billingitem->family_help_id!=0)
                                                    $name = $name.'\'s '.$billingitem->familyHelp->relationship;
                                            @endphp
                                            <tr>                                          
                                                {{-- <td>{{$billingitem->id}}</td> --}}
                                                <td>{{$no}}</td>
                                                @php $no++; @endphp
                                                <td>{{$billingitem->patient_registration_no}}</td>
                                                <td>{{$name}}</td>  
                                                <form action="{{url('pharmacy/medicineGiven')}}" method="post">
                                                {{csrf_field()}}
                                                <td>
                                                    @if(!empty($servs))
                                                        @foreach($servs as $serv) 
                                                            {{$serv->item->name}} <br>
                                                        @endforeach
                                                    @endif
                                                </td>  
                                                <td>
                                                @if(!empty($servs))
                                                    @foreach($servs as $serv) 
                                                        {{' '. $serv->quantity.' X'.$serv->perday.' X '.$serv->days}} <br>
                                                        <input type="hidden" name="status[]" value="Given">
                                                        <input type="hidden" name="medicine_record_id[]" value="{{$serv->id}}">
                                                    @endforeach
                                                @endif
                                                </td>
                                                <td>
                                                @if(!empty($servs))
                                                    @foreach($servs as $serv) 
                                                        {{$serv->total}} <br> 
                                                    @endforeach
                                                @endif
                                                </td>
                                                <td>
                                                <input type="hidden" name="billing_list_id" value="{{$billingitem->id}}">
                                                    <button type="submit" href="" class="btn btn-primary">Ok</button></td>
                                                
                                            </form>
                                            </tr> 
                                          @endforeach
                                      </tbody>
                                  </table>
                              </div>
                              <!-- END DATA TABLE-->
                          </div>
                      </div>
                </div> 
            </div> 
    </section>
    <style>
        .table-row:hover td{
            cursor: pointer; 
            background-color: grey;
            color:white;
        }
        
        .active-row td{
            background-color: grey;
            color:white;
        }
    </style>