@extends('layouts.admin')
@section('content')
    <div class="container">
        @include('components.content-header',[
            'header'=>'Payments',
            'button'=>[
                'url'=>$new_payment_url,
                'name'=>'Add new payment',
                'color'=>'primary'
            ]
        ])
        @include('components.table',[
            "columns"=>['No','Description','Amount','Date'],
            "collection"=>$payments,
            "data_columns"=>[
                'iteration',
                'description',
                'amount',
                'date'
            ]
        ])
    </div>
@endsection