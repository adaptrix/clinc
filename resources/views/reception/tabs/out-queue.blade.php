@php
use App\PatientQueueTemporary;
use App\Patient;
$patients = PatientQueueTemporary::where('created_at', '>=', date('Y-m-d').' 00:00:00')->where('status','=','out')->orderBy('created_at','asc')->get();
@endphp

<!-- DATA TABLE-->
<section class="p-t-20">
  <div class="container">
      <div class="row">
          <div class="col-md-12">
              <h3 class="">Patients Went Out</h3>
                <h5>Date:{{Carbon\Carbon::now()->format('d/m/y')}}</h5><br>
              <div class="row">
                  <div class="col-md-12">
                      <!-- DATA TABLE-->
                      <div class="table-responsive m-b-40">
                          <table id="patient-que-table" class="table datatable mytable">
                              <thead>
                                  <tr>
                                      <th>Que #</th> 
                                      <th>Time</th>
                                      <th>reg. #</th>
                                      <th>name</th>
                                      <th>Patient<br>Type</th>
                                      <th>Service Mode</th> 
                                      <th>Alloc. Doctor</th>
                                      <th>Unit Alloc.</th>
                                      <th></th> 
                                  </tr>
                              </thead>
                              <tbody>
                                    @foreach($patients as $pt)
                                        @php
                                            $patient = Patient::find($pt->patient_reg_no);
                                        @endphp
                                    <tr>
                                        <td>{{$pt->patient_que_no}}</td> 
                                        <td>{{$pt->created_at->format('h:i a')}}</td>
                                        <td>{{$pt->patient_reg_no}}</td>
                                        <td>{{$patient->name}}</td>
                                        <td>{{$patient->patient_type}}</td>
                                        <td>{{$pt->service_mode}}</td> 
                                        <td>{{$pt->doctor}}</td>
                                        <td>{{$pt->unit_allocation}}</td>
                                        <td><a href="{{url('reception/patientEntered',$pt->id)}}" class="btn btn-primary">Enter</a></td>
                                   </tr>
                                    @endforeach
                              </tbody>
                          </table>
                      </div>
                      <!-- END DATA TABLE                  -->
                  </div>
              </div>
              {{-- <h3 class="">Lab Queue</h3>
              <h5>Date:{{Carbon\Carbon::now()->format('d/m/y')}}</h5><br>
              <div class="row">
                  <div class="col-md-12">
                      <!-- DATA TABLE-->
                      <div class="table-responsive m-b-40">
                          <table id="patient-que-table" class="table datatable table-borderless table-data3">
                              <thead>
                                  <tr>
                                    <th>Que #</th> 
                                    <th>Time</th>
                                    <th>reg. #</th>
                                    <th>name</th>
                                    <th>Patient<br>Type</th>
                                    <th>Service Mode</th> 
                                    <th>Alloc. Doctor</th>
                                    <th>Unit Alloc.</th>
                                    <th></th>
                                    <th></th>
                                  </tr>
                              </thead>
                              <tbody>
                              </tbody>
                          </table>
                      </div>
                      <!-- END DATA TABLE                  -->
                  </div>
              </div> --}}
          </div>
      </div>
  </div>
</section>
<!-- END DATA TABLE-->
