@php
use App\Service;
use App\PatientType;
use App\ServiceMode;
$services = Service::where('trashed','false')->get();
$patienttypes = PatientType::where('trashed','false')->get();
$servicemodes = ServiceMode::where('trashed','false')->get();
@endphp
<div class="section__content section__content--p30">
    <div class="container-fluid">
        <form enctype="multipart/form-data" method="post" action="{{ url('reception/patient/store') }}" id="register-child-patient">
            {{ csrf_field() }} 
            <input type="hidden" name="create_patient" value="true">
            <div class="row">
                <div class="col-lg-9">
                    <div class="card">  
                            <input type="text" name="type" id="type" value="child" hidden>
                            <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col-md-12"><br> 
                                    <h5>Patient details</h5>
                                    <hr>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="reg_no" class=" form-control-label">Patient Reg No.</label>
                                        <div class="input-group"> 
                                            @php                                        
                                                $nextId = DB::select("show table status like 'patients'");
                                                $reg_no = $nextId[0]->Auto_increment;
                                            @endphp
                                            <input type="text" name="reg_no" id="child_reg_no" class="form-control" required readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="name" class=" form-control-label">Name</label>
                                        <div class="input-group"> 
                                            <input type="text" name="name" id="name" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="gender" class=" form-control-label">Gender</label>
                                        <div class="input-group"> 
                                            <select name="gender" id="gender" class="form-control" required>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                {{-- --}}
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="phone" class=" form-control-label">Mobile No.</label>
                                        <div class="input-group"> 
                                            <input type="text" name="phone" id="phone" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="father_name" class=" form-control-label">Father's Name</label>
                                        <div class="input-group"> 
                                            <input type="text" name="father_name" id="father_name" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="father-occupation" class=" form-control-label">Father's Occupation</label>
                                        <div class="input-group"> 
                                            <input type="text" id="father_occupation" name="father_occupation" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="mother-name" class=" form-control-label">Mother's Name</label>
                                        <div class="input-group"> 
                                            <input type="text" id="mother_name" name="mother_name" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="mother-occupation" class=" form-control-label">Mother's Occupation</label>
                                        <div class="input-group"> 
                                            <input type="text" id="mother_occupation" name="mother_occupation" class="form-control">
                                        </div>
                                    </div>
                                </div> 
                                 <birthday-calculate day="true"></birthday-calculate>
                                <r-p-t id="child"></r-p-t>
                                <div class="col-md-12"><br> 
                                    <h5>Service details</h5>
                                    <hr>
                                </div> 
                                <r-s-d id="child"></r-s-d>
                                <input id="imagedata" type="hidden" name="imagedata" value=""/> 
                            </div>
                            @php
                                $index = 3;
                            @endphp
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card"> 
                        <div class="card-body">
                           <r-c-c id="child"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="card col-lg-12">
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Submit
                            </button>
                            <button type="reset" class="btn btn-danger btn-sm">
                                <i class="fa fa-ban"></i> Reset
                            </button>
                        </div>
                </div>
            </div>        
        </form>
    </div>
</div>