@extends('home')
@section('section')

    <div class="default-tab" id="v-reception">
        <nav>
          <div class="nav nav-tabs" id="nav-tab" role="tablist"> 
            <a class="nav-item nav-link active" id="patient-list-tab" data-toggle="tab" href="#patient-list" role="tab" aria-controls="patient-list"
             aria-selected="true" ><i class="fas fa-list"></i> <span class="underline">P</span>atients Queue</a>

            <a class="nav-item nav-link" id="patient-visit-tab" data-toggle="tab" href="#patient-visit" role="tab" aria-controls="patient-visit"
             aria-selected="false"><i class="fas fa-plus"></i> <span class="underline">R</span>egistered Patient</a>
      
            <a class="nav-item nav-link" id="adult-new-patient-tab" data-toggle="tab" href="#adult-new-patient" role="tab" aria-controls="adult-new-patient"
             aria-selected="false"><i class="fas fa-plus"></i> <span class="underline">A</span>dult New Patient</a>
      
            <a class="nav-item nav-link" id="child-new-patient-tab" data-toggle="tab" href="#child-new-patient" role="tab" aria-controls="child-new-patient"
             aria-selected="false"><i class="fas fa-plus"></i> <span class="underline">C</span>hild New Patient</a>
             
            <a class="nav-item nav-link" id="out-queue-tab" data-toggle="tab" href="#out-queue" role="tab" aria-controls="out-queue"
            aria-selected="false"><i class="fas fa-plus"></i> Patients <span class="underline">o</span>ut</a> 
      
             
            <a class="nav-item nav-link" id="sop-tab" data-toggle="tab" href="#sop" role="tab" aria-controls="sop"
            aria-selected="false"><i class="fas fa-plus"></i> SOPs</a> 
      
            {{-- <a class="nav-item nav-link @if(Auth::user()->hasRole('doctor')) active @endif" id="registered-patient-tab" data-toggle="tab" href="#registered-patient" role="tab" aria-controls="registered-patient"
             aria-selected="false"><i class="fas fa-list"></i> Registered Patients</a> --}}
            
          </div>
        </nav>
        <div class="tab-content pl-3 pt-2" id="nav-tabContent">
       
          <div class="tab-pane fade show active" id="patient-list" role="tabpanel" aria-labelledby="patient-list-tab">
            @include('reception.tabs.queue')
          </div>
      
          <div class="tab-pane fade" id="patient-visit" role="tabpanel" aria-labelledby="patient-visit-tab">
            @include('reception.tabs.visit-form')
          </div>
      
          <div class="tab-pane fade" id="adult-new-patient" role="tabpanel" aria-labelledby="adult-new-patient-tab">
            @include('reception.tabs.create-adult')
          </div>
      
          <div class="tab-pane fade" id="child-new-patient" role="tabpanel" aria-labelledby="child-new-patient-tab">
            @include('reception.tabs.create-child')
          </div>
          <div class="tab-pane fade" id="out-queue" role="tabpanel" aria-labelledby="out-queue-tab">
            @include('reception.tabs.out-queue')  
          </div>       
          <div class="tab-pane fade" id="sop" role="tabpanel" aria-labelledby="sop-tab">
            @include('components.standard_operating_procedure_tab') 
          </div>       
        </div>
    </div>      
    <div class="captured_image"></div>
    <div class="child-captured_image"></div>
    <style>
      .underline{
        text-decoration: underline !important;
        color:black !important;
      }
    </style>
@endsection
@section('scripts')
    <script src="{{asset('js/v-reception.js')}}"></script>
@endsection