@extends('layouts.admin')
@section('content')
    <div class="container"> 
        @include('components.content-header',[
            "header"=>"Standard operating procedures",
            "button"=>[
                "name"=>"Add new standard operating procedure",
                "color"=>"primary",
                "url"=>$new_standard_operating_procedure_url
            ]
        ])
        @include('components.table',[
            "columns"=> ["No","File","Description","Department","Action"],
            "collection"=>$standard_operating_procedures,
            "data_columns"=>[
                "iteration",
                "file_name",
                "description",
                "designation_name",
                "buttons"=>[
                    ["name"=>"Edit","url"=>$edit_standard_operating_procedure_url,"id"=>true,"color"=>"primary"],
                    ["name"=>"Download","url"=>$download_standard_operating_procedure_url,"id"=>true,"color"=>"success"],
                    ["name"=>"Delete","url"=>$remove_standard_operating_procedure_url,"id"=>true,"color"=>"danger","should_confirm"=>true,"action"=>"delete"] 
                ]
            ]
        ])
    </div> 
@endsection