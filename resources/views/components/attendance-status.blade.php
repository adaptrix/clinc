@if ($status == 'present')
    <i class="fa fa-check present"></i>    
@endif
@if ($status == 'absent')
    <i class="fa fa-times absent"></i>
@endif
@if ($status == 'late')
    <i class="fa fa-clock late"></i>  
@endif 
@if ($status == '-') 
    _
@endif