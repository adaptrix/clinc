<div class="row" style="padding-right:16px;padding-left:14px">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-8">
                <h4>{{$header}}</h4>
            </div>
            @if (isset($button))
            <div class="col-md-4 text-right">
                <a href="{{$button['url']}}" class="btn btn-outline-{{$button['color']}}"><i class="fa fa-plus"></i>  {{$button['name']}}</a>
            </div>
            @endif
        </div>
        <hr>
    </div> 
</div>
@include('components.filters')