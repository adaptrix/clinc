@extends('layouts.admin')
@section('content')
    <div class="container">
        {{-- @include('components.alert-message') --}}
        <div class="container-fluid">
            <div class="row" style="margin:auto;">
                <div class="col-md-12">
                    <h4>{{$header}}</h4>
                    <hr>
                </div>
                <div class="col-md-12">
                    <form action="{{ url($form_url) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @if ($is_editing)
                            <input type="hidden" name="id" value="{{$model->id??null}}">
                        @endif
                        <div class="row">
                            @yield('form')
                        </div>
                        <div class="row"> 
                            <div class="col-md-12">
                                <hr>
                                <button class="btn btn-danger btn-sm" type="button" onclick="window.history.back()">Back</button>
                                @if ($is_editing)
                                    <button type="submit" class="btn btn-success btn-sm">Save</button>
                                @else
                                    <button type="submit" class="btn btn-success btn-sm">Submit</button>                                    
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection