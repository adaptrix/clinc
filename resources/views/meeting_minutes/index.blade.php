@extends('layouts.admin')

@section('content')
    <div class="container">
        @include('components.content-header',[
            'header'=>"Meeting Minutes",
            'button'=>[
                "url"=>$new_meeting_minute_url,
                "name"=>"Add new meeting minute",
                "color"=>"primary"
            ]
        ])
        @include('components.table',[
            "columns"=>["No","Date","File","Action"],
            "collection"=>$meeting_minutes,
            "data_columns"=>[
                "iteration",
                "date",
                "file_name",
                "buttons"=>[
                    ["name"=>"Edit","url"=>$edit_meeting_minute_url,"color"=>"primary","id"=>true],
                    ["name"=>"View","url"=>$view_meeting_minute_url,"color"=>"success","id"=>true],
                    ["name"=>"Remove","url"=>$remove_meeting_minute_url,"color"=>"danger","id"=>true,"should_confirm"=>true,"action"=>"remove"]
                ]
            ]
        ])
    </div>
@endsection