@php
    $clinic_name = \App\Setting::where('name','clinic_name')->first();
    $clinic_name = $clinic_name == null ? 'Enter clinic name':$clinic_name->value;

    $service_charge = \App\Setting::where('name','service_charge')->first();
    $service_charge = $service_charge==null?'Enter service charge':$service_charge->value;

    $mkoa = \App\Setting::where('name','mkoa')->first();
    $mkoa = $mkoa==null?'Enter mkoa':$mkoa->value;
    
    $wilaya = \App\Setting::where('name','wilaya')->first();
    $wilaya = $wilaya==null?'Enter wilaya':$wilaya->value;
@endphp
@extends('layouts.admin') @section('content')
    <div class="container"> 
        <form action="{{url('admin/settings/update/')}}" method="post" enctype="multipart/form-data" class="form-horizontal"> 
            {{csrf_field()}}
            <div class="card">
                <div class="card-header">
                    <strong>System </strong> general settings
                </div>
                <div class="card-body card-block"> 
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="clinic_name" class=" form-control-label">Clinic Name:</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="clinic_name" value="{{$clinic_name}}" name="clinic_name" placeholder="Name..." class="form-control">
                                <small class="form-text text-muted"></small>
                            </div>
                        </div>       
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="wilaya" class=" form-control-label">Wilaya:</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="wilaya" value="{{$wilaya}}" name="wilaya" placeholder="Name..." class="form-control">
                                <small class="form-text text-muted"></small>
                            </div>
                        </div>       
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="mkoa" class=" form-control-label">Mkoa:</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="mkoa" value="{{$mkoa}}" name="mkoa" placeholder="Name..." class="form-control">
                                <small class="form-text text-muted"></small>
                            </div>
                        </div>       
                        {{-- <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="discount" class=" form-control-label">Discount:</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="discount" name="discount" placeholder="Text" class="form-control">
                                <small class="form-text text-muted"></small>
                            </div>
                        </div>        --}}
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="service_charge" class=" form-control-label">Service Charge:</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <input type="number" id="service_charge" value="{{$service_charge}}" name="service_charge" placeholder="Tsh" class="form-control">
                                <small class="form-text text-muted"></small>
                            </div>
                        </div>   
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Save
                    </button> 
                </div>
            </div> 
        {{-- <div class="card card-body">
            <div class="row">
                <p>Clear the database and reset the system.</p>
                <div class="col-md-12"><a href="{{url('admin/settings/update')}}" class="btn btn-primary">Clear</a></div>
            </div>
        </div>  --}}
    </div>
</form>
@endsection