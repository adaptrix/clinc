@php
    use App\Supplier;
    
    $suppliers = Supplier::all();
@endphp
@extends('layouts.admin') 
@section('content')
<div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="container">
                    <a href="{{url('admin/pharmacy/suppliers/new')}}" class="btn btn-primary">Add Supplier</a>
        
                    </div>
                </div><br><br>
                <!-- DATA TABLE-->
                <div class="table-responsive m-b-40">
                    <table id="patient-que-table" class="datatable table table-borderless table-data3">
                        <thead>
                            <tr>
                                <th>Number</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Contact Person</th>
                                <th>Phone</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($suppliers as $supplier)
                            <tr class="table-row">
                            <td>{{$supplier->id}}</td>
                                <td>{{$supplier->name}}</td>
                                <td>{{$supplier->address}}</td>
                                <td>{{$supplier->contact_person}}</td>
                                <td>{{$supplier->phone_no}}</td>
                                <td><a href="{{ url('admin/pharmacy/supplier/view',$supplier->id) }}" class="btn btn-primary">View</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- END DATA TABLE                  -->
            </div>
        </div>
        
        </div>
@endsection