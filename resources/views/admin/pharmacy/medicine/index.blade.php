@php
    use App\Medicine;
    use App\MedicineCategory;
    $categories = MedicineCategory::all();
    $medicines = Medicine::where('trashed','false')->get();
    $i=1;
@endphp
@extends('layouts.admin') @section('content')
<div class="container">
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="container">
            <a href="{{url('admin/pharmacy/medicines/new')}}" class="btn btn-primary">Add Medicine</a>

            </div>
        </div><br><br>
        <!-- DATA TABLE-->
        <div class="table-responsive m-b-40">
            <table id="patient-que-table" class="datatable mytable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th> 
                        <th>Type</th>
                        <th>Code 1</th>
                        <th>Code 2</th>
                        {{-- <th>Description</th> --}}
                        <th>Unit buying <br>Price</th>
                        <th>Unit selling <br>Price</th>
                        <th>Batch number</th>
                        <th>Expiry date</th>
                        <th>Remaining in stock</th>
                        <th></th> 
                    </tr>
                </thead>
                <tbody>
                    @foreach($medicines as $medicine)
                        @php
                            
                        @endphp
                    <tr class="table-row">
                        <td>{{$i}}</td>
                        <td>{{$medicine->name}}</td> 
                        <td>{{$medicine->category->name}}</td> 
                        <td>{{$medicine->code}}</td>
                        <td>{{$medicine->code_2}}</td>
                        {{-- <td>{{$medicine->description}}</td> --}}
                        <td>{{$medicine->unit_purchasing_price}}</td>
                        <td>{{$medicine->selling_price}}</td>
                        <td>
                            @foreach ($medicine->medicineBatches as $batch)
                                {!!$batch->batch_number.'</br>'!!}
                            @endforeach
                        </td>
                        <td>
                            @foreach ($medicine->medicineBatches as $batch)
                            {!!Carbon\Carbon::parse($batch->expiry_date)->format('d/m/Y').'</br>'!!}
                        @endforeach
                        </td>
                        <td>
                            @foreach ($medicine->medicineBatches as $batch)
                            {!!$batch->remaining_quantity.' '.$medicine->selling_unit_name.'s'.'</br>'!!}
                        @endforeach
                        </td>
                        <td>
                            <a href="{{url('admin/pharmacy/medicines/view',$medicine->id)}}" class="btn btn-sm btn-success">View</a>
                            <a href="{{url('admin/pharmacy/medicines/view',$medicine->id)}}" class="btn btn-sm btn-primary">Edit</a>
                            <a href="{{url('admin/pharmacy/medicines/delete',$medicine->id)}}" class="btn btn-sm btn-danger">Delete</a>
                        </td>
                    </tr>
                    @php
                        $i++;
                    @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- END DATA TABLE                  -->
    </div>
</div>

</div>
@endsection