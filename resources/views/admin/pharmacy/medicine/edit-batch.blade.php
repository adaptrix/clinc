@php
    $batch = \App\MedicineStockBatch::find($id);
@endphp
@extends('layouts.admin')
@section('content')
    <div class="container"> 
        <div class="card card-body">
            @php 
            @endphp
            <h4>Batch Details</h4>
            <hr>
            <form id="" enctype="multipart/form-data" method="post" action="{{url('admin/pharmacy/medicines/editBatch')}}">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{$id}}">
                <div class="modal-body">
                    <div class="row form-group"> 
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Number of units bought:</label>
                                <div class="input-group"> 
                                    <input type="number" name="number of units bought" value="{{!empty($batch->number_of_units_bought)?$batch->number_of_units_bought:''}}" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Expiry date:</label>
                                <div class="input-group"> 
                                    <input type="date" name="expiry_date" value="{{!empty($batch->expiry_date)?$batch->expiry_date:''}}" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Batch number:</label>
                                <div class="input-group"> 
                                    <input type="text" value="{{!empty($batch->batch_number)?$batch->batch_number:''}}" name="batch_number"class="form-control">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="medicine_id" value="{{$id}}">
                        <div class="col-md-2">
                            <br>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection