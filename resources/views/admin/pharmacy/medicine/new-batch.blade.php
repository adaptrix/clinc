@extends('layouts.admin')
@section('content')
    <div class="container"> 
        <div class="card card-body">
            @php 
            @endphp
            <h4>Batch Details</h4>
            <hr>
            <form id="" enctype="multipart/form-data" method="post" action="{{url('admin/pharmacy/medicines/newBatch')}}">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="row form-group">
                        <input type="text" name="id" id="service_id" hidden>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Number of units bought:</label>
                                <div class="input-group"> 
                                    <input type="number" name="number of units bought" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Expiry date:</label>
                                <div class="input-group"> 
                                    <input type="date" name="expiry_date" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                    <label>Batch number:</label>
                                <div class="input-group"> 
                                    <input type="text" name="batch_number"class="form-control">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="medicine_id" value="{{$id}}">
                        <div class="col-md-2">
                            <br>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection