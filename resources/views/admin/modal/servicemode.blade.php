<div class="modal fade" id="servicemodeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Service Mode</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="servicemode-form" enctype="multipart/form-data" method="post" action="{{ url('/admin/servicemode/create') }}" >
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="row form-group">
                        <input type="text" name="id" id="service_mode_id" hidden>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="service_mode" class=" form-control-label">Service Mode</label>
                                <div class="input-group">
                                    
                                    <input type="text" name="service_mode" id="service_mode" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="service_mode" class=" form-control-label">Price</label>
                                <div class="input-group">
                                    
                                    <input type="text" name="price" id="price" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end edit Category -->
    