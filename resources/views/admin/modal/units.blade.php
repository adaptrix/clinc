<div class="modal fade" id="unitModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Unit Allocation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="units-form" enctype="multipart/form-data" method="post" action="{{ url('/admin/unit/create') }}" >
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="row form-group">
                        <input type="text" name="id" id="unit_id" hidden>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="service" class=" form-control-label">Unit</label>
                                <div class="input-group">
                                    {{-- <div class="input-group-addon">
                                        <i class="fa fa-id-card"></i>
                                    </div> --}}
                                    <input type="text" name="unit" id="unit" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end edit Category -->
