@extends('layouts.admin')
@section('content')
@php 
    use App\User;
    $users = User::all();
@endphp
<div class="container">
    <div class="card card-body">

        <form class="" enctype="multipart/form-data" method="post" action="{{ url('/admin/user/create') }}">
            {{ csrf_field() }}
            <input type="text" name="id" id="user-id" hidden>
            <div class="modal-body">

                <div class="row form-group">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="employee_id" class=" form-control-label">Employee No.</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-id-card"></i>
                                </div>
                                <input type="text" name="employee_id" value="{{old('employee_id')}}" id="employee_id" class="form-control" required>        
                            </div>
                        </div> 
                     </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name" class=" form-control-label">Name</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <input type="text" name="name" id="name" class="form-control" value="{{old('name')}}" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email" class=" form-control-label">Email</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-at"></i>
                                </div>
                                <input type="email" name="email" value="{{old('email')}}" id="email" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="role" class=" form-control-label">User Role</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <select name="role" id="role" class="form-control" required>
                                    <option value="">Please select...</option>
                                    <option value="receptionist">Receptionist</option>
                                    <option value="laboratorist">Laboratorist</option>
                                    <option value="pharmacist">Pharmacist</option>
                                    <option value="doctor">Doctor</option>
                                    <option value="admin">Administrator</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="password" class=" form-control-label">Password</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-lock"></i>
                                </div>
                                <input type="password" id="password" name="password" value="{{old('password')}}" class="form-control">
                            </div>
                        </div>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="passconfrim" class=" form-control-label">Confirm Password</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-lock"></i>
                                </div>
                                <input type="password" id="passconfirm" name="password_confirmation" class="form-control">
                            </div>
                        </div>
                        
                        @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Add User</button>
                </div>
        </form>
    </div> 
      <!-- DATA TABLE-->
  <section class="p-t-20">
        <div class="container">
            <div class="row">
                    
                <div class="col-md-12">
                                     
                    <div class="row">
                        <div class="col-md-12">
                            <!-- DATA TABLE-->
                            <div class="table-responsive m-b-40">
                                <table id="users-table" class="mytable datatable">
                                    <thead>
                                        <tr>
                                            <th>Employee ID</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Role</th> 
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>    
                                        @foreach($users as $user)
                                            <tr>  
                                                <td>{{$user->id}}</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->roles[0]->name}}</td>
                                                <td>
                                                    <a href="{{url('admin/users/edit',$user->id)}}" class="btn btn-primary">Edit</a>
                                                    <a href="{{url('admin/users/delete',$user->id)}}" class="btn btn-danger">Delete</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE                  -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
    <!-- END DATA TABLE-->
   
@endsection