@php
    $to =!empty($to)?$to:\Carbon\Carbon::today()->format('Y-m-d');
    $from = !empty($from)?$from:\Carbon\Carbon::today()->subDays(7)->format('Y-m-d'); 
    
    $meds = \App\Medicine::orderBy('name','asc')->get();
    $no = 1;
@endphp
@extends('layouts.admin') 
@section('content')
<div class="container card card-body" style="padding:20px;margin:25px;">
    <div class="row"> 
        <form action="{{url('admin/reports/stock/filter')}}" method="get">
            {{ csrf_field() }}
            <div class="row col-lg-12">
                <div class="col-lg-4 form-group">
                    <label for="">From:</label>
                    <input type="date" value="{{$from}}" name="from" id="" class="form-control input-sm" style="font-size:12px;">
                </div>
                <div class="col-lg-4 form-group">
                    <label for="">To:</label>
                    <input type="date" value="{{$to}}" name="to" id="" class="form-control input-sm" style="font-size:12px;">
                </div>
                <div class="col-lg-4 form-group">
                    <label for=""></label><br>
                    <button class="form-control btn btn-md btn-primary" style="margin-top:10px;">Update</button>
                </div>
            </div>
        </form>
        <div class="row col-lg-12"> 
            <table class="mytable datatable" style="margin-left:20px;">
                <tr>
                    <th>No</th>
                    <th>Medicine Name</th>
                    <th>Quantity bought</th>
                    <th>Quantity sold</th>
                    <th>Remaining in stock</th>
                    <th>Total amount in sales</th>
                    <th>Total profit made</th>
                </tr>
                @foreach ($meds as $med)
                    @php 
                        $remaining_quantity = 0;
                        $med_records = \App\MedicineRecord::whereBetween('created_at',[$from,$to])
                                            ->where('medicine_id',$med->id)
                                            ->get();
                        foreach($med_records as $med_record){
                            $stock_uses = $med_record->medicineStockUses->sortByDesc('created_at');
                            foreach($stock_uses as $med_stock_use){
                                $remaining_quantity = $med_stock_use->medicineStockBatchMedicineUses->total_remaining_quantity;
                            }
                        } 
                        $quantity_sold_sum = 0;
                        foreach($med_records as $med_record){
                            foreach($med_record->medicineStockUses as $med_stock_use){
                                $quantity_sold_sum += $med_stock_use->medicineStockBatchMedicineUses->used_quantity;
                            }
                        } 

                        $quantity_bought_sum =  \App\MedicineStockBatch::where('medicine_id',$med->id)
                                                                    ->sum('total_quantity');
                        $total_sales = 0;
                        foreach($med_records as $med_record){
                            foreach($med_record->medicineStockUses as $med_stock_use){
                                $total_sales += $med_stock_use->medicineStockBatchMedicineUses->total_cost;
                            }
                        }  
                        $total_buying_cost = 0;
                        foreach($med_records as $med_record){
                            foreach($med_record->medicineStockUses as $med_stock_use){
                                $total_buying_cost += $med_stock_use->medicineStockBatchMedicineUses->total_buying_price;
                            }
                        }   
                        $total_profit = $total_sales - $total_buying_cost;
                    @endphp 
                    <tr>
                        <td>{{$no}}</td>
                        <td>{{$med->name}}</td>
                        <td>{{$quantity_bought_sum}}</td>
                        <td>{{$quantity_sold_sum}}</td>
                        <td>{{$remaining_quantity}}</td>
                        <td>{{$total_sales}}</td>
                        <td>{{$total_profit}}</td>
                    </tr>
                    @php
                        $no++;
                    @endphp
                @endforeach
            </table>
        </div>
    </div> 
</div>


@endsection