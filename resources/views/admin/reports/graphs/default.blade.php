@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5>Filters</h5>
                <hr>
                <form action="{{request()->fullUrl()}}" method="GET">
                    <div class="row">
                        <div class="col-md-3 form-group">
                            <label for="">From</label>
                            <input type="date" name="from" value="{{$from}}" id="" class="form-control">
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="">To</label>
                            <input type="date" name="to" value="{{$to}}" id="" class="form-control">
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="">X axis <small>( Y axis is number of patients) </small></label>
                            <select name="diagnosis_id" id="" class="form-control select2">
                                @foreach ($diagnoses as $item)
                                    <option value="{{$item->id}}" {{$diagnosis_id==$item->id?'selected':''}}>{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3 form-group">
                            <button type="submit" class="btn btn-outline-success button-margin"><i class="fa fa-filter"></i>Filter</button>
                        </div>
                    </div>
                </form>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {!! $chartjs->render() !!}
            </div>
        </div>
    </div>
@endsection