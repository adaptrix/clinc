@php
    use App\PatientQueueTemporary;
    use App\Patient;
    $to =!empty($to)?$to:\Carbon\Carbon::today()->format('Y-m-d');
    $from = !empty($from)?$from:\Carbon\Carbon::today()->subDays(7)->format('Y-m-d'); 
    
    $patients= \App\Patient::whereBetween('created_at',[$from,$to])->orderBy('created_at','asc')->get();
    // $patients = PatientQueueTemporary::where('created_at','>=',Carbon\Carbon::today())->whereIn('status',['waiting','out','entered'])->orderBy('service_mode','asc')->orderBy('created_at','asc')->get();
    $patients = PatientQueueTemporary::whereBetween('created_at',[$from,$to])->orderBy('service_mode','asc')->orderBy('created_at','asc')->get();
    $no = 1;
@endphp
@extends('layouts.admin') 
@section('content')
<div class="container card card-body" style="padding:20px;margin:25px;">
    <div class="row"> 
        <form action="{{url('admin/reports/consultations/filter')}}" method="get">
            {{ csrf_field() }}
            <div class="row col-lg-12">
                <div class="col-lg-3 form-group">
                    <label for="">From:</label>
                    <input type="date" value="{{$from}}" name="from" id="" class="form-control input-sm" style="font-size:12px;">
                </div>
                <div class="col-lg-3 form-group">
                    <label for="">To:</label>
                    <input type="date" value="{{$to}}" name="to" id="" class="form-control input-sm" style="font-size:12px;">
                </div>
                <div class="col-lg-3 form-group">
                    <label for="">Doctor:</label>
                    <select name="" id=""  class="form-control input-sm">
                        <option value="">Doctor 1</option>
                    </select>
                </div>
                <div class="col-lg-3 form-group">
                    <label for=""></label><br>
                    <button class="form-control btn btn-md btn-primary" style="margin-top:10px;">Update</button>
                </div>
            </div>
        </form>
        <div class="table-responsive m-b-40">
                <table id="patient-doc-que" class="mytable datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Time/<br>Registered</th> 
                                <th>Status</th>
                                <th>Reg #</th>
                                <th>name</th>
                                <th>Type</th>
                                <th>Service Mode</th>
                                {{-- <th>Waiting Time</th> --}}
                                {{-- <th></th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($patients as $pt)
                                @php
                                    $patient = Patient::find($pt->patient_reg_no);
                                @endphp
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$pt->created_at->format('h:i a')}}</td> 
                                <td>{{$pt->status}}</td>
                                <td>{{$pt->patient_reg_no}}</td>
                                <td>{{$patient->name}}</td>
                                <td>{{$patient->patient_type}}</td>
                                <td>{{$pt->service_mode}}</td>
                                {{-- <td><a href="{{route('patient-details',$pt->id)}}" class="btn btn-primary">Attend to</a></td> --}}
                            </tr>
                            @php
                                $no++;
                            @endphp
                            @endforeach
                        </tbody>
                    </table>
        </div>
    </div> 
</div>


@endsection