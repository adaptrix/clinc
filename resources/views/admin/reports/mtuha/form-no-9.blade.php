<div class="report-form-header text-center">
        <p>Taarifa ya DTC</p>
    </div>
    <div class="report-form-subheader text-center row">
        <div class="col-md-4 text-center">
            Jina la kituo: <strong style="text-decoration:underline;">{{$kituo}}</strong>
        </div>
        <div class="col-md-4 text-center">
            Wilaya:  <strong style="text-decoration:underline;">{{$wilaya}}</strong>
        </div>
        <div class="col-md-4 text-center"> 
            Mkoa:  <strong style="text-decoration:underline;">{{$mkoa}}</strong>
        </div>
        <div class="col-md-6 text-center mt-2">
            Mwezi:   <strong style="text-decoration:underline;">{{$month_formatted}}</strong>
        </div>
        <div class="col-md-6 text-center mt-2">
            Mwaka:   <strong style="text-decoration:underline;">{{$year}}</strong>
        </div> 
    </div>
 <table class="reporttable" style="width:90%; margin:auto;font-size:15px">
    <tr>
        <th rowspan="2">Na.</th>
        <th rowspan="2">Maelezo</th>
        <th colspan="3">Umri chini <br> chini ya mwezi 1</th>
        <th colspan="3">Umri <br> mwezi 1 <br> hadi umri <br>chini ya <br> mwaka 1</th>
        <th colspan="3">Umri <br> mwaka 1 <br> hadi umri <br>chini ya <br> miaka 5</th>  
        <th colspan="3">Jumla</th> 
    </tr>
    <tr> 
        <th>ME</th>
        <th>KE</th>
        <th>Jumla</th> 
        <th>ME</th>
        <th>KE</th>
        <th>Jumla</th> 
        <th>ME</th>
        <th>KE</th>
        <th>Jumla</th>  
        <th>Jumla <br> kuu</th>  
    </tr> 
    <tr>
        <td>1</td>
        <td>Idadi ya wagonjwa waliotibiwa DTC</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2</td>
        <td>Idadi ya wagonjwa waliotibiwa DTC walio na upungufu mkubwa wa maji na chumvichumvi mwilini</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>3</td>
        <td>Idadi ya wagonjwa waliotibiwa DTC walio na upungufu kiasi wa maji na chumvichumvi mwilini</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>4</td>
        <td>idadi ya wagonjwa walio na damu katika kinyesi</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>5</td>
        <td>idadi ya wagonjwa waliopewa rufaa</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>6</td>
        <td>Idadi ya wagonjwa waliopatiwa zinki</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>7</td>
        <td>Idadi ya wagonjwa waliopatiwa paketi za ORS</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>8</td>
        <td>Idadi ya wagonjwa waliolazwa</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>9</td>
        <td>idadi ya wagonjwa waliofia DTC</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</table>
<div class="container p-5 m-1">
    <div class="row">
        <div class="col-md-4 mt-2">
            Jina la Mtayarishaji wa taarifa: <strong>{{Auth::user()->name}}</strong>
        </div>
        <div class="col-md-4 mt-2">
            Kada: .............................................................
        </div>
        <div class="col-md-4 mt-2">
            Wadhifa: ...........................................................
        </div>
        <div class="col-md-4 mt-2">
            Sahihi: ..........................................................
        </div>
        <div class="col-md-4 mt-2">
            Tarehe: <strong>{{\Carbon\Carbon::now()->format('d/m/Y')}}</strong>
        </div>
        <div class="col-md-4 mt-2">
            Imepitiwa na: .............................................................
        </div>
        <div class="col-md-4 mt-2">
            Namba ya simu ya Kituo/Wilaya/Mkoa: .....................................................
        </div>
        <div class="col-md-4 mt-2">
            Taarifa imepokelewa wilayani tarehe: ........................................................
        </div>
    </div>
</div>
