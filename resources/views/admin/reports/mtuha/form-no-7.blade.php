<div class="report-form-header">
    <p style="font-size:36px !important; font-weight:700;margin-left:29px;">Ripoti ya Mwezi ya Ufuatiliaji wa Watoto</p>
</div> 
<div class="report-form-subheader text-center row">
    <div class="col-md-4 text-center">
        Jina la kituo: <strong style="text-decoration:underline;">{{$kituo}}</strong>
    </div>
    <div class="col-md-4 text-center">
        Wilaya:  <strong style="text-decoration:underline;">{{$wilaya}}</strong>
    </div>
    <div class="col-md-4 text-center"> 
        Mkoa:  <strong style="text-decoration:underline;">{{$mkoa}}</strong>
    </div>
    <div class="col-md-6 text-center mt-2">
        Mwezi:   <strong style="text-decoration:underline;">{{$month_formatted}}</strong>
    </div>
    <div class="col-md-6 text-center mt-2">
        Mwaka:   <strong style="text-decoration:underline;">{{$year}}</strong>
    </div> 
</div>
<table class="reporttable1" style="margin:auto; width:90%;font-size:15px">
    <tr>
        <th rowspan="2">Na</th>
        <th rowspan="2">Maelezo</th>
        <th rowspan="2"></th>
        <th colspan="3" class="text-center">Idadi</th>
    </tr>
    <tr>  
        <th class="text-center">ME</th>
        <th class="text-center">KE</th>
        <th class="text-center">Jumla</th>
    </tr>
    <tr>
        <td>1</td>
        <td>Idadi ya watoto walioandikishwa na kupewa vyeti vya kuzaliwa</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th>2</th>
        <th>Aina ya Chanjo kwa Umri</th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
    <tr>
        <td>2a</td>
        <td>BCG Umri mwaka &lt;1 (Ndani ya eneo lahuduma)</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2b</td>
        <td>BCG Umri mwaka 1+ (Ndani ya eneo la huduma)</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2c</td>
        <td>BCG Umri mwaka &lt;1 (Nje ya eneo la huduma)</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2d</td>
        <td>BCG Umri mwaka 1+ (Nje ya eneo la huduma)</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2e</td>
        <td>Polio Umri mwaka &lt;1 (Ndani ya eneo la huduma)</td>
        <td>Dozi 0</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 1</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 2</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 3</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2f</td>
        <td>Polio Umri mwaka 1+(Ndani ya eneo la huduma)</td>
        <td>Dozi 1</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 2</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 3</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2g</td>
        <td>Polio Umri mwaka &lt;1(Nje ya eneo la huduma)</td>
        <td>Dozi 0</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 1</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 2</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 3</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2h</td>
        <td>Polio Umri mwaka 1+(Nje ya eneo la huduma)</td>
        <td>Dozi 1</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 2</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 3</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2i</td>
        <td>Polio ya sindano Miezi 18 (Ndani ya eneo la huduma)</td>
        <td>Dozi 1</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2j</td>
        <td>Polio ya sindano Miezi 18 (Nje ya eneo la huduma)</td>
        <td>Dozi 1</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2k</td>
        <td>Rota umri wiki 6 hadi 15(Ndani ya eneo la huduma)</td>
        <td>Dozi 1</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2l</td>
        <td>Rota umri wiki 6 hadi 15 (Nje ya eneo la huduma)</td>
        <td>Dozi 1</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2m</td>
        <td>Rota umri wiki 10 hadi 32 (Nje ya eneo la huduma)</td>
        <td>Dozi 2</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2n</td>
        <td>Rota umri wiki 10 hadi 32 (Ndani ya eneo la huduma)</td>
        <td>Dozi 2</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2o</td>
        <td>PENTA Umri mwaka &lt;1 (Ndani ya eneo la huduma)</td>
        <td>Dozi 1</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 2</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 3</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2p</td>
        <td>PENTA Umri mwaka 1+ (Ndani ya eneo la huduma)</td>
        <td>Dozi 1</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 2</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 3</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2q</td>
        <td>PENTA Umri mwaka &lt;1 (Nje ya eneo la huduma)</td>
        <td>Dozi 1</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 2</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 3</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2r</td>
        <td>PENTA Umri mwaka 1+ (Nje ya eneo la huduma)</td>
        <td>Dozi 1</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 2</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 3</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2s</td>
        <td>Pneumococcal (PCV13) &lt; 1(Ndani ya eneo la huduma)</td>
        <td>Dozi 1</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 2</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 3</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2t</td>
        <td>Pneumococcal (PCV13) 1+ (Ndani ya eneo la huduma)</td>
        <td>Dozi 1</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 2</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 3</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2u</td>
        <td>Pneumococcal (PCV13) &LT;1 (Nje ya eneo la huduma)</td>
        <td>Dozi 1</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 2</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 3</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2v</td>
        <td>Pneumococcal (PCV13) 1+ (Nje ya eneo la huduma)</td>
        <td>Dozi 1</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 2</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Dozi 3</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2w</td>
        <td>Surua/Rubela umri miezi 9 (Ndani ya eneo la huduma)</td>
        <td>Dozi 1</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2x</td>
        <td>Surua/Rubela umri miezi 9 (Nje ya eneo la huduma)</td>
        <td>Dozi 1</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2y</td>
        <td>Surua/Rubela umri miezi 18 (Ndani ya eneo la huduma)</td>
        <td>Dozi 2</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2z</td>
        <td>Surua/Rubela umri miezi 18 (Ndani ya eneo la huduma)</td>
        <td>Dozi 2</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th>3</th>
        <th colspan="5">Hali ya Chanjo ya Pepo punda kwa mama wakati wa kujifungua</th>
    </tr>
    <tr>
        <td></td>
        <td><strong>Idadi ya watoto walioandikishwa</strong></td>
        <td><strong>ME</strong></td>
        <td><strong>KE</strong></td>
        <td colspan="2"><strong>Jumla</strong></td>
    </tr>
    <tr>
        <td>3a</td>
        <td>Waliokingwa</td>
        <td></td>
        <td></td>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td>3b</td>
        <td>Waiokuwa na kinga</td>
        <td></td>
        <td></td>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td>3c</td>
        <td>Haijulikani</td>
        <td></td>
        <td></td> 
        <td colspan="2"></td>
    </tr>
    <tr>
        <td colspan="6"></td>
    </tr>
    <tr>
        <th>4</th>
        <th colspan="5">Mahudhuri na uwiano wa uzito, umri na urefu; <br> umri chini ya mwaka 1</th>
    </tr>
    <tr>
        <td><strong></strong></td>
        <td><strong>Maelezo</strong></td>
        <td><strong></strong></td>
        <td><strong>ME</strong></td>
        <td><strong>KE</strong></td>
        <td><strong>Jumla</strong></td>
    </tr>  
    <tr>
        <td>4a</td>
        <td>Jumla ya Mahudhurio ya Watoto</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td rowspan="3">4b</td>
        <td rowspan="3">Uwiano wa uzito kwa umri</td>
        <td>&gt;80% au &gt;-2SD</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr> 
        <td>60-80% au -2 hadi -SD</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr> 
        <td>&lt;60% au &lt;-3SD</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td rowspan="3">4c</td>
        <td rowspan="3">Uwiwano wa uzito kwa urefu</td>
        <td>&gt;-2SD</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr> 
        <td>-2 hadi -3SD</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr> 
        <td>&lt;-3SD</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td rowspan="3">4d</td>
        <td rowspan="3">Uwiano wa urefu kwa umri</td>
        <td>&gt;-2SD</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr> 
        <td>-2 hadi -3SD</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr> 
        <td>&lt;-3SD</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="5"></td> 
    </tr>
    <tr>
        <th>5</th>
        <th colspan="5">Mahudhuri na uwiano wa uzito, umri na urefu; <br> umri mwaka 1 mpaka 5</th>
    </tr> 
    <tr>
        <td>5a</td>
        <td>Jumla ya Mahudhurio</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td rowspan="3">5b</td>
        <td rowspan="3">Uwiano wa uzito kwa umri</td>
        <td>&gt;80% au &gt;-2SD</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr> 
        <td>60-80% au -2 hadi -SD</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr> 
        <td>&lt;60% au &lt;-3SD</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td rowspan="3">5c</td>
        <td rowspan="3">Uwiwano wa uzito kwa urefu</td>
        <td>&gt;-2SD</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr> 
        <td>-2 hadi -3SD</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr> 
        <td>&lt;-3SD</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td rowspan="3">5d</td>
        <td rowspan="3">Uwiano wa urefu kwa umri</td>
        <td>&gt;-2SD</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr> 
        <td>-2 hadi -3SD</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr> 
        <td>&lt;-3SD</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th>6</th>
        <th colspan="5">Nyongeza ya viatamin A</th>
    </tr>
    <tr>
        <td><strong></strong></td>
        <td><strong>Maelezo</strong></td>
        <td><strong></strong></td>
        <td><strong>ME</strong></td>
        <td><strong>KE</strong></td>
        <td><strong>Jumla</strong></td>
    </tr>  
    <tr>
        <td>6a</td>
        <td>Watoto wa umri wa miezi 6</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>6b</td>
        <td>Watoto wa umri wa chini ya mwaka 1</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>6c</td>
        <td>Watoto wa umri zaidi ya mwaka 1-5</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th>7</th>
        <th colspan="5">Waliopewa Mebendazole/ Albendazole</th>
    </tr>
    <tr>
        <td><strong></strong></td>
        <td><strong>Maelezo</strong></td>
        <td><strong></strong></td>
        <td><strong>ME</strong></td>
        <td><strong>KE</strong></td>
        <td><strong>Jumla</strong></td>
    </tr>  
    <tr>
        <td>7a</td>
        <td>Watoto wa umri wa mwaka 1 hadi 5</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th>8</th>
        <th colspan="5">Ulishaji wa watoto wachanga</th>
    </tr>
    <tr>
        <td><strong></strong></td>
        <td><strong>Maelezo</strong></td>
        <td><strong></strong></td>
        <td><strong>ME</strong></td>
        <td><strong>KE</strong></td>
        <td><strong>Jumla</strong></td>
    </tr>  
    <tr>
        <td>8a</td>
        <td>Watoto wachanga chini ya miezi sita wanaonyonya maziwa ya mama pekee (EBF)</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>8b</td>
        <td>Watoto wachanga wanaopewa maziwa mbadala</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th>9</th>
        <th colspan="5">Taarifa za PMTCT / LLIN</th>
    </tr>
    <tr>
        <td><strong></strong></td>
        <td><strong>Maelezo</strong></td>
        <td><strong></strong></td>
        <td><strong>ME</strong></td>
        <td><strong>KE</strong></td>
        <td><strong>Jumla</strong></td>
    </tr>  
    <tr>
        <td>9a</td>
        <td>Watoto waliozaliwa na mama mwenye maambukizi ya VVU/ watoto wenye HEID no.</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>9b</td>
        <td>Watoto waliohamishiwa Kliniki ya huduma na matibabu kwa wenye VVU(CTC)</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>9c</td>
        <td>Watoto waliopatiwa LLIN</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr> 
</table>

<div class="container p-5 m-1">
    <div class="row">
        <div class="col-md-4 mt-2">
            Jina la Mtayarishaji wa taarifa: <strong>{{Auth::user()->name}}</strong>
        </div>
        <div class="col-md-4 mt-2">
            Kada: .............................................................
        </div>
        <div class="col-md-4 mt-2">
            Wadhifa: ...........................................................
        </div>
        <div class="col-md-4 mt-2">
            Sahihi: ..........................................................
        </div>
        <div class="col-md-4 mt-2">
            Tarehe: <strong>{{\Carbon\Carbon::now()->format('d/m/Y')}}</strong>
        </div>
        <div class="col-md-4 mt-2">
            Imepitiwa na: .............................................................
        </div>
        <div class="col-md-4 mt-2">
            Namba ya simu ya Kituo/Wilaya/Mkoa: .....................................................
        </div>
        <div class="col-md-4 mt-2">
            Taarifa imepokelewa wilayani tarehe: ........................................................
        </div>
    </div>
</div>