
@extends('layouts.admin') 
@section('content')
<div class="container" style="padding:20px;">
    <div class="row">
        <form action="{{url('admin/reports/mtuha')}}" class="col-md-12" method="get">
            {{ csrf_field() }}
            <div class="row">
                <div class="form-group col-md-3">
                    <label for="">Form number:</label>
                    <select name="form_no" class="form-control" id="">
                        <option value="1" {{$form_no==1?'selected':''}}>1</option>
                        <option value="2" {{$form_no==2?'selected':''}}>2</option>
                        <option value="3" {{$form_no==3?'selected':''}}>3</option>
                        <option value="4" {{$form_no==4?'selected':''}}>4</option>
                        <option value="5" {{$form_no==5?'selected':''}}>5</option>
                        <option value="6" {{$form_no==6?'selected':''}}>6</option>
                        <option value="7" {{$form_no==7?'selected':''}}>7</option>
                        <option value="8" {{$form_no==8?'selected':''}}>8</option>
                        <option value="9" {{$form_no==9?'selected':''}}>9</option>
                        <option value="10" {{$form_no==10?'selected':''}}>10</option>
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label for="">Month:</label>
                    <select name="month" class="form-control" id="">
                        @foreach ($months as $key=>$m)
                            <option value="{{$key}}" {{$key==$month?'selected':''}}>{{$m}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label for="">Year:</label>
                    <select name="year" class="form-control" id="">
                        @foreach ($years as $y)
                            <option value="{{$y}}" {{$y==$year?'selected':''}}>{{$y}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-3 pt-4">
                    <button class="btn btn-sm btn-primary mt-2">select</button>
                </div>  
            </div> 
        </form>
    </div>
</div>
<div class="container mt-3">
    <div class="row">
        @if ($form_no==5)
            @include('admin.reports.mtuha.form-no-5')
        @elseif($form_no==1)
            @include('admin.reports.mtuha.form-no-1')
        @elseif($form_no==2)
            @include('admin.reports.mtuha.form-no-2')
        @elseif($form_no==7)
            @include('admin.reports.mtuha.form-no-7')
        @elseif($form_no==9)
            @include('admin.reports.mtuha.form-no-9') 
        @endif
    </div>
</div>


@endsection