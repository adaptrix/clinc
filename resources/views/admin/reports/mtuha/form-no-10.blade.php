<div class="report-form-header text-center">
        <p>FOMU YA KUTOLEA TAARIFA NA SABABU ZA VIFO</p>
    </div>
    <div class="report-form-subheader text-center row">
        <div class="col-md-4 text-center">
            Jina la kituo: <strong style="text-decoration:underline;">{{$kituo}}</strong>
        </div>
        <div class="col-md-4 text-center">
            Wilaya:  <strong style="text-decoration:underline;">{{$wilaya}}</strong>
        </div>
        <div class="col-md-4 text-center"> 
            Mkoa:  <strong style="text-decoration:underline;">{{$mkoa}}</strong>
        </div>
        <div class="col-md-6 text-center mt-2">
            Mwezi:   <strong style="text-decoration:underline;">{{$month_formatted}}</strong>
        </div>
        <div class="col-md-6 text-center mt-2">
            Mwaka:   <strong style="text-decoration:underline;">{{$year}}</strong>
        </div> 
    </div>
 <table class="reporttable" style="width:90%; margin:auto;font-size:15px">
    <tr>
        @foreach (range(1,14) as $number)
            @if ($number == 7)
            <th colspan="3">{{$number}}</th>
            @else
                <th>{{$number}}</th>
            @endif
        @endforeach
    </tr>
    <tr> 
        <th colspan="10" class="text-center">Taarifa za Msingi za Marehemu</th>
        <th colspan="3" class="text-center">Taarifa za Kitabibu</th>
        <th colspan="3" class="text-center">Taarifa za Upasuaji</th>
    </tr>
    <tr>
        <th rowspan="2">Tarehe ya kifo</th>
        <th rowspan="2">Namba ya Usajili ya kituo</th>
        <th rowspan="2">Jina</th>
        <th rowspan="2">Jinsi</th>
        <th rowspan="2">Alipokuwa anaishi</th>
        <th rowspan="2">Tarehe ya kuazaliwa</th>
        <th rowspan="1" colspan="3">Umri / Makadirio ya umri</th>
        <th rowspan="2">Mahali kifo kilipotokea</th> 
        <th rowspan="2">Sababu ya Msingin ya kifo(Underlying Cause)</th>
        <th rowspan="2">Code ICD 10</th>
        <th rowspan="2">Sababu nyingine ya msingi iliyochangia kifo</th>
        <th rowspan="2">Je, Upasuaji ulifanyika ndani ya wiki 4 zilizopita? (N/H)</th>
        <th rowspan="2">Tarehe ya upasuaji ulipofanyika</th>
        <th rowspan="2">Sababu ya upasuaji</th>
    </tr>
    <tr>
        <th>Siku</th>
        <th>Miezi</th>
        <th>Miaka</th>
    </tr>
    @foreach (range(1,7) as $number)
        <tr>
            @foreach (range(1,16) as $item)
                <td style="height: 25px"></td>
            @endforeach
        </tr>
    @endforeach
</table> 

 <table class="reporttable" style="width:90%; margin:auto;font-size:15px; margin-top:40px">
    <tr>
        @foreach (range(15,30) as $number)
            @if ($number == 7)
                <th colspan="3">{{$number}}</th>
            @elseif($number == 23 || $number == 27)
                <th colspan="2">{{$number}}</th>
            @else
                <th>{{$number}}</th>
            @endif
        @endforeach
        <th colspan="2" rowspan="2">31</th>
    </tr>
    <tr> 
        <th colspan="2" class="text-center">Uchunguzi wa maiti</th>
        <th colspan="5" class="text-center">Kifo kilivyotokeamfano ugonjwa, ajali, kipigo</th>
        <th colspan="8" class="text-center">Kifo cha mtoto tumboni au mtoto mchanga</th>
        <th colspan="3" class="text-center">Kifo kinachotokana na ujauzito au uzazi</th> 
    </tr>
    <tr>
        <th rowspan="2">Uchunguzi wa maiti ulifanyika? (N/H)</th>
        <th rowspan="2">Majibu yalitumika kuthibitisha kifo? (N/H)</th> 
        <th rowspan="2">Kisababishi cha kifo</th> 
        <th rowspan="2">Tarehe ya kifo/kisababishi</th> 
        <th rowspan="2">Namna tukio lilivyotokea</th> 
        <th rowspan="2">Mahali tukio lilipotokea</th> 
        <th rowspan="2">Mimba ya watoto pacha(N/H)</th> 
        <th rowspan="2">Mtoto amezaliwa mfu?(N/H)</th> 
        <th rowspan="1" colspan="2">Je kifo kimetokea ndani ya saa 24 tangu kuazaliwa?</th> 
        <th rowspan="2">Uzito wa mtoto (grams)</th> 
        <th rowspan="2">Umri wa mimba kwa wiki</th> 
        <th rowspan="2">Umri wa mama</th> 
        <th rowspan="1" colspan="2">Kifo cha mtoto mchanga?</th> 
        <th rowspan="2">Je marehemu alikua mjamzito? N\H)</th> 
        <th rowspan="2">Tukio la kifo wakati wa ujauzito au baada ya kujifunua (KU,N42,N43,HJ)</th> 
        <th rowspan="2">Je ujauzito ulichangia kifo? (N\H)</th> 
        <th rowspan="2">Kifo kimekaguliwa?? (N\H)</th> 
        <th rowspan="2">Kama jibu ni N, andika Tarehe na kielelezo? (N\H)</th> 
    </tr>
    <tr>
        <th>(N/H)</th>
        <th>Muda aliofariki</th>
        <th>(N/H)</th>
        <th>Hali ya mama</th>
    </tr>
    @foreach (range(1,7) as $number)
        <tr>
            @foreach (range(1,20) as $item)
                <td style="height: 25px"></td>
            @endforeach
        </tr>
    @endforeach
</table> 
