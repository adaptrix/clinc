@php
use App\PatientQueueTemporary;
use App\Patient;
use App\BillingList;
$patients = PatientQueueTemporary::where('created_at', '>=', date('Y-m-d').' 00:00:00')->whereIn('status',['waiting','out'])->orderBy('service_mode','asc')->orderBy('created_at','asc')->take(10);
$results = BillingList::whereIn('status',['done','Left'])->orderBy('updated_at','asc')->orderBy('status','asc')->take(10);

@endphp
@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row">
        @include('admin.components.dashboard-card',['title'=>'Total Users','icon'=>'fa-user','stat'=>\App\User::count()])
        @include('admin.components.dashboard-card',['title'=>'Total patients','icon'=>'fa-users','stat'=>\App\Patient::count()])
        @include('admin.components.dashboard-card',['title'=>'Medicines','icon'=>'fa-syringe','stat'=>\App\Medicine::count()])
    </div> 
    <div class="row"> 
        <div class="col-md-6"> 
            <div class="card">
                <div class="card-header bg-dark">
                    <a href=""><h4 class="text-white">Lab queue</h4></a>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive m-b-40">
                            <table id="" class="mytable">
                                    <thead>
                                        <tr>
                                                <th>Queue <br> #</th>  
                                                <th>reg. <br> #</th>
                                                <th>name</th> 
                                                <th>Service <br> Mode</th> 
                                                <th>Alloc. <br> Doctor</th>  
                                                <th>Status</th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $i = 1;
                                        @endphp
                                            @foreach ($results as $result)
                                            <tr {!!$result->status == 'Left'?'style="background-color:#faa;"':''!!}>
                                                <td>{{$i}}</td>
                                                @php 
                                                    $i++;
                                                    $patient = App\Patient::find($result->patient_registration_no);
                                                    $patientQ = App\PatientQueueTemporary::where('patient_reg_no',$result->patient_registration_no)->where('visit_no',$result->visit_no)->first();
                                                    $name = $patient->name; 
                                                    if($result->family_help_id!=0)
                                                            $name = $name.'\'s '.$result->familyHelp->relationship;
                                                @endphp 
                                                <td>{{$result->patient_registration_no}}</td>
                                                <td>{{$name}}</td> 
                                                <td>{{$patientQ->service_mode}}</td>
                                                <td>{{$patientQ->doctor}}</td>  
                                                <td>{{$result->status}}</td> 
                                            </tr>   
                                            @endforeach
                                    </tbody>
                                </table>
                        </div>
                    </div>    
                </div>

        </div>
        <div class="col-md-6"> 
            <div class="card">
                <div class="card-header bg-dark">
                    <a href=""><h4 class="text-white">Patients queue</h4></a>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive m-b-40">
                            <table id="patient-que-table" class="mytable">
                                <thead>
                                    <tr>
                                        <th>Que <br> #</th>  
                                        <th>reg. <br> #</th>
                                        <th>name</th> 
                                        <th>Service <br> Mode</th> 
                                        <th>Alloc. <br> Doctor</th> 
                                        <th>Status</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                        @foreach($patients as $pt)
                                            @php
                                                $patient = Patient::find($pt->patient_reg_no);                                                
                                            @endphp
                                        <tr {!!$pt->status=="Out"?'style="background-color:#ffaaaa !important;"':''!!}>
                                            <td>{{$pt->patient_que_no}}</td>  
                                            <td>{{$pt->patient_reg_no}}</td>
                                            <td>{{$patient->name}}</td> 
                                            <td>{{$pt->service_mode}}</td> 
                                            <td>{{$pt->doctor}}</td> 
                                            <td>{{$pt->status}}</td> 
                                        </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>    
                </div>

        </div>
    </div>   
</div>
@endsection  