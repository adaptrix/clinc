<h5 class="m-t-10">{{$is_editing?'Edit supplier equipment price':'Add equipment to supplier'}}</h5>
<form action="{{$add_equipment_url}}" class="form" method="POST">
    {{ csrf_field() }}
    <div class="row">
        <input type="hidden" name="supplier_id" value="{{$supplier->id}}">
        @if ($is_editing)
            <input type="hidden" name="supplier_equipment_id" value="{{$supplier_equipment_id}}">            
            <input type="hidden" name="equipment_id" value="{{$edit_equipment_id}}">            
        @endif
        <div class="form-group col-md-5">
            <label for="">Equipment</label>
            <select class="form-control" name="equipment_id" id="" {{$is_editing?'disabled':''}}>
                @foreach ($equipments as $equipment)
                    <option value="{{$equipment->id}}" {{isset($edit_equipment_id)?($edit_equipment_id==$equipment->id?'selected':''):''}}>{{$equipment->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-5">
            <label for="">Price</label>
            <input type="number" class="form-control" name="price" id="" value="{{$edit_price??''}}">
        </div>
        <div class="col-md-2 text-right">
            <button class="btn btn-primary btn-sm" style="margin-top:32px;">{{$is_editing?'Edit equipment':'Add equipment'}}</button>
        </div>
    </div>
</form>
@if (!$is_editing) 
<h5 class="m-t-10">All supplier equipments</h5>
<table class="mytable m-t-10">
    <thead>
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Price</th>
            <th>Code</th> 
            <th>Action</th>
        </tr>        
    </thead>
    <tbody>
        @foreach ($supplier->equipments as $equipment)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$equipment->name}}</td>
            <td>{{$equipment->pivot->price}}</td>
            <td>{{$equipment->code}}</td>   
            <td>
                <a href="{{Request::fullUrl().'&supplier_equipment_id='.$equipment->pivot->id}}" class="btn btn-sm btn-primary">Edit</a>
                <a href="{{$equipment_delete_url."/".$equipment->pivot->id}}" class="btn btn-sm btn-danger">Remove</a>
            </td>
        </tr>
    @endforeach   
    </tbody>


</table>
@endif