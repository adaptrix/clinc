@extends('components.add-form')

@section('form')
    <input type="hidden" name="lpo_id" value="{{$lpo->id}}">
    <div class="col-md-4"> 
        <label for="">Sale type</label>
        <select name="sale_type" id="" class="form-control">
            @foreach ($sale_types as $key => $sale_type)
                <option value="{{$key}}">{{$sale_type}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-4">
        <label for="">Date received</label>
        <input type="date" name="received_date" value="{{\Carbon\Carbon::today()->format('Y-m-d')}}" id="" class="form-control" required>
    </div>
    <div class="col-md-4">
        <label for="">Sale number</label>
        <input type="text" name="sale_number" id="" class="form-control" required>
    </div>
    @if ($lpo->medicines()->count()>0)
        <div class="col-md-12" style="margin-left:-16px; margin-top: 20px">
            @include('components.content-header',["header"=>"Medicines"])
        </div>
        @foreach ($lpo->medicines() as $medicine)
            <input type="hidden" name="medicine_lpo_item_ids[]" value="{{$medicine->id}}">
            <div class="col-md-12 row">
                <div class="col-md-3">
                    <label for="">Medicine name:</label>
                    <input class="form-control" type="text" name="" id="" readonly value="{{$medicine->data->name}}">
                </div>
                <div class="col-md-3">
                    <label for="">Status</label>
                    <select name="medicine_lpo_item_stati[]" id="" class="form-control">
                        @foreach ($item_stati as $key=>$status)
                            <option value="{{$key}}">{{$status}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="">Quantity</label>
                    <input type="number" class="form-control" min="0" max="{{$medicine->quantity}}" placeholder="{{$medicine->quantity}}" name="medicine_lpo_item_quantities[]" required>
                </div>
                <div class="col-md-3">
                    <label for="">Reason/Comment <small>(optional)</small></label>
                    <input type="text" class="form-control" name="medicine_lpo_item_reasons[]">
                </div>
                <div class="col-md-3">
                    <label for="">Expiry date <small>(optional)</small></label>
                    <input type="date" class="form-control" name="medicine_expiry_dates[]">
                </div>
            </div> <br>
        @endforeach 
    @endif
    @if ($lpo->equipments()->count()>0)
        <div class="col-md-12" style="margin-left:-16px; margin-top: 20px">
            @include('components.content-header',["header"=>"Equipments"])
        </div>
        @foreach ($lpo->equipments() as $equipment)
            <input type="hidden" name="equipment_lpo_item_ids[]" value="{{$equipment->id}}">
            <div class="col-md-12 row">
                <div class="col-md-3">
                    <label for="">Equipment name:</label>
                    <input class="form-control" type="text" name="" id="" readonly value="{{$equipment->data->name}}">
                </div>
                <div class="col-md-3">
                    <label for="">Status</label>
                    <select name="equipment_lpo_item_stati[]" id="" class="form-control">
                        @foreach ($item_stati as $key=>$status)
                            <option value="{{$key}}">{{$status}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="">Quantity</label>
                    <input type="number" name="equipment_lpo_item_quantities[]" min="0" max="{{$equipment->quantity}}" placeholder="{{$equipment->quantity}}" class="form-control" required>
                </div> 
                <div class="col-md-3">
                    <label for="">Reason/Comment <small>(optional)</small></label>
                    <input type="text" name="equipment_lpo_item_reasons[]" class="form-control">
                </div>
            </div> 
        @endforeach
    @endif
@endsection