@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>LPO Details</h3>
            </div>
            <div class="col-md-12">
                {{-- <hr> --}}
            </div>
        </div>
        <div class="row m-t-10">
            <div class="col-md-12">
                <table class="table table-striped">
                    <tr>
                        <td>Date:</td>
                        <td>{{$lpo->created_at->format('d/m/Y')}}</td>
                    </tr>
                    <tr>
                        <td>Status:</td>
                        <td>{{$lpo->status}}</td>
                    </tr>
                    <tr>
                        <td>Supplier:</td>
                        <td>{{$lpo->supplier->name??null}}</td>
                    </tr>
                    <tr>
                        <td>Supplier phone #:</td>
                        <td>{{$lpo->supplier->phone_no}}</td>
                    </tr>
                    <tr>
                        <td>Contact person:</td>
                        <td>{{$lpo->supplier->contact_person}}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3>LPO Items</h3>
            </div>
            <div class="col-md-12">
                {{-- <hr> --}}
            </div>
        </div>
        <div class="row m-t-10">
            <div class="col-md-12">
                <table class="table table-striped">
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Total amount</th>                        
                    </tr>
                    @foreach ($lpo->items as $item)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$item->data->name}}</td>
                            <td>{{$item->type}}</td>
                            <td>{{$item->quantity}}</td>
                            <td>{{$item->price}}</td>
                            <td>{{$item->total_amount}}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><strong>Total:</strong></td>
                        <td><strong>{{$lpo->total_amount}}</strong></td>
                    </tr>
                </table>
                <hr>
            </div>
            <div class="col-md-12 text-right">
                <a href="{{url()->previous()}}" class="btn btn-sm btn-warning">Back</a>
            </div>
        </div>
    </div>
@endsection