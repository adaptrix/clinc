@extends('layouts.admin')
@section('content')
    <div class="container">
        @include('components.content-header',[
            "header"=>"Employees",
            "button"=>[
                "color"=>"primary",
                "name"=>"Add new employee",
                "url"=>$new_employee_url
            ]
        ])
        @include('components.table',[
            "columns"=>["No","Name","Gender","Designation","Action"],
            "collection"=>$employees,
            "data_columns"=>[
                "iteration",
                "name",
                "gender",
                "department_name", 
                "buttons"=>[
                    ["name"=>"Edit","url"=>$edit_employee_url,"color"=>"primary","id"=>true],
                    ["name"=>"View","url"=>$view_employee_url,"color"=>"success","id"=>true],
                    ["name"=>"Remove","url"=>$remove_employee_url,"color"=>"danger","id"=>true,"should_confirm"=>true,"action"=>"remove"]
                ]
            ]
        ])
    </div>
@endsection