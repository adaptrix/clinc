@extends('components.add-form')
@section('form')
    <div class="col-md-6 form-group">
        <label for="">Name</label>
        <input type="text" name="name" class="form-control" value="{{$model->name??null}}">
    </div>
    <div class="col-md-6 form-group">
        <label for="">Email</label>
        <input type="email" name="email" class="form-control" value="{{$model->email??null}}">
    </div>
    <div class="col-md-6 form-group">
        <label for="">Password <small>{{$edit_password_info}}</small></label>
        <input type="password" name="password" class="form-control" value="" {{$is_editing?"":"required"}}>
    </div>
    <div class="col-md-6 form-group">
        <label for="">Phone no</label>
        <input type="number" name="phone_no" class="form-control" value="{{$model->phone_no??null}}">
    </div> 
    <div class="col-md-6 form-group">
        <label for="">Employee code</label>
        <input type="text" name="employee_code" class="form-control" value="{{$model->employee_code??null}}">
    </div>
    <div class="col-md-6 form-group">
        <label for="">Joining date</label>
        <input type="date" name="joining_date" class="form-control" value="{{$model->form_joining_date??null}}">
    </div>
    <div class="col-md-6 form-group">
        <label for="">Net salary</label>
        <input type="number" name="salary" class="form-control" value="{{$model->salary??null}}">
    </div>
    <div class="col-md-6 form-group">
        <label for="">Gender</label> 
        <select name="gender" id="" class="form-control">
            @foreach ($genders as $gender)
                <option value="{{$gender}}" {{$gender==$model->gender?'selected':''}}>{{$gender}}</option>
            @endforeach
        </select>
    </div> 
    <div class="col-md-6 form-group">
        <label for="">Address</label>
        <textarea name="address" id="" cols="30" rows="2" class="form-control">{{$model->address??null}}</textarea>
    </div> 
    <div class="col-md-6 form-group">
        <label for="">Department</label>
        <select name="department_id" id="" class="form-control">
            @foreach ($departments as $department)
                <option value="{{$department->id}}" {{$department->id==$model->department_id?'selected':''}}>{{$department->name}}</option>
            @endforeach
        </select>
    </div>
@endsection