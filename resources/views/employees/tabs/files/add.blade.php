@extends('employees.view')
@section('tab-content')  
<div class="container-fluid">
    <div class="row" style="margin:auto;">
        <div class="col-md-12">
            <h4>{{$header}}</h4>
            <hr>
        </div>
        <div class="col-md-12">
            <form action="{{ url($form_url) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                @if ($is_editing)
                    <input type="hidden" name="id" value="{{$model->id??null}}">
                @endif
                <input type="hidden" name="id" value="{{$id}}">
                <input type="hidden" name="employee_id" value="{{$id}}">
                <div class="row">        
                    <div class="col-md-6">
                        <label for="">Description <small>(optional)</small></label>
                        <textarea name="description" id="" cols="30" rows="2" class="form-control"></textarea>
                    </div>
                    <div class="col-md-6">
                        <label for="">Select file</label>
                        <input type="file" name="document" id="" class="form-control">
                    </div> 
                </div>
                <div class="row"> 
                    <div class="col-md-12">
                        <hr>
                        <button class="btn btn-danger btn-sm" type="button" onclick="window.history.back()">Back</button>
                        @if ($is_editing)
                            <button type="submit" class="btn btn-success btn-sm">Save</button>
                        @else
                            <button type="submit" class="btn btn-success btn-sm">Submit</button>                                    
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>

@endsection
