@extends('employees.view')
@section('tab-content')
    <div class="container"> 
        @include('components.content-header',[
            "header"=>"Employee appraisals",
            "button"=>[
                "name"=>"Add new employee appraisal",
                "color"=>"primary",
                "url"=>$new_employee_appraisal_url
            ]
        ])
        @include('components.table',[
            "columns"=> ["No","File","Description","Date","Action"],
            "collection"=>$employee_appraisals,
            "data_columns"=>[
                "iteration",
                "name",
                "description",
                "date",
                "buttons"=>[ 
                    ["name"=>"Download","url"=>$download_employee_appraisal_url,"id"=>true,"color"=>"success"],
                    ["name"=>"Delete","url"=>$remove_employee_appraisal_url,"id"=>true,"color"=>"danger","should_confirm"=>true,"action"=>"delete"] 
                ]
            ]
        ])
    </div> 
@endsection