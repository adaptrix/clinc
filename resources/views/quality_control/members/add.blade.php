@extends('components.add-form') 
@section('form')  
<div class="col-md-12 form-group">
    <label for="">Select Members</label>
    <select name="members[]" id="" class="form-control select2" multiple>
        @foreach ($staffs as $staff)
            <option value="{{$staff->id}}" {{in_array($staff->id,$selected_members)?'selected':''}}>{{$staff->name}}</option>
        @endforeach
    </select>
</div> 
@endsection