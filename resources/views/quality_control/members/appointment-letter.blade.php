@extends('components.add-form')

@section('form')
    <div class="col-md-12 form-group">
        <label for="">
            Type appointment letter <br>
            <small>**In places where you want the name to appear just place "{{$name_template_string}}"</small>
        </label>
        <textarea name="letter" id="" cols="30" rows="1" class="editor">{{$letter}}</textarea>
    </div>
@endsection