@extends('layouts.admin')

@section('content')
    <div class="container">
        @include('components.content-header',[
            "header"=>"Employee leaves",
            "button"=>[
                "name"=>"Assign leave",
                "color"=>"primary",
                "url"=>$new_employee_leave_url
            ]
        ])
        @include('components.table',[
            "columns"=>["No","Employee Name","Reason","Dates","Days","Action"],
            "collection"=> $leaves,
            "data_columns"=>[
                "iteration",
                "name",
                "reason",
                "date",
                "number_of_days",
                "button"=>[ 
                    ["name"=>"Edit","url"=>$edit_employee_leave_url,"color"=>"primary","id"=>true],
                    // ["name"=>"View","url"=>$view_leave_url,"color"=>"success","id"=>true],
                    ["name"=>"Remove","url"=>$remove_employee_leave_url,"color"=>"danger","id"=>true,"should_confirm"=>true,"action"=>"remove"]
                ]
            ]
        ])
    </div>
@endsection