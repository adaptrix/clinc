<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TotalCost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medicine_stock_batch_medicine_stock_uses', function (Blueprint $table) {
            $table->string('total_cost')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medicine_stock_batch_medicine_stock_uses', function (Blueprint $table) {
            $table->dropColumn('total_cost');
        });
    }
}
