<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamilyHelpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_helps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patient_registration_no');
            $table->integer('visit_no');
            $table->string('relationship');
            $table->string('name', 100);
            $table->string('dob', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_helps');
    }
}
