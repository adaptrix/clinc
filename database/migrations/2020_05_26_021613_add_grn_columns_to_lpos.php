<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGrnColumnsToLpos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('l_p_os', function (Blueprint $table) {
            $table->string('received_date', 100)->nullable();
            $table->string('sale_type', 100)->nullable();
            $table->string('sale_number', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('l_p_os', function (Blueprint $table) {
            $table->dropColumn('received_date');
            $table->dropColumn('sale_type');
            $table->dropColumn('sale_number');
        });
    }
}
