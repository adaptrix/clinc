<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicineStockBatchMedicineStockUsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicine_stock_batch_medicine_stock_uses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('medicine_stock_batch_id');
            $table->integer('medicine_stock_use_id');
            $table->string('used_quantity', 100);   
            $table->string('remaining_quantity', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicine_stock_batch_medicine_stock_uses');
    }
}
