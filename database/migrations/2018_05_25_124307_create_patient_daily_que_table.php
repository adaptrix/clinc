<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientDailyQueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_daily_que', function (Blueprint $table) 
        {
            $table->increments('patient_que_id');
            $table->string('patient_reg_no');
            $table->string('service_mode_id');
            $table->string('service_id');
            $table->string('doc_id');
            $table->string('unit_id');
            $table->string('room_no');
            $table->date('date');
            $table->timestamps();
 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_daily_que');
    }
}
