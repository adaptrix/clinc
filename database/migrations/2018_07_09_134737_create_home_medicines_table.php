<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeMedicinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_medicines', function (Blueprint $table) {
            $table->increments('id');
            $table->text('content')->nullable();
            $table->string('status')->nullable();
            $table->string('patient_registration_no');
            $table->integer('visit_no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_medicines');
    }
}
