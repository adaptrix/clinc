<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCommentFromDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comment_from_doctors', function (Blueprint $table) {
            //
            $table->string('patient_registration_no');
            $table->integer('visit_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comment_from_doctors', function (Blueprint $table) {
            //
            $table->dropColumn('patient_registration_no');
            $table->dropColumn('visit_no');
        });
    }
}
