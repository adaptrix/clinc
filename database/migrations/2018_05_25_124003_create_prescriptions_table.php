<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prescriptions', function (Blueprint $table) 
        {
            $table->increments('prescription_id');
            $table->string('doc_id');
            $table->string('patient_reg_no');
            $table->mediumText('cace_history');
            $table->mediumText('medication');
            $table->mediumText('description');
            $table->mediumText('medication_from_pharmacist');
            $table->string('diagnosis_id');
            $table->timestamps();

            $table->foreign('doc_id')->references('doc_id')->on('doctors');
            $table->foreign('patient_reg_no')->references('reg_no')->on('patients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prescriptions');
    }
}
