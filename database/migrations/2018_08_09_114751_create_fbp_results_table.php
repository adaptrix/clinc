<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFbpResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fbp_results', function (Blueprint $table) {
            $table->increments('id');
            $table->text('wbc')->nullable();
            $table->string('gra')->nullable();
            $table->string('Hb')->nullable(); 
            $table->text('from_machine')->nullable();
            $table->string('lab_test_record_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fbp_results');
    }
}
