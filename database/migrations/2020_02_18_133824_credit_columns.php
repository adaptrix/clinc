<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreditColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credits', function (Blueprint $table) {
            $table->text('description')->nullable();
            $table->string('status', 100)->nullable()->default('created');
            $table->string('date', 100)->nullable();
            $table->string('amount', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('credits', function (Blueprint $table) {
            $table->dropColumn('date');
            $table->dropColumn('status');
            $table->dropColumn('description');
            $table->dropColumn('amount');
        });
    }
}
