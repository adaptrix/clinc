<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_changes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('amount_recieved');
            $table->string('change');
            $table->string('change_given');
            $table->string('change_due');
            $table->integer('billing_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_changes');
    }
}
