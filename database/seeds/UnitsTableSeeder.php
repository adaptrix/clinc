<?php

use Illuminate\Database\Seeder;
use App\Unit;

class UnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $unit = new Unit;
        $unit->name = "Unit 1";
        $unit->description = "General Unit";
        $unit->save();
    }
}
