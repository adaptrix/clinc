<?php

use Illuminate\Database\Seeder;
use App\Room;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $room = new Room;
        $room->name = "Room 1";
        $room->use = "Consultation";
        $room->save();

        $room = new Room;
        $room->name = "Room 2";
        $room->use = "Consultation";
        $room->save();
    }
}
