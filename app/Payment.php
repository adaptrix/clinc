<?php

namespace App;

use App\BaseModel as Model;
use Carbon\Carbon;

class Payment extends Model
{
    protected $guarded = [];

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value);
    }

    public function getAmountAttribute($value)
    {
        return number_format($value)." Tsh";
    }

    public function getDateAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }
}
