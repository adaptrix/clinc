<?php

namespace App;


use \App\BaseModel as Model;


class Prescription extends Model
{
    protected $table = 'prescriptions';
    protected $primaryKey = 'prescription_id';
}
