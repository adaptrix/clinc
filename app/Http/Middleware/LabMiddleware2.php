<?php

namespace App\Http\Middleware;

use Closure;

class LabMiddleware2
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user()->hasRole('laboratorist_2'))
		{
			return redirect('/');
		}
        return $next($request);
    }
}
