<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PatientVisit;
use App\PatientQueueTemporary;
use App\Patient;
use App\Vital;
use App\ChiefComplain;
use App\History;
use App\HomeMedicine;
use App\Examination;
use App\Symptom;
use App\LabTestRecord;
use App\MedicineRecord;
use App\SymptomRecord;
use App\LabTest;
use App\CommentFromDoctor;
use App\LabList;
use App\PharmacyList;
use App\BillingList;
use App\KnownPatientOf; 
use App\FollowUpDate;
use App\FbpResult;
use App\StoolResult;
use App\UrinalResult;
use App\NoteFromDoctor;
use App\NoteFromLaboratory;
use App\FamilyHelp;
use App\Medicine;
use App\MedicineUnit;

class DoctorController extends Controller
{
    public function index()
    {
        $title = 'Consultant';
        return view('doctor.index',compact('title'));
    }

    public function patientDetails($id){ 
        return view('doctor.patient-details',['id'=>$id,'title'=>'Patient Details']);
    }

    public function patientDiagnosis(Request $request){    
       $pqt = PatientQueueTemporary::find($request->patient_que_id);
       $prn = $pqt->patient_reg_no;
       $pvn = $pqt->visit_no; 

       $history = new History;
       $history->patient_registration_no = $prn;
       $history->visit_no = $pvn;
       $history->content = $request->history;
       $history->save();

       $home_medicine = new HomeMedicine;
       $home_medicine->content = $request->medicine_at_home;
       $home_medicine->patient_registration_no = $prn;
       $home_medicine->visit_no = $pvn;
       $home_medicine->status = $request->dose_status;
       $home_medicine->save();

       $examination = new Examination;
       $examination->patient_registration_no = $prn;
       $examination->visit_no  = $pvn;
       $examination->pa = $request->pa;
       $examination->ge = $request->ge;
       $examination->cns = $request->cns;
       $examination->cvs = $request->cvs;
       $examination->res = $request->res;
       $examination->other = $request->other;
       $examination->save();

        $kpf=KnownPatientOf::where('patient_registration_no',$prn)->first(); 
        if($request->known_patient_of==null||$request->known_patient_of=="")
            $rkpf = "";
        else
            $rkpf = $request->known_patient_of;
        if($kpf){
            $kpf->content = $rkpf;
            $kpf->save(); 
        }else{
            $kpf = new KnownPatientOf;
            $kpf->content = $rkpf;
            $kpf->patient_registration_no = $prn;
            $kpf->save(); 
        }

       if($request->symptoms!=null)
       foreach($request->symptoms as $sym){
           $symptom = Symptom::firstOrCreate(['name' => $sym ]);

           $symptom_record = new SymptomRecord;
           $symptom_record->patient_registration_no = $prn;
           $symptom_record->visit_no = $pvn;
           $symptom_record->symptom_id = $symptom->id;
           $symptom_record->save();
       }   
       
       if($request->diagnosis!=null)
       foreach($request->diagnosis as $diagnosis){ 
           $diagnosis_record = new \App\DiagnosisRecord;
           $diagnosis_record->patient_registration_no = $prn;
           $diagnosis_record->visit_no = $pvn;
           $diagnosis_record->diagnosis_id = $diagnosis;
           $diagnosis_record->save();
       }


       if($request->lab_tests!=null){
            $billinglist = new BillingList;
            $billinglist->patient_registration_no = $prn;
            $billinglist->visit_no = $pvn;
            $billinglist->billing_id = 0;
            $billinglist->type = "Laboratory";
            $billinglist->save();     

            foreach($request->lab_tests as $l_test){
                $lab_test = new LabTestRecord;
                $lab_test->patient_registration_no = $prn;
                $lab_test->visit_no = $pvn;
                $lab_test->lab_test_id = $l_test;
                $lab_test->status = "pending";
                $lab_test->billing_id = $billinglist->id;
                $lab_test->save();
                
                if($lab_test->lab_test->name=="Urinal"){
                    $urinal = new UrinalResult;
                    $urinal->lab_test_record_id = $lab_test->id;
                    $urinal->save();
                }
                else if($lab_test->lab_test->name=="FBP"){
                    $fbp = new FbpResult;
                    $fbp->lab_test_record_id = $lab_test->id;
                    $fbp->save();
                }
                else if($lab_test->lab_test->name=="Stool"){
                    $stool = new StoolResult;
                    $stool->lab_test_record_id = $lab_test->id;
                    $stool->save();
                }  
            } 
           
            $nfd = new NoteFromDoctor;
            $nfd->content = $request->note_from_doctor;
            $nfd->billing_id = $billinglist->id;
            $nfd->save();     
            $nfl = new NoteFromLaboratory; 
            $nfl->billing_id = $billinglist->id;
            $nfl->save();  
       }

       $array_key = 0;
       if($request->medicine!=null){   
            $billinglist = new BillingList;
            $billinglist->patient_registration_no = $prn;
            $billinglist->visit_no = $pvn;
            $billinglist->billing_id = 0;
            $billinglist->type = "Pharmacy";
            $billinglist->save();   
            foreach($request->medicine as $key=>$med){ 
                    $unit = MedicineUnit::firstOrCreate(['name' => $request->medicine_unit[$key] ]);

                    $medicine_record = new MedicineRecord;
                    $medicine_record->medicine_id = $med;
                    $medicine_record->patient_registration_no = $prn;
                    $medicine_record->visit_no = $pvn;
                    $medicine_record->billing_id = $billinglist->id;

                    if(array_key_exists($key,$request->quantity))
                        $medicine_record->quantity = $request->quantity[$key];

                    if(array_key_exists($key,$request->per_day))
                        $medicine_record->perday = $request->per_day[$key];

                    if(array_key_exists($key,$request->days))
                        $medicine_record->days = $request->days[$key].$request->length[$key];

                    if(array_key_exists($key,$request->total))
                        $medicine_record->total = $request->total[$key];

                    if(array_key_exists($key,$request->dose))
                        $medicine_record->dose_with_unit = $request->quantity[$key].':'.$request->medicine_unit[$key];

                    $medicine_record->save();
                    // $array_key++;
            }  
        }

       $comment_from_doctor = new CommentFromDoctor;
       $comment_from_doctor->patient_registration_no = $prn;
       $comment_from_doctor->visit_no = $pvn;
       $comment_from_doctor->content = $request->comment_from_doctor;
       $comment_from_doctor->save();

       $follow_up_date = new FollowUpDate;
       $follow_up_date->patient_registration_no = $pvn; 
       $follow_up_date->visit_no = $pvn;
       $follow_up_date->content =  $request->follow_up_date;
       $follow_up_date->save();
       $pqt->status = "attended";
       $pqt->save();
    

       return redirect('/doctor');
    }

    public function prescribeMedicine(Request $request){    
        // dd($request);    
       $array_key = 0;
       $prn = $request->prn;
       $pvn = $request->pvn;
       if($request->medicine!=null){  
            $billinglist = new BillingList;
            $billinglist->patient_registration_no = $prn;
            $billinglist->visit_no = $pvn;
            $billinglist->billing_id = 0;
            $billinglist->type = "Pharmacy";
            $billinglist->save();  
            foreach($request->medicine as $med){
                $unit = MedicineUnit::firstOrCreate(['name' => $request->medicine_unit[$array_key] ]);
                $medicine_record = new MedicineRecord;
                $medicine_record->medicine_id = $med;
                $medicine_record->patient_registration_no = $prn;
                $medicine_record->visit_no = $pvn;
                $medicine_record->quantity = $request->quantity[$array_key];
                $medicine_record->perday = $request->per_day[$array_key];
                $medicine_record->days = $request->days[$array_key].$request->length[$array_key];
                $medicine_record->total = $request->total[$array_key];
                $medicine_record->dose_with_unit = $request->quantity[$array_key].':'.$request->medicine_unit[$array_key];
                $medicine_record->billing_id = $billinglist->id;
                $medicine_record->save();
                $array_key++;
            }           
        }
        return view('doctor.history',['id'=>$prn,'title'=>'Patient History','url'=>$request->url]);
    }

    public function prescribeMedicineFh(Request $request){        
        $array_key = 0;
        $prn = $request->prn;
        $pvn = $request->pvn;
        if($request->medicine!=null){  
             $billinglist = new BillingList;
             $billinglist->patient_registration_no = $prn;
             $billinglist->visit_no = $pvn;
             $billinglist->billing_id = 0;
             $billinglist->type = "Pharmacy";
             $billinglist->family_help_id = $request->fhid;
             $billinglist->save();   
             foreach($request->medicine as $med){
                     $unit = MedicineUnit::firstOrCreate(['name' => $request->medicine_unit[$array_key] ]);
                     $medicine_record = new MedicineRecord;
                     $medicine_record->medicine_id = $med;
                     $medicine_record->patient_registration_no = $prn;
                     $medicine_record->visit_no = $pvn;
                     $medicine_record->quantity = $request->quantity[$array_key];
                     $medicine_record->perday = $request->per_day[$array_key];
                     $medicine_record->days = $request->days[$array_key].$request->length[$array_key];
                     $medicine_record->total = $request->total[$array_key];
                     $medicine_record->dose_with_unit = $request->dose[$array_key].':'.$request->medicine_unit[$array_key];
                     $medicine_record->family_help_id = $request->fhid;
                     $medicine_record->billing_id = $billinglist->id;
                     $medicine_record->save();
                     $array_key++;
             }          
         }
         return view('doctor.familyhelp.view',['pvn'=>$pvn,'prn'=>$prn,'title'=>'Patient History','url'=>$request->url,'fhid'=>$request->fhid]);
     }

     public function giveDiagnosis(Request $request)
     {       
        if($request->diagnosis!=null)
        foreach($request->diagnosis as $diagnosis){ 
            $diagnosis_record = new \App\DiagnosisRecord;
            $diagnosis_record->patient_registration_no = $request->prn;
            $diagnosis_record->visit_no = $request->pvn;
            $diagnosis_record->diagnosis_id = $diagnosis;
            $diagnosis_record->save();
        }
        return redirect()->back();
     }

    public function giveLabTest(Request $request)
    {
        $prn = $request->prn;
        $pvn = $request->pvn;
        if($request->has('lab_tests')){
            $billinglist = new BillingList;
            $billinglist->patient_registration_no = $prn;
            $billinglist->visit_no = $pvn;
            $billinglist->billing_id = 0;
            $billinglist->type = "Laboratory";
            $billinglist->save();  
            
            foreach($request->lab_tests as $l_test){
                $lab_test = new LabTestRecord;
                $lab_test->patient_registration_no = $prn;
                $lab_test->visit_no = $pvn;
                $lab_test->lab_test_id = $l_test;
                $lab_test->status = "pending";
                $lab_test->billing_id = $billinglist->id;
                $lab_test->save();
                
                if($lab_test->lab_test->name=="Urinal"){
                    $urinal = new UrinalResult;
                    $urinal->lab_test_record_id = $lab_test->id;
                    $urinal->save();
                }
                else if($lab_test->lab_test->name=="FBP"){
                    $fbp = new FbpResult;
                    $fbp->lab_test_record_id = $lab_test->id;
                    $fbp->save();
                }
                else if($lab_test->lab_test->name=="Stool"){
                    $stool = new StoolResult;
                    $stool->lab_test_record_id = $lab_test->id;
                    $stool->save();
                }  
            } 
               

            $nfd = new NoteFromDoctor;
            $nfd->content = $request->note_from_doctor;
            $nfd->billing_id = $billinglist->id;
            $nfd->save();     
            $nfl = new NoteFromLaboratory; 
            $nfl->billing_id = $billinglist->id;
            $nfl->save();  
       }
       //return redirect()->back();
       return view('doctor.history',['id'=>$prn,'title'=>'Patient History','url'=>$request->url]);
    }

    public function giveLabTestFh(Request $request)
    {
        $prn = $request->prn;
        $pvn = $request->pvn;
        if($request->has('lab_tests')){
          
            $billinglist = new BillingList;
            $billinglist->patient_registration_no = $prn;
            $billinglist->visit_no = $pvn;
            $billinglist->billing_id = 0;
            $billinglist->type = "Laboratory";
            $billinglist->family_help_id = $request->fhid;
            $billinglist->save();   

            foreach($request->lab_tests as $l_test){
                $lab_test = new LabTestRecord;
                $lab_test->patient_registration_no = $prn;
                $lab_test->visit_no = $pvn;
                $lab_test->lab_test_id = $l_test;
                $lab_test->status = "pending";
                $lab_test->family_help_id = $request->fhid;
                $lab_test->billing_id = $billinglist->id;
                $lab_test->save();
                
                if($lab_test->lab_test->name=="Urinal"){
                    $urinal = new UrinalResult;
                    $urinal->lab_test_record_id = $lab_test->id;
                    $urinal->save();
                }
                else if($lab_test->lab_test->name=="FBP"){
                    $fbp = new FbpResult;
                    $fbp->lab_test_record_id = $lab_test->id;
                    $fbp->save();
                }
                else if($lab_test->lab_test->name=="Stool"){
                    $stool = new StoolResult;
                    $stool->lab_test_record_id = $lab_test->id;
                    $stool->save();
                }  
            }     

            // $nfd = new NoteFromDoctor;
            // $nfd->content = $request->note_from_doctor;
            // $nfd->billing_id = $billinglist->id;
            // $nfd->save();     
            // $nfl = new NoteFromLaboratory; 
            // $nfl->billing_id = $billinglist->id;
            // $nfl->save();  
       }
       //return redirect()->back();
       return view('doctor.familyhelp.view',['pvn'=>$pvn,'prn'=>$prn,'fhid'=>$request->fhid,'title'=>'Patient History','url'=>$request->url]);
    }

    public function familyHelp(Request $request){    
        $prn = $request->prn;
        $pvn = $request->pvn;   

        $family_help = new FamilyHelp;
        $family_help->name = $request->name;
        $family_help->dob = $request->dob;
        $family_help->relationship = $request->relationship;
        $family_help->patient_registration_no = $prn;
        $family_help->visit_no = $pvn;
        $family_help->save();
     
        if($request->symptoms!=null)
        foreach($request->symptoms as $sym){
            $symptom = Symptom::firstOrCreate(['name' => $sym ]);
 
            $symptom_record = new SymptomRecord;
            $symptom_record->patient_registration_no = $prn;
            $symptom_record->visit_no = $pvn;
            $symptom_record->symptom_id = $symptom->id;
            $symptom_record->family_help_id = $family_help->id;
            $symptom_record->save();
        }
 
        if($request->lab_tests!=null){
            $billinglist = new BillingList;
            $billinglist->patient_registration_no = $prn;
            $billinglist->visit_no = $pvn;
            $billinglist->billing_id = 0;
            $billinglist->type = "Laboratory";
            $billinglist->family_help_id = $family_help->id;
            $billinglist->save();  

             foreach($request->lab_tests as $l_test){
                 $lab_test = new LabTestRecord;
                 $lab_test->patient_registration_no = $prn;
                 $lab_test->visit_no = $pvn;
                 $lab_test->lab_test_id = $l_test;
                 $lab_test->status = "pending";
                 $lab_test->family_help_id = $family_help->id;
                 $lab_test->billing_id = $billinglist->id;
                 $lab_test->save();
                 
                 if($lab_test->lab_test->name=="Urinal"){
                     $urinal = new UrinalResult;
                     $urinal->lab_test_record_id = $lab_test->id;
                     $urinal->save();
                 }
                 else if($lab_test->lab_test->name=="FBP"){
                     $fbp = new FbpResult;
                     $fbp->lab_test_record_id = $lab_test->id;
                     $fbp->save();
                 }
                 else if($lab_test->lab_test->name=="Stool"){
                     $stool = new StoolResult;
                     $stool->lab_test_record_id = $lab_test->id;
                     $stool->save();
                 }  
             } 
                
        }
 
        $array_key = 0;
        if($request->medicine!=null){  
             $billinglist = new BillingList;
             $billinglist->patient_registration_no = $prn;
             $billinglist->visit_no = $pvn;
             $billinglist->billing_id = 0;
             $billinglist->type = "Pharmacy";
             $billinglist->family_help_id = $family_help->id;
             $billinglist->save(); 
             foreach($request->medicine as $med){
                     $unit = MedicineUnit::firstOrCreate(['name' => $request->medicine_unit[$array_key] ]);
                     $medicine_record = new MedicineRecord;
                     $medicine_record->medicine_id = $med;
                     $medicine_record->patient_registration_no = $prn;
                     $medicine_record->visit_no = $pvn;
                     $medicine_record->quantity = $request->quantity[$array_key];
                     $medicine_record->perday = $request->per_day[$array_key];
                     $medicine_record->days = $request->days[$array_key].$request->length[$array_key];
                     $medicine_record->total = $request->total[$array_key];
                     $medicine_record->family_help_id = $family_help->id;
                     $medicine_record->dose_with_unit = $request->dose[$array_key].':'.$request->medicine_unit[$array_key];
                     $medicine_record->billing_id = $billinglist->id;
                     $medicine_record->save();
                     $array_key++;
             }            
        }       
 
        return redirect()->back();
     }

    public function updateVisit(Request $request){
        dd($request);
    }

    public function patientVisit($id){
        return view('doctor.patient-visit',['id'=>$id,'title'=>'Patient visit']);
    }

    public function history(Request $request){
        if($request->has('status')){
            if($request->status == 'close'){ 
                $pqt = BillingList::find($request->billinglist_id);
                $pqt->status = "Closed";
                $pqt->save();
            }
        }
        return view('doctor.history',['id'=>$request->id,'title'=>'Patient History','url'=>$request->url]);
    }

    public function getVisit(Request $request){
        return view('doctor.history',['id'=>$request->id,'title'=>'Patient History','visit_no'=>$request->visit_no,'url'=>$request->url]);
    }

    public function editPatientForm($id)
    {
        return view('doctor.edit-patient',['id'=>$id]);
    }

    public function editPatient(Request $request)  
    { 
        $pqt = PatientQueueTemporary::find($request->id);
        $patient = Patient::find($pqt->patient_reg_no);
        $vital = Vital::where('patient_reg_no',$pqt->patient_reg_no)->where('visit_no',$pqt->visit_no)->first();
        $patient->DOB = $request->dob;
        $patient->save();
        $vital->weight = $request->weight;
        $vital->temperature = $request->temperature;
        $vital->height = $request->height;
        $vital->weight = $request->weight;
        $vital->pulse_rate = $request->pulse_rate;
        $vital->blood_pressure = $request->blood_pressure;
        $vital->save();
        return redirect($request->url);
    }

    public function closeFile($id){ 
        $pqt = BillingList::find($id);
        $pqt->status = "Closed";
        $pqt->save();
        return redirect('/doctor');
    }

    public function getMedicines(Request $request){
        $medicines = Medicine::where('medicine_category_id',$request->id)->get();
        return $medicines->toJson();
    }

    public function getMgPerTablet(Request $request)
    {
        $medicine = Medicine::find($request->medicine_id);
        return ["mg_per_tablet"=>$medicine->mgs_per_tablet];
    }

    public function familyHelpView(Request $request)
    {  
        if($request->has('id')){ 
            $list = BillingList::find($request->id);
            if($list->status != "viewed"){
                $list->status = "viewed";
                $list->save();
            }
        }
        return view('doctor.familyhelp.view',["prn"=>$request->prn,"pvn"=>$request->pvn,"fhid"=>$request->fhid,"url2"=>$request->url2]);
    }

    public function familyHelpAdd(Request $request)
    { 
        return view('doctor.familyhelp.add',["prn"=>$request->prn,"pvn"=>$request->pvn]);
    }
}
