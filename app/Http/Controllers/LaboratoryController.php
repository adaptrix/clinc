<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LabTestRecord;
use App\LabList;
use App\Billing;
use App\BillingTransaction;
use App\BillingChange; 

class LaboratoryController extends Controller
{
    public function index()
    {
        $title = 'Laboratory';
        return view('lab.index',compact('title'));
    }
}
