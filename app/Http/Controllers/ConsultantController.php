<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;

class ConsultantController extends Controller
{ 
    public function __construct()
    {
        $this->initialise(
            '/consultants',
            'consultants.',
            ['role:admin'],
            [
                "add_consultant_url"=>'/add',
                "form_url"=>'/add',
                "edit_url"=>'/add',
                "delete_url"=>'/remove'
            ],
            User::class
        ); 
    }

    public function addForm()
    {   
        $this->header = $this->hasId()?"Edit Consultant":"Add Consultant";
        $this->model = $this->getModel(User::class); 
        return $this->cView('add');
    }


    public function add()
    {
        $this->user = $this->getModel(User::class);
        $form_data = $this->request->except(['_token','password']);
        if($this->hasId()){ 
            if(!empty($this->request->password))
                $form_data["password"] = bcrypt($this->request->password);
            // dd($form_data);
            $this->user->update($form_data);
        }
        else{
            $role_consultant = Role::whereName('consultant')->first();

            $form_data['password'] = bcrypt($this->request->password);
            $user = User::create($form_data); 

            $user->roles()->attach($role_consultant);
        }
        return redirect($this->root_url)->with('success_msg','Consultant added successfully'); 
    }

    public function index()
    { 
        $this->consultants = User::consultants();
        return $this->cView('index');
    }

    public function remove()
    {
        $this->defaultModel::find($this->request->consultant_id)->delete();
        return redirect($this->root_url)->with('success_msg','Consultant removed successfully');
    }

}
