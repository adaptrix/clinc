<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request; 
use App\MedicineRecord;
use App\Billing;
use App\BillingTransaction;
use App\PharmacyList;
use App\BillingChange; 


class PharmacyController extends Controller
{
    public function index()
    {
        $title = 'Pharmacy';
        return view('pharmacy.index',compact('title'));
    }

}
