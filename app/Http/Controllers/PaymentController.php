<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->initialise(
            '/payments',
            'payments.',
            ['role:admin'],
            [
                'new_payment_url'=>'/add',
                'form_url'=>'/add'
            ],
            Payment::class
        );
    }

    public function index()
    {
        $this->filters = ['from_date','to_date'];
        $this->payments = $this->defaultModel::canFilter($this->filters)->get();
        return $this->cView('index');
    }

    public function addForm()
    {
        $this->header = $this->hasId()?"Edit payment":"Add payment";
        $this->model = $this->getModel($this->defaultModel);
        return $this->cView('add');
    }

    public function add()
    {
        $form_data = $this->request->except('_token');
        $this->model = $this->getModel($this->defaultModel);
        if($this->hasId())
            $this->model->update($form_data);
        else
            $this->model->create($form_data);

        return redirect($this->root_url)->with('success_msg','Payment added successfully');
    }
}
