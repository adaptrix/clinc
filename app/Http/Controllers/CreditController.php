<?php

namespace App\Http\Controllers;

use App\Credit;
use App\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CreditController extends Controller
{
    public function __construct()
    {
        $this->initialise(
            '/credit',
            'credits.',
            ['role:admin'],
            [
                "add_credit_url"=>"/add",
                "form_url"=>"/add",
                "edit_url"=>"/add",
                "delete_url"=>"/remove",
                "pay_url"=>"/pay"
            ],
            Credit::class
        );
    }

    public function index()
    {
        $this->filters = ['from_date','to_date'];
        $this->credits = $this->defaultModel::canFilter($this->filters)->get();
        return $this->cView('index');
    }

    public function addForm()
    {
        $this->header = $this->hasId()?"Edit credit":"Add credit";
        $this->model = $this->getModel(Credit::class);
        return $this->cView('add');
    }

    public function add()
    {
        $this->credit = $this->getModel(Credit::class);

        $this->form_data = $this->request->except('_token');

        if($this->hasId()){
            $this->credit->update($this->form_data);
        }else{
            $this->credit::create($this->form_data);
        }

        return redirect($this->root_url)->with('success_msg','Credit addedd successfully');
    }

    public function pay()
    {
        $credit = $this->getModel();
        $credit->status = "paid";
        $credit->save();
        $payment = Payment::create([
            "description"=>$credit->description,
            "date"=>Carbon::today(),
            "amount"=>$credit->amount
        ]);
        return redirect($this->root_url)->with('success_msg','Credit paid successfully');
    }

    public function remove()
    {
        $this->defaultModel::find($this->request->id)->delete();
        return redirect($this->root_url)->with('success_msg','Credit deleted successfully');
    }
}
