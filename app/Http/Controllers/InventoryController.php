<?php

namespace App\Http\Controllers;

use App\InventoryRecord;
use Illuminate\Http\Request;

class InventoryController extends Controller
{ 
    public function __construct()
    {
        $this->initialise(
                "/inventory",
                "inventory.",
                ['role:admin'],
                [
                    "add_item_url"=>'/add',
                    "form_url"=>'/add',
                    "edit_url"=>'/add',
                    "delete_url"=>'/remove'
                ],
                InventoryRecord::class
            ); 
    }

    public function index()
    {
        $this->inventory_records = $this->defaultModel::all();
        return $this->cView('index');
    }

    public function addForm()
    {   
        $this->header = $this->hasId()?"Edit Item":"Add Item";
        $this->model = $this->getModel(InventoryRecord::class); 
        return $this->cView('add');
    }

    public function add()
    {
        $this->defaultModel = $this->getModel(InventoryRecord::class);
        if($this->hasId())
            $this->defaultModel->update($this->request->only('name','unit','quantity'));
        else
            $this->defaultModel::create($this->request->only('name','unit','quantity'));
        return redirect($this->root_url)->with('success_msg','Record added successfully'); 
    }

    public function remove()
    { 
        $this->defaultModel::find($this->request->id)->delete();
        return redirect()->back()->with('success_msg','Record deleted successfully');
    }
}

