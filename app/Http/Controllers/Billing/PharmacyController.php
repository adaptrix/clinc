<?php

namespace App\Http\Controllers\Billing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MedicineRecord;
use App\Billing;
use App\BillingTransaction;
use App\BillingList;
use App\BillingChange; 


class PharmacyController extends Controller
{
    //

    public function prescriptionDetails($id){
        return view('billings.index',['medicine_id'=>$id,'title'=>'Billing','pharmacy_tab'=>2]);
    }

    public function getMedCost($id){
        $medrecord = MedicineRecord::find($id);
        $medcost = $medrecord->medicine->selling_price * $medrecord->days;
        return $medcost;
    }

    public function payBill(Request $request){ 
        
        switch ($request->action) {
            case 'bill':
                return $this->bill($request);
                break;
            case 'discount':
                return $this->discount($request);
                break;
            case 'refused':
                return $this->refused($request);
                break;
            
            case 'cashless':
                return $this->cashless($request);
                break;
            default:
                # code...
                break;
        }

    }
 
    public function bill($request)
    {
        $billing = new Billing;
        $billing->patient_registration_no = $request->prn;
        $billing->visit_no = $request->pvn;
        $billing->service_cost = $request->total_amount;
        $billing->amount_due = $request->amount_due;
        $billing->transaction_type = 'pharmacy';
        $billing->discount = 0;
        $billing->save();
        
        $billingtransaction = new BillingTransaction;
        $billingtransaction->amount_paid = $request->amount_recieved;
        $billingtransaction->amount_due = $request->amount_due;
        $billingtransaction->billing_id = $billing->id;
        $billingtransaction->save();
 
        $billingchange = new BillingChange;
        $billingchange->amount_recieved = $request->amount_recieved;
        $billingchange->change = $request->change_remaining;
        $billingchange->change_given = $request->change_returned; 
        $billingchange->change_due = $this->a($request->change_remaining) - $this->a($request->change_returned);//$request->change_remaining;
        $billingchange->billing_id = $billing->id;
        $billingchange->save();

        $pharmacylist = BillingList::find($request->list_id);
        $pharmacylist->billing_id = $billing->id;
        $pharmacylist->status = "paid";
        $pharmacylist->save();
 
        $index = 0;
        if($request->has('medicine_record_id'))
            foreach($request->medicine_record_id as $ms){
                $mr = MedicineRecord::find($request->medicine_record_id[$index]);
                if($request->has('status'.$request->medicine_record_id[$index]))
                    $mr->status = "Paid";
                else
                    $mr->status = "Refused";
                $mr->save();
                $index++;
            } 
        return redirect('/billing')->with('success','Patient billed');
    }
 
    public function cashless($request)
    {
        $billing = new Billing;
        $billing->patient_registration_no = $request->prn;
        $billing->visit_no = $request->pvn;
        $billing->service_cost = $request->total_amount;
        $billing->amount_due = 0;
        $billing->transaction_type = 'pharmacy';
        $billing->discount = 0;
        $billing->save();
        
        $billingtransaction = new BillingTransaction;
        $billingtransaction->amount_paid = $request->total_amount;
        $billingtransaction->amount_due = 0;
        $billingtransaction->billing_id = $billing->id;
        $billingtransaction->save();
 
        $billingchange = new BillingChange;
        $billingchange->amount_recieved = $request->total_amount;
        $billingchange->change =0;
        $billingchange->change_given = 0; 
        $billingchange->change_due = 0;
        $billingchange->billing_id = $billing->id;
        $billingchange->save();

        $pharmacylist = BillingList::find($request->list_id);
        $pharmacylist->billing_id = $billing->id;
        $pharmacylist->status = "paid";
        $pharmacylist->save();
 
        $index = 0;
        if($request->has('medicine_record_id'))
            foreach($request->medicine_record_id as $ms){
                $mr = MedicineRecord::find($request->medicine_record_id[$index]);
                if($request->has('status'.$request->medicine_record_id[$index]))
                    $mr->status = "Paid";
                else
                    $mr->status = "Refused";
                $mr->save();
                $index++;
            } 
        return redirect('/billing')->with('success','Patient billed');
    }

    public function discount($request)
    {  
        $billing = new Billing;
        $billing->patient_registration_no = $request->prn;
        $billing->visit_no = $request->pvn;
        $billing->service_cost = $this->a($request->total_amount)-$this->a($request->service_charge);
        $billing->amount_due = $this->a($request->amount_due);
        $billing->transaction_type = 'pharmacy';
        $billing->discount = 0;
        $billing->save();
        
        $billingtransaction = new BillingTransaction;
        $billingtransaction->amount_paid = $this->a($request->amount_recieved);
        $billingtransaction->amount_due = $this->a($request->amount_due);
        $billingtransaction->billing_id = $billing->id;
        $billingtransaction->save();
 
        $billingchange = new BillingChange;
        $billingchange->amount_recieved = $this->a($request->amount_recieved);
        $billingchange->change = $this->a($request->change_remaining);
        $billingchange->change_given = $this->a($request->change_returned); 
        $billingchange->change_due = $this->a($request->change_remaining) - $this->a($request->change_returned);//$request->change_remaining;
        $billingchange->billing_id = $billing->id;
        $billingchange->save();

        $pharmacylist = BillingList::find($request->list_id);
        $pharmacylist->billing_id = $billing->id;
        $pharmacylist->status = "paid";
        $pharmacylist->save();
 
        $index = 0;
        if($request->has('medicine_record_id'))
            foreach($request->medicine_record_id as $ms){
                $mr = MedicineRecord::find($request->medicine_record_id[$index]);
                if($request->has('status'.$request->medicine_record_id[$index]))
                    $mr->status = "Paid";
                else
                    $mr->status = "Refused";
                $mr->save();
                $index++;
            } 
        return redirect('/billing')->with('success','Patient billed');
    }

    public function refused($request)
    {     
        $lablist = BillingList::find($request->list_id);
        $lablist->billing_id = 0;
        $lablist->queue_no = 0;
        $lablist->status = 'refused';
        $lablist->save();
        return redirect('/billing');
    }

    public function medicineGiven(Request $request){ 
        $index = 0;  
        if($request->has('status'))
        foreach($request->status as $ms){
            $mr = MedicineRecord::find($request->medicine_record_id[$index]);
            
            if($mr){
                $medicine_stock_batch_id = $this->getMd($mr->medicine_id,$mr->total,$mr->id); 
                $mr->status = $ms;
                $mr->save();
                $index++;    
            }
           
        }
        
        $billinglistitem = BillingList::find($request->billing_list_id);
        $billinglistitem->status = "CompleteCycle";
        $billinglistitem->save();
        return redirect('/pharmacy')->with('success','Patient has been given medicine');
    }

        
    public function getMd($medicine_id, $requested_medicine_amount, $medicine_record_id)
    { 
        //$medicine_id=1;
        //$requested_medicine_amount = 53;//remaining 400
        $medicine = \App\Medicine::find($medicine_id);
        $selling_price = $medicine->selling_price; 
        if($medicine->quantity_per_unit!="0"&&!empty($medicine->quantitiy_per_unit))
            $buying_price = $medicine->unit_purchasing_price/$medicine->quantitiy_per_unit;
        else
            $buying_price = $medicine->unit_purchasing_price/1;

        $continue_iterations = true;
        $batch_ids[]=[0]; 
        //dd('');
        $remaining_quantity = $requested_medicine_amount; 
        if($this->checkForEnoughMedicinesInStock($requested_medicine_amount,$medicine_id)){
            while($continue_iterations){       
                $batch  = \App\MedicineStockBatch::where('medicine_id',$medicine_id)
                                            ->where('remaining_quantity','!=','0')
                                            ->whereNotIn('id',$batch_ids)
                                            ->orderBy('expiry_date','asc')
                                            ->first();         
                $batch_ids[] = $batch->id;
                if($requested_medicine_amount<=$batch->remaining_quantity){  
                    //good there's enough....
                    $remaining_quantity = $batch->remaining_quantity - $requested_medicine_amount;
                    $batch->remaining_quantity = $remaining_quantity;
                    $batch->save();

                    $total_remaining_quantity = \App\MedicineStockBatch::where('medicine_id',$medicine->id)
                                        ->sum('remaining_quantity');

                    //store each use after batch save
                    
                    $medicine_stock_uses =new \App\MedicineStockUse;
                    $medicine_stock_uses->medicine_record_id = $medicine_record_id;
                    $medicine_stock_uses->save();

                    $use_records = new \App\MedicineStockBatchMedicineStockUse;
                    $use_records->medicine_stock_batch_id= $batch->id;
                    $use_records->medicine_stock_use_id= $medicine_stock_uses->id;
                    $use_records->used_quantity= $requested_medicine_amount;
                    $use_records->remaining_quantity = $remaining_quantity;
                    $use_records->total_remaining_quantity = $total_remaining_quantity;
                    $use_records->total_cost = $requested_medicine_amount * $medicine->selling_price;
                    $use_records->total_buying_price = $requested_medicine_amount * $buying_price;
                    $use_records->save();
                    
                    //continue with the storage procedure
                    $continue_iterations = false; 
                }
                else{
                    $requested_medicine_amount = $requested_medicine_amount - $batch->remaining_quantity;
                    

                    $batch->remaining_quantity = 0;
                    $batch->save();  

                    $total_remaining_quantity = \App\MedicineStockBatch::where('medicine_id',$medicine->id)
                                        ->sum('remaining_quantity');

                    //save each use after batch save
                    $medicine_stock_uses =new \App\MedicineStockUse;
                    $medicine_stock_uses->medicine_record_id = $medicine_record_id;
                    $medicine_stock_uses->save();

                    $use_records = new \App\MedicineStockBatchMedicineStockUse;
                    $use_records->medicine_stock_batch_id= $batch->id;
                    $use_records->medicine_stock_use_id= $medicine_stock_uses->id;
                    $use_records->used_quantity= $requested_medicine_amount;
                    $use_records->total_remaining_quantity = $total_remaining_quantity;
                    $use_records->remaining_quantity = 0;
                    $use_records->total_cost = $requested_medicine_amount * $medicine->selling_price;
                    $use_records->total_buying_price = $requested_medicine_amount * $buying_price;
                    $use_records->save();
                    
                } 
            }     
        }
        else{
            echo 'there arent enough meds';
        }   
        return $batch_ids;
    }


    public function checkForEnoughMedicinesInStock($total_quantity, $medicine_id)
    { 
        $requested_medicine_amount = $total_quantity;
        $medicine_id = $medicine_id;
        $continue_iterations = true;
        $batch_ids[]=[0]; 
        $remaining_quantity = $requested_medicine_amount;
        $there_are_enough_medicines = "false";
        $i = 1;
        $medicine = \App\Medicine::find($medicine_id);
        while($continue_iterations){       
            $batch  = \App\MedicineStockBatch::where('medicine_id',$medicine_id)
                                        ->where('remaining_quantity','!=','0')
                                        ->whereNotIn('id',$batch_ids)
                                        ->orderBy('expiry_date','asc')
                                        ->first();
            
            if($batch!=null){
                $batch_ids[] = $batch->id;
                if($requested_medicine_amount<=$batch->remaining_quantity){  
                    //good there's enough....
                    $remaining_quantity = $batch->remaining_quantity - $requested_medicine_amount;
                    $batch->remaining_quantity = $remaining_quantity; 
                    //continue with the storage procedure
                    $requested_medicine_amount = 0;
                    $continue_iterations = false; 
                    $there_are_enough_medicines = true;
                }
                else{
                    $requested_medicine_amount = $requested_medicine_amount - $batch->remaining_quantity;
                    $batch->remaining_quantity = 0;  
                }
            }
            else{
                $continue_iterations = false; 
                $there_are_enough_medicines = false;
            } 
            $i++;
        } 
        return $there_are_enough_medicines; 
    }

    public function a($a)
    { 
        $a = str_replace(',', '', $a);
        return $a;
    }

    public function getMedDescription(Request $request){
        $medicine = \App\Medicine::find($request->id);
        return ["description"=>$medicine->description];
    }
}
