<?php

namespace App\Http\Controllers\Billing;

use App\Billing;
use App\BillingChange;
use App\BillingList;
use App\BillingTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PatientQueueTemporary;

class BillingController extends Controller
{
    //
    public function index(){
        return view('billings.index',['title'=>'Billing']);
    }
    
    public function requestGloves(Request $request)
    {
        $glove_request = new \App\GloveRequest;
        $glove_request->no_of_gloves = $request->no_of_gloves;
        $glove_request->save();
        return redirect('/laboratory')->with('equipment',true);;
    }

    public function payForAppointment(Request $request)
    {         
        $billing = Billing::wherePatientRegistrationNo($request->prn)
                            ->whereVisitNo($request->pvn)
                            ->first();        
        $queue = PatientQueueTemporary::wherePatientRegNo($request->prn)
                            ->whereVisitNo($request->pvn)
                            ->first();  
                            
        $queue->status = "waiting";
        $queue->save();

        $billing->amount_due = $request->amount_due; 
        $billing->save();

        $billingtransaction = new BillingTransaction();
        $billingtransaction->amount_paid = $request->amount_recieved;
        $billingtransaction->amount_due = $request->amount_due;
        $billingtransaction->billing_id = $billing->id;
        $billingtransaction->save();

        $billingchange = new BillingChange();
        $billingchange->amount_recieved = $request->amount_recieved;
        $billingchange->change = $request->remaining - $request->change_returned;
        $billingchange->change_given = $request->change_returned;
        $billingchange->change_due = $request->change_remaining - $request->change_returned;
        $billingchange->billing_id = $billing->id;
        $billingchange->save();
    
        $billinglist = BillingList::find($request->list_id);
        $billinglist->status = "paid";
        $billinglist->save();
        return redirect()->back();
    }
}
