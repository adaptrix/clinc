<?php

namespace App\Http\Controllers;

use App\MeetingMinute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MeetingMinuteController extends Controller
{
    public function __construct()
    {
        $this->initialise(
            '/og-meeting-minutes',
            'meeting_minutes',
            ['role:admin'],
            [
                "new_meeting_minute_url"=>"/add",
                "form_url"=>"/add",
                "edit_meeting_minute_url"=>"/add",
                "view_meeting_minute_url"=>"/view",
                "remove_meeting_minute_url"=>"/remove"
            ],
            MeetingMinute::class
        );

        $this->is_og= $this->request->is("og-*");
    }

    public function index()
    {
        $this->filters = ['year','month'];
        if($this->is_og)
            $this->meeting_minutes = $this->defaultModel::canFilter($this->filters)->whereType('og')->get();
        else
            $this->meeting_minutes = $this->defaultModel::canFilter($this->filters)->whereType('qc')->get();
        return $this->cView('index');
    }

    public function addForm()
    {
        $this->header = $this->hasId()?"Edit meeting minute":"Add new meeting minute";
        $this->file_edit_message = $this->hasId()?"(The file you upload will automatically replace the old one)":"";
        $this->model = $this->getModel(); 
        return $this->cView('add');
    }

    public function add()
    { 
        $meeting_minute = $this->getModel();
        $form_data = [];

        if($this->request->hasFile('document')){
            $file = $this->request->file('document');
            $form_data["file_name"] = $file->getClientOriginalName();
            $form_data["url"] = $file->store('meeting_minutes');            
        }

        $form_data = array_merge($this->request->only('content','date'),$form_data);
        $form_data["type"] = $this->is_og?"og":"qc";

        if($this->hasId()){
            if( $this->request->should_remove_file){
                Storage::delete($meeting_minute->url);
                $form_data["file_name"] = null;
                $form_data["url"] = null;
            }
            $meeting_minute->update($form_data);
        }else{
            $form_data["url"] = $file_url??null;
            $form_data["file_name"] = $file_name??null;
            $meeting_minute::create($form_data);
        }

        return redirect($this->root_url)->with('success_msg','Successfully upadated the minute');
    }

    public function view()
    {
        $this->model = $this->getModel();
        return $this->cView('view');
    }

    public function remove()
    {
        $model = $this->getModel();
        if($model->hasFile())
            Storage::delete($model->url);
        $model->delete();
        return redirect($this->root_url)->with('success_msg','Minute deleted successfully');
    }
}