<?php

namespace App\Http\Controllers;

use App\CorporateCompany;
use Illuminate\Http\Request;

class CorporateCompanyController extends Controller
{
    public function __construct() {
        $this->initialise(
            '/corporate-companies',
            'corporate-companies',
            ["role:admin"],
            [
                "new_cc_url"=>'/add',
                "edit_cc_url"=>'/add',
                "delete_cc_url"=>'remove'
            ],
            CorporateCompany::class
        );
    }

    public function index()
    {
        $this->corporate_companies = $this->defaultModel::all();
        return $this->cView('index');
    }

    public function addForm()
    {
        $this->model = $this->getModel();
        $this->header = $this->hasId()?"Edit corporate company details":"Add corporate company";
        return $this->cView('add');
    }

    public function add()
    {
        $this->corporate_company = $this->getModel();
        $form_data = $this->request->except('_token');
        if($this->is_editing){
            $this->corporate_company->update($form_data);
        }else{
            $this->corporate_company::create($form_data);
        }
        return redirect($this->root_url)->with('success_msg','Corporate company details update successfully!');
    }

    public function remove()
    {
        $this->getModel()->delete();
        return redirect($this->root_url)->with('success_msg','Corporate company removed successfully');
    }
}
