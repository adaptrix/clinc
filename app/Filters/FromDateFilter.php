<?php 

namespace App\Filters;

use Carbon\Carbon;

class FromDateFilter 
{ 
    public function filter($builder, $value)
    {
        $date = Carbon::parse($value);

        return $builder->where('date',">=", $date);
    }
}
