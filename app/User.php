<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable; 

    protected $fillable = ['name', 'email', 'password','email','phone_no','employee_code','address','department_id','joining_date','gender','fee','commission','salary']; 
    protected $hidden = ['password', 'remember_token', ];
    public static $GENDERS = ["male","female"];

    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }

    /**
    * @param string|array $roles
    */

    public function authorizeRoles($roles)
    {
        if ($this->hasAnyRole($roles)) {
            return true;
        }
        abort(401, 'This action is unauthorized.');
    }

    /**
    * Check multiple roles
    * @param array $roles
    */

    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }

        return false;
    }

    /**
    * Check one role
    * @param string $role
    */

    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }

    public function getRoleAttribute()
    {
        return $this->roles()->first();
    }

    public static function consultants()
    {
        $role = Role::whereName('consultant')->first();
        $users = $role?$role->users:[];
        return $users;
    }

    public static function staffs()
    {
        $staffs = User::join('role_user','users.id','=','role_user.user_id')
                        ->join('roles','role_user.role_id','=','roles.id')
                        ->whereIn('roles.name',Role::$STAFF_ROLES)
                        ->get(['users.id','users.name','gender']);  
        return $staffs;
    }

    public static function getStaff($role)
    {
        $staffs = User::join('role_user','users.id','=','role_user.user_id')
                        ->join('roles','role_user.role_id','=','roles.id')
                        ->where('roles.name',$role);
        return $staffs;
    }

    public static function departments()
    {
        return array_map(function($element){
            return studly_case($element);
        },Role::$STAFF_ROLES); 
    }

    public static function departmentModels()
    {
        return Role::whereIn('name',Role::$STAFF_ROLES)->get();
    }

    public function announcements()
    { 
        return $this->belongsToMany(Announcement::class, 'announcement_targets', 'user_id', 'announcement_id')->withPivot('status');
    }

    public function announcementTargets()
    {
        return $this->hasMany(AnnouncementTarget::class, 'user_id', 'id');
    }

    public function department()
    {
        return $this->hasOne(Role::class, 'id', 'department_id');
    }

    public function getDepartmentNameAttribute()
    {
        return studly_case($this->roles()->first()->name);
    }

    public function getFormJoiningDateAttribute()
    {
        return Carbon::parse($this->joining_date)->format('Y-m-d');
    }

    public function attendance($date)
    {
        $id = $this->id; 
        $date = $date->format('Y-m-d');
        $attendance = EmployeeAttendance::whereEmployeeId($id)->where('date',$date)->first();

        return $attendance?$attendance->status:"-";
    }

    public function attendanceStats($date)
    {
        $id = $this->id;
        $month = $date->month;
        $query =  EmployeeAttendance::whereEmployeeId($id)->whereMonth('date',$month);
        $stats['total'] = EmployeeAttendance::whereEmployeeId($id)->whereMonth('date',$month)->count();
        $stats['late'] = EmployeeAttendance::whereEmployeeId($id)->whereMonth('date',$month)->whereStatus('late')->count();
        $stats['present'] = EmployeeAttendance::whereEmployeeId($id)->whereMonth('date',$month)->whereStatus('present')->count();
        $stats['absent'] = EmployeeAttendance::whereEmployeeId($id)->whereMonth('date',$month)->whereStatus('absent')->count();

        return (object)$stats;
    }

    public function leaves()
    {
        $employee_leaves = EmployeeLeave::whereEmployeeId($this->id)->get();
        $sum = 0;
        foreach($employee_leaves as $leave) $sum += $leave->number_of_days;
        return $sum;
    }
}
