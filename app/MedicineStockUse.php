<?php

namespace App;


use \App\BaseModel as Model;


class MedicineStockUse extends Model
{
    public function medicineStockBatchMedicineUses()
    {
        return $this->hasOne('App\MedicineStockBatchMedicineStockUse','medicine_stock_use_id');
    }
}
