<?php

namespace App;


use \App\BaseModel as Model;


class MedicineStockBatch extends Model
{
    //
    public function medicine()
    {
        return $this->belongsTo('App\Medicine','medicine_id');
    }
}
