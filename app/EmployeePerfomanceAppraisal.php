<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class EmployeePerfomanceAppraisal extends Model
{
    protected $guarded = [];

    public function setDateAttribute( $value)
    {
        $this->attributes["date"] = Carbon::parse($value)->format('d/m/Y');
    }
}
