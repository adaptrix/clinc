Mousetrap.bind('b', function() {  
    activateTab('lab-bill');
 });

 Mousetrap.bind('n', function() {  
    activateTab('pharmacy-bill');
 });

 Mousetrap.bind('g', function() {  
    activateTab('gloves');
 });
 Mousetrap.bind('p', function() {  
    activateTab('appointment');
 });

function activateTab(tab){
    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
};


function showChange(rowId){
    var total = $('#service_cost'+rowId);
    var totalSpan = $('#service_cost_span'+rowId);
    var change = $('#change'+rowId);
    var amountPaid = $('#recieved'+rowId);

    var Total = parseInt(total.val());
    var AmountPaid = parseInt(amountPaid.val());
    var Change = parseInt(change.val());

    if(AmountPaid>=Total){
        var diff = AmountPaid - Total;
        change.val(diff);
    }
    else{
        change.val(0);
    }
    
    var ts = total.val()+totalSpan.text() + change.val() + amountPaid.val();
    console.log(ts); 
} 
 
function newSum(rowid,checkboxid){
    var id = rowid+""+checkboxid;
    var price = parseInt(a($('#price'+id).val()));
    var total = parseInt(a($('#sum'+rowid).val()));
    console.log(price+""+total);
    if($('#checkbox'+id).is(":checked")){
        $('#sum'+rowid).val((total+price).toLocaleString());
        $('#service_cost'+rowid).val((total+price).toLocaleString()); 
        console.log(total+price);
    }else{
        $('#sum'+rowid).val((total-price).toLocaleString());
        $('#service_cost'+rowid).val((total-price).toLocaleString());
        console.log(total-price);
    }
    newDue(rowid);
}

function newDue(rowid){ 
    var previousChange = parseInt(a($('#previous_change'+rowid).val()));
    var total = parseInt(a($('#sum'+rowid).val()));      
    l(total);
    var dueField = $('#amount_required_to_pay'+rowid); 
    if(previousChange>=total){ 
        dueField.val(0); 
    }else{  
        l(total+''+previousChange);
        dueField.val((total-previousChange).toLocaleString()); 
    }
    newChange(rowid);
}

function newChange(rowid){
    //get the required fields for the transaction
    var amountReceivedField = $('#amount_recieved'+rowid);
    var changeRemainingField = $('#change_remaining'+rowid);
    var amountDueField = $('#amount_due'+rowid);
    var amountRequiredToPayField = $('#amount_required_to_pay'+rowid);

    var amountDue = parseInt(a($('#amount_due'+rowid).val()));
    var previousChange = parseInt(a($('#previous_change'+rowid).val()));
    var total = parseInt(a($('#sum'+rowid).val()));   
    var amountReceived = parseInt(a(amountReceivedField.val()));  
    var amountRequiredToPay = parseInt(a(amountRequiredToPayField.val()));
    var changeRemaining = parseInt(a(changeRemainingField.val())); 
    var changeReturned = parseInt(a($('#change_returned'+rowid).val()));

    // to be used later if required
    var amountRemaining;
    if(amountRequiredToPay>0){
        //there is some cash to pay 
        amountReceivedField.removeAttr('readonly');

        //calculate the remaining change and amount due(if needed)
        if(amountReceived>=amountRequiredToPay){
            changeRemainingField.val((amountReceived-amountRequiredToPay).toLocaleString());
            amountDueField.val(0);
        }
        else{
            changeRemainingField.val(0);
            amountRemaining = amountRequiredToPay - amountReceived;
            amountDueField.val(amountRemaining);
        }
        l(amountDueField.val());
    }else{
        // no cash to pay the change is sufficient
        changeRemainingField.val((previousChange-total).toLocaleString());
        amountReceivedField.prop('readonly',true);        
    }
}

function a(number){
    try {
        number = number.replace(/\,/g,'');
        return number;
    } catch (err) {
        l(err);
        return 0;
    }
   
}


var rowNo = 1, 
    table = document.getElementById('patient-que-table'), 
    rowCount = table.dataset.rowCount, 
    row,
    color = "#aaaaaa",
    textColor = "black",
    textColorSelected = "White",
    prevRowColor,
    fieldCount,
    fieldNo  = 1,
    rowIdSelector,
    fieldIdSelector,
    field,
    fieldSelected;

Mousetrap.stopCallback = function () {
    return false;
}
setFirstRowColor();
setFirstField();
Mousetrap.bind('up',up);

Mousetrap.bind('down',down);

Mousetrap.bind('left',left);

Mousetrap.bind('right',right);

function up(){ 
    console.log('up');
    if(row!=null&&prevRowColor!=null){
         row.style.backgroundColor = prevRowColor; 
         row.style.color = textColor;
    }   
    if(rowNo>1) rowNo--;
    //else rowNo = rowCount;
    rowIdSelector = "row" + rowNo;
    row = document.getElementById(rowIdSelector);
    prevRowColor = row.style.backgroundColor;
    row.style.backgroundColor = color;
    row.style.color = textColorSelected;
    fieldCount = row.dataset.fieldsCount; 
    fieldNo = 1;
    if(fieldSelected.nodeName=="DIV") fieldSelected.style.border = "0px solid black";
    setFirstField();
}

function down(){ 
    if(row!=null&&prevRowColor!=null){
        row.style.backgroundColor = prevRowColor;
        row.style.color = textColor;
    }     
    if(rowNo<rowCount) rowNo++; 
    rowIdSelector = "row" + rowNo;
    row = document.getElementById(rowIdSelector);
    prevRowColor = row.style.backgroundColor;
    row.style.backgroundColor = color;
    row.style.color = textColorSelected;
    fieldCount = row.dataset.fieldsCount; 
    fieldNo = 1;
    if(fieldSelected.nodeName=="DIV") fieldSelected.style.border = "0px solid black";
    setFirstField();
}

function right(){
    if(fieldSelected.nodeName=="DIV") fieldSelected.style.border = "0px solid black";
    if(fieldNo<fieldCount) fieldNo++;
    console.log(fieldCount + " " + fieldNo);
    fieldIdSelector = 'field' + rowNo + fieldNo;
    fieldSelected = document.getElementById(fieldIdSelector);
    if(fieldSelected.nodeName=="DIV") fieldSelected.style.border = "1px solid black";
    fieldId = fieldSelected.dataset.fieldId;
    field = document.getElementById(fieldId);
    field.focus();
    console.log(fieldId+" "+fieldIdSelector);
}

function left(){
    if(fieldSelected.nodeName=="DIV") fieldSelected.style.border = "0px solid black";
    if(fieldNo>1) fieldNo--;
    fieldIdSelector = 'field' + rowNo + fieldNo;
    fieldSelected = document.getElementById(fieldIdSelector);
    if(fieldSelected.nodeName=="DIV") fieldSelected.style.border = "1px solid black";
    fieldId = fieldSelected.dataset.fieldId;
    field = document.getElementById(fieldId);
    field.focus();
    console.log(fieldId+" "+fieldIdSelector); 
}

function setFirstRowColor(){
    console.log(rowCount);
    if(rowCount>0){
        row = document.getElementById('row'+1);
        rowIdSelector = 1;
        prevRowColor = row.style.backgroundColor;
        row.style.backgroundColor = color;
        row.style.color = textColorSelected;
        fieldCount = row.dataset.fieldsCount; 
    }
}

function setFirstField(){
    fieldIdSelector = 'field' + rowNo + fieldNo;
    fieldSelected = document.getElementById(fieldIdSelector);
    if(fieldSelected.nodeName=="DIV") fieldSelected.style.border = "1px solid black";
    fieldId = fieldSelected.dataset.fieldId;
    field = document.getElementById(fieldId);
    field.focus();
}
 

$('.datatable').dataTable({
    keys:true,
    "ordering":false,
    "bLengthChange":false,
});