var users_table = $('#users-table');
var rooms_table = $('#rooms-table');
var services_table = $('#services-table');
var servicemodes_table = $('#servicemodes-table');
var units_table = $('#units-table');

user = function(id)
{
    var data = 'get';
    var _token = $('[name=_token]').val();
    $.ajax({
        url: 'user/update',
        type: 'POST',
        dataType: 'json',
        data: {id:id,data:data,_token:_token}
    }).done(function(data){
        $('#user-id').val(data.userid);
        $('#employee_id').val(data.employee_id);
        $('#name').val(data.username);
        $('#email').val(data.email);
        $('#role').val(data.role);
        $('#user-form').attr('action',data.url);

        $('#userModal').modal('show');
    }).fail(function(){
        console.log('error');
    });
    
}

room = function(id)
{
    var data = 'get';
    var _token = $('[name=_token]').val();
    $.ajax({
        url: 'room/update',
        type: 'POST',
        dataType: 'json',
        data: {id:id,data:data,_token:_token}
    }).done(function(data){
        $('#room_id').val(data.roomid);
        $('#room_no').val(data.room);
        $('#use').val(data.use);
        $('#rooms-form').attr('action',data.url);

        $('#roomModal').modal('show');
    }).fail(function(){
        console.log('error');
    });
    
}

service = function(id,type)
{
    var _token = $('[name=_token]').val();
    $.ajax({
        url: 'service/update',
        type: 'POST',
        dataType: 'json',
        data: {id:id,data:type,_token:_token}
    }).done(function(data){
        if(type === 'get')
        {
            $('#service_id').val(data.serviceid);
            $('#service').val(data.service);
            $('#price').val(data.price);
            $('#service-form').attr('action',data.url);
            $('#serviceModal').modal('show');
        }
        else if(type === 'delete')
        {
            services_table.ajax.reload( null, false );
            Lobibox.notify('success',{
                msg: 'Delete Successful'                    
            });
        }
        
    }).fail(function(){
        console.log('error');
    });
    
}

servicemode = function(id)
{
    var data = 'get';
    var _token = $('[name=_token]').val();
    $.ajax({
        url: 'servicemode/update',
        type: 'POST',
        dataType: 'json',
        data: {id:id,data:data,_token:_token}
    }).done(function(data){
        $('#service_mode_id').val(data.modeid);
        $('#service_mode').val(data.servicemode);
        $('#price').val(data.price);
        $('#servicemode-form').attr('action',data.url);

        $('#servicemodeModal').modal('show');
    }).fail(function(){
        console.log('error');
    });
    
}

unit = function(id)
{
    var data = 'get';
    var _token = $('[name=_token]').val();
    $.ajax({
        url: 'unit/update',
        type: 'POST',
        dataType: 'json',
        data: {id:id,data:data,_token:_token}
    }).done(function(data){
        $('#unit_id').val(data.unitid);
        $('#unit').val(data.unit);
        $('#units-form').attr('action',data.url);

        $('#unitModal').modal('show');
    }).fail(function(){
        console.log('error');
    });
    
}

var user_form = $('#user-form');
user_form.submit(function(event){
    event.preventDefault();
    var formData = new FormData(this);
    var url = $(this).attr('action');
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    })
    .done(function(feedback) {
        $('#user-form')[0].reset();
        user_form.modal('toggle');
        //users_table.ajax.reload( null, false );
        $('#user-form').attr('action','user/create');
        Lobibox.notify('success',{
            msg: 'Success'                    
        });
    })
    .fail(function() {        
        console.log("error");
    });
});

var room_form = $('#rooms-form');
room_form.submit(function(event){
    event.preventDefault();
    var formData = new FormData(this);
    var url = $(this).attr('action');
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    })
    .done(function(feedback) {
        room_form[0].reset();
        room_form.modal('toggle');
        //rooms_table.ajax.reload( null, false );
        $('#rooms-form').attr('action','room/create');
        Lobibox.notify('success',{
            msg: 'Success'                    
        });
    })
    .fail(function(erro) {
        console.log("error");
        console.log(error);
    });
});

var service_form = $('#service-form');
service_form.submit(function(event){
    event.preventDefault();
    var formData = new FormData(this);
    var url = $(this).attr('action');
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    })
    .done(function(feedback) {
        service_form[0].reset();
        service_form.modal('hide');
       // services_table.ajax.reload( null, false );
        $('#service-form').attr('action','service/create');
        Lobibox.notify('success',{
            msg: 'Success'                    
        });
    })
    .fail(function() {
        console.log("error");
    });
});

var servicemode_form = $('#servicemode-form');
servicemode_form.submit(function(event){
    event.preventDefault();
    var formData = new FormData(this);
    var url = $(this).attr('action');
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    })
    .done(function(feedback) {
        servicemode_form[0].reset();
        servicemode_form.modal('toggle');
       // servicemodes_table.ajax.reload( null, false );
        $('#servicemode-form').attr('action','servicemode/create');
        Lobibox.notify('success',{
            msg: 'Success'                    
        });
    })
    .fail(function() {
        console.log("error");
    });
});

var units_form = $('#units-form');
units_form.submit(function(event){
    event.preventDefault();
    var formData = new FormData(this);
    var url = $(this).attr('action');
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    })
    .done(function(feedback) {
        units_form[0].reset();
        units_form.modal('toggle');
     //  units_table.ajax.reload( null, false );
        $('#unit-form').attr('action','unit/create');
        Lobibox.notify('success',{
            msg: 'Success'
        });
    })
    .fail(function() {
        console.log("error");
    });
});

$('.select2').select2();

// $('.datatable').dataTable();
$(document).ready(function(){
    $('.mytable').DataTable();
})




var medIndex = 1;
function add_medicine_fields(){ 
    medIndex++;
    var medRow = document.createElement("div");
    medRow.setAttribute("class","row");
    medRow.setAttribute("id","medRow"+medIndex);
    medRow.setAttribute("style","padding-top:10px;");
    var fields = "<div class='col-md-4' id='medicines'> <select class='select2a form-control' name='medicines[]'>"+$('#medicines').html()+"<select/></div>"+"<div class='col-md-2'>        <input type='number' name='price[]' id=' placeholder='Price' class='form-control' required>    </div>   <div class='col-md-2'>        <button type='button' onclick='remove_medicine_fields("+medIndex+");' class='btn btn-outline-danger'><i class='fa fa-minus'></i></button>    </div>";
    medRow.innerHTML = fields;
    $('#medicine_container').append(medRow);
        
    $('.select2a').select2(); 
}

function remove_medicine_fields(mid){
    $('#medRow'+mid).remove();
}

function enable_mgs_per_tablet(){
    var medicineCategoriesField = $('#medicine_category');
    if(medicineCategoriesField.val()==4){
        $('#mgs_per_tablet').prop('disabled',false);
        console.log("sdfasdfasdf");
    }
    else{
        $('#mgs_per_tablet').prop('disabled',true); 
    }
    console.log('disabled enabled, who cares'+medicineCategoriesField.find(":selected").val());
}


function toggleTargets(target){
    console.log(target.value)
    if(target.value==='Select')
        document.getElementById('targets').classList.remove('hidden');
    else
        document.getElementById('targets').classList.add('hidden');
}