let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/general/index.js', 'public/js/v-general.js')
    .js('resources/assets/js/reception/index.js','public/js/v-reception.js')
    .sass('resources/assets/sass/app.scss', 'public/css');